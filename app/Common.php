<?php

define('datatable', [
   'css' => ['assets/plugins/datatables/datatables.bundle.css'],
   'js' => [
      'assets/plugins/datatables/datatables.bundle.js',
   ]
]);