<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
//$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

$routes->group('admin', ['namespace' => 'App\Controllers\Admin'], function($routes) {
   $routes->group('dashboard', function($routes) {
      $routes->get('/', 'Dashboard::index');
   });

   $routes->group('datareferensi', ['namespace' => 'App\Controllers\Admin\DataReferensi'], function($routes) {
      $routes->group('fakultas', function($routes) {
         $routes->get('/', 'Fakultas::index');

         $routes->post('submit', 'Fakultas::submit');
         $routes->post('getdata', 'Fakultas::getData');
         $routes->post('hapus', 'Fakultas::hapus');
      });

      $routes->group('prodi', function($routes) {
         $routes->get('/', 'Prodi::index');
         $routes->get('getdropdownlist', 'Prodi::getDropdownList');

         $routes->post('submit', 'Prodi::submit');
         $routes->post('getdata', 'Prodi::getData');
         $routes->post('hapus', 'Prodi::hapus');
      });

      $routes->group('jenisjenjangpendidikan', function($routes) {
         $routes->get('/', 'JenisJenjangPendidikan::index');

         $routes->post('submit', 'JenisJenjangPendidikan::submit');
         $routes->post('getdata', 'JenisJenjangPendidikan::getData');
         $routes->post('hapus', 'JenisJenjangPendidikan::hapus');
      });

      $routes->group('ruanganbelajar', function($routes) {
         $routes->get('/', 'RuanganBelajar::index');
         $routes->get('getdropdownlist', 'RuanganBelajar::getDropdownList');

         $routes->post('submit', 'RuanganBelajar::submit');
         $routes->post('getdata', 'RuanganBelajar::getData');
         $routes->post('hapus', 'RuanganBelajar::hapus');
      });

      $routes->group('agama', function($routes) {
         $routes->get('/', 'Agama::index');

         $routes->post('submit', 'Agama::submit');
         $routes->post('getdata', 'Agama::getData');
         $routes->post('hapus', 'Agama::hapus');
      });

      $routes->group('alattransportasi', function($routes) {
         $routes->get('/', 'AlatTransportasi::index');

         $routes->post('submit', 'AlatTransportasi::submit');
         $routes->post('getdata', 'AlatTransportasi::getData');
         $routes->post('hapus', 'AlatTransportasi::hapus');
      });

      $routes->group('golpang', function($routes) {
         $routes->get('/', 'Golpang::index');

         $routes->post('submit', 'Golpang::submit');
         $routes->post('getdata', 'Golpang::getData');
         $routes->post('hapus', 'Golpang::hapus');
      });

      $routes->group('ikatankerja', function($routes) {
         $routes->get('/', 'IkatanKerja::index');

         $routes->post('submit', 'IkatanKerja::submit');
         $routes->post('getdata', 'IkatanKerja::getData');
         $routes->post('hapus', 'IkatanKerja::hapus');
      });

      $routes->group('jabatanfungsional', function($routes) {
         $routes->get('/', 'JabatanFungsional::index');

         $routes->post('submit', 'JabatanFungsional::submit');
         $routes->post('getdata', 'JabatanFungsional::getData');
         $routes->post('hapus', 'JabatanFungsional::hapus');
      });

      $routes->group('jalurmasuk', function($routes) {
         $routes->get('/', 'JalurMasuk::index');

         $routes->post('submit', 'JalurMasuk::submit');
         $routes->post('getdata', 'JalurMasuk::getData');
         $routes->post('hapus', 'JalurMasuk::hapus');
      });

      $routes->group('jenisaktivitasmahasiswa', function($routes) {
         $routes->get('/', 'JenisAktivitasMahasiswa::index');

         $routes->post('submit', 'JenisAktivitasMahasiswa::submit');
         $routes->post('getdata', 'JenisAktivitasMahasiswa::getData');
         $routes->post('hapus', 'JenisAktivitasMahasiswa::hapus');
      });

      $routes->group('jenisevaluasi', function($routes) {
         $routes->get('/', 'JenisEvaluasi::index');

         $routes->post('submit', 'JenisEvaluasi::submit');
         $routes->post('getdata', 'JenisEvaluasi::getData');
         $routes->post('hapus', 'JenisEvaluasi::hapus');
      });

      $routes->group('jeniskeluar', function($routes) {
         $routes->get('/', 'JenisKeluar::index');

         $routes->post('submit', 'JenisKeluar::submit');
         $routes->post('getdata', 'JenisKeluar::getData');
         $routes->post('hapus', 'JenisKeluar::hapus');
      });

      $routes->group('jenismatakuliah', function($routes) {
         $routes->get('/', 'JenisMatakuliah::index');

         $routes->post('submit', 'JenisMatakuliah::submit');
         $routes->post('getdata', 'JenisMatakuliah::getData');
         $routes->post('hapus', 'JenisMatakuliah::hapus');
      });

      $routes->group('jenispegawai', function($routes) {
         $routes->get('/', 'JenisPegawai::index');

         $routes->post('submit', 'JenisPegawai::submit');
         $routes->post('getdata', 'JenisPegawai::getData');
         $routes->post('hapus', 'JenisPegawai::hapus');
      });

      $routes->group('jenispendaftaran', function($routes) {
         $routes->get('/', 'JenisPendaftaran::index');

         $routes->post('submit', 'JenisPendaftaran::submit');
         $routes->post('getdata', 'JenisPendaftaran::getData');
         $routes->post('hapus', 'JenisPendaftaran::hapus');
      });

      $routes->group('jenisprestasi', function($routes) {
         $routes->get('/', 'JenisPrestasi::index');

         $routes->post('submit', 'JenisPrestasi::submit');
         $routes->post('getdata', 'JenisPrestasi::getData');
         $routes->post('hapus', 'JenisPrestasi::hapus');
      });

      $routes->group('tingkatprestasi', function($routes) {
         $routes->get('/', 'TingkatPrestasi::index');

         $routes->post('submit', 'TingkatPrestasi::submit');
         $routes->post('getdata', 'TingkatPrestasi::getData');
         $routes->post('hapus', 'TingkatPrestasi::hapus');
      });

      $routes->group('jenissubstansi', function($routes) {
         $routes->get('/', 'JenisSubstansi::index');

         $routes->post('submit', 'JenisSubstansi::submit');
         $routes->post('getdata', 'JenisSubstansi::getData');
         $routes->post('hapus', 'JenisSubstansi::hapus');
      });

      $routes->group('jenistinggal', function($routes) {
         $routes->get('/', 'JenisTinggal::index');

         $routes->post('submit', 'JenisTinggal::submit');
         $routes->post('getdata', 'JenisTinggal::getData');
         $routes->post('hapus', 'JenisTinggal::hapus');
      });

      $routes->group('kategorikegiatan', function($routes) {
         $routes->get('/', 'KategoriKegiatan::index');

         $routes->post('submit', 'KategoriKegiatan::submit');
         $routes->post('getdata', 'KategoriKegiatan::getData');
         $routes->post('hapus', 'KategoriKegiatan::hapus');
      });

      $routes->group('kebutuhankhusus', function($routes) {
         $routes->get('/', 'KebutuhanKhusus::index');

         $routes->post('submit', 'KebutuhanKhusus::submit');
         $routes->post('getdata', 'KebutuhanKhusus::getData');
         $routes->post('hapus', 'KebutuhanKhusus::hapus');
      });

      $routes->group('kelompokmatakuliah', function($routes) {
         $routes->get('/', 'KelompokMatakuliah::index');

         $routes->post('submit', 'KelompokMatakuliah::submit');
         $routes->post('getdata', 'KelompokMatakuliah::getData');
         $routes->post('hapus', 'KelompokMatakuliah::hapus');
      });

      $routes->group('lembagapengangkat', function($routes) {
         $routes->get('/', 'LembagaPengangkat::index');

         $routes->post('submit', 'LembagaPengangkat::submit');
         $routes->post('getdata', 'LembagaPengangkat::getData');
         $routes->post('hapus', 'LembagaPengangkat::hapus');
      });

      $routes->group('modekuliah', function($routes) {
         $routes->get('/', 'ModeKuliah::index');

         $routes->post('submit', 'ModeKuliah::submit');
         $routes->post('getdata', 'ModeKuliah::getData');
         $routes->post('hapus', 'ModeKuliah::hapus');
      });

      $routes->group('modelpenilaian', function($routes) {
         $routes->get('/', 'ModelPenilaian::index');

         $routes->post('submit', 'ModelPenilaian::submit');
         $routes->post('getdata', 'ModelPenilaian::getData');
         $routes->post('hapus', 'ModelPenilaian::hapus');
      });

      $routes->group('negara', function($routes) {
         $routes->get('/', 'Negara::index');

         $routes->post('submit', 'Negara::submit');
         $routes->post('getdata', 'Negara::getData');
         $routes->post('hapus', 'Negara::hapus');
      });

      $routes->group('wilayah', function($routes) {
         $routes->get('/', 'Wilayah::index');

         $routes->post('submit', 'Wilayah::submit');
         $routes->post('getdata', 'Wilayah::getData');
         $routes->post('hapus', 'Wilayah::hapus');
      });

      $routes->group('nilaihuruf', function($routes) {
         $routes->get('/', 'NilaiHuruf::index');

         $routes->post('submit', 'NilaiHuruf::submit');
         $routes->post('getdata', 'NilaiHuruf::getData');
         $routes->post('hapus', 'NilaiHuruf::hapus');
      });

      $routes->group('pekerjaan', function($routes) {
         $routes->get('/', 'Pekerjaan::index');

         $routes->post('submit', 'Pekerjaan::submit');
         $routes->post('getdata', 'Pekerjaan::getData');
         $routes->post('hapus', 'Pekerjaan::hapus');
      });

      $routes->group('pembiayaan', function($routes) {
         $routes->get('/', 'Pembiayaan::index');

         $routes->post('submit', 'Pembiayaan::submit');
         $routes->post('getdata', 'Pembiayaan::getData');
         $routes->post('hapus', 'Pembiayaan::hapus');
      });

      $routes->group('penghasilan', function($routes) {
         $routes->get('/', 'Penghasilan::index');

         $routes->post('submit', 'Penghasilan::submit');
         $routes->post('getdata', 'Penghasilan::getData');
         $routes->post('hapus', 'Penghasilan::hapus');
      });

      $routes->group('statuskeaktifanpegawai', function($routes) {
         $routes->get('/', 'StatusKeaktifanPegawai::index');

         $routes->post('submit', 'StatusKeaktifanPegawai::submit');
         $routes->post('getdata', 'StatusKeaktifanPegawai::getData');
         $routes->post('hapus', 'StatusKeaktifanPegawai::hapus');
      });

      $routes->group('statuskepegawaian', function($routes) {
         $routes->get('/', 'StatusKepegawaian::index');

         $routes->post('submit', 'StatusKepegawaian::submit');
         $routes->post('getdata', 'StatusKepegawaian::getData');
         $routes->post('hapus', 'StatusKepegawaian::hapus');
      });

      $routes->group('statusmahasiswa', function($routes) {
         $routes->get('/', 'StatusMahasiswa::index');

         $routes->post('submit', 'StatusMahasiswa::submit');
         $routes->post('getdata', 'StatusMahasiswa::getData');
         $routes->post('hapus', 'StatusMahasiswa::hapus');
      });
   });

   $routes->group('perkuliahan', ['namespace' => 'App\Controllers\Admin\Perkuliahan'], function($routes) {
      $routes->group('matakuliah', function($routes) {
         $routes->get('/', 'Matakuliah::index');
         $routes->get('getdropdownlist', 'Matakuliah::getDropdownList');

         $routes->post('getdata', 'Matakuliah::getData');
         $routes->post('submit', 'Matakuliah::submit');
         $routes->post('hapus', 'Matakuliah::hapus');
      });

      $routes->group('substansikuliah', function($routes) {
         $routes->get('/', 'SubstansiKuliah::index');
         $routes->get('getdropdownlist', 'SubstansiKuliah::getDropdownList');

         $routes->post('submit', 'SubstansiKuliah::submit');
         $routes->post('getdata', 'SubstansiKuliah::getData');
         $routes->post('hapus', 'SubstansiKuliah::hapus');
      });

      $routes->group('kurikulum', function($routes) {
         $routes->get('/', 'Kurikulum::index');
         $routes->get('getdropdownlist', 'Kurikulum::getDropdownList');

         $routes->post('submit', 'Kurikulum::submit');
         $routes->post('getdata', 'Kurikulum::getData');
         $routes->post('hapus', 'Kurikulum::hapus');
         $routes->post('getdaftarmatakuliah', 'Kurikulum::getDaftarMatakuliah');
         $routes->post('submitmatkulkurikulum', 'Kurikulum::submitMatkulKurikulum');
         $routes->post('getdaftarmatkulkurikulum', 'Kurikulum::getDaftarMatkulKurikulum');
         $routes->post('hapusmatkulkurikulum', 'Kurikulum::hapusMatkulKurikulum');
         $routes->post('submitmatkulbersyarat', 'Kurikulum::submitMatkulBersyarat');
         $routes->post('getdaftarkurikulumprodi', 'Kurikulum::getDaftarKurikulumProdi');
         $routes->post('submitsalinmatkulkurikulum', 'Kurikulum::submitSalinMatkulKurikulum');
         $routes->post('submitapakahwajibmatkul', 'Kurikulum::submitApakahWajibMatkul');
      });
      
      $routes->group('kelaspekuliahan', function($routes) {
         $routes->get('/', 'KelasPerkuliahan::index');
         $routes->get('getdropdownlist', 'KelasPerkuliahan::getDropdownList');

         $routes->post('submit', 'KelasPerkuliahan::submit');
         $routes->post('getdata', 'KelasPerkuliahan::getData');
         $routes->post('hapus', 'KelasPerkuliahan::hapus');
      });
   });
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
