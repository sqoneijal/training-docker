<!DOCTYPE html>
<html lang="id" translate="no">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title><?php echo $title;?></title>
   <?php
   echo css_tag(['https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700']);
   echo @$internalCss;
   echo css_tag([
      'assets/plugins/global/plugins.bundle.css',
      'assets/css/style.bundle.css',
   ]);
   ?>
</head>
<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled">
   <div id="root" class="d-flex flex-column flex-root"></div>
   <?php
   echo '<script>var segment = '.$segment.', content = '.@$content.';</script>';
   echo script_tag([
      'assets/plugins/global/plugins.bundle.js',
   ]);
   echo @$internalJs;
   echo $webpack_js;
   ?>
</body>
</html>