<?php

namespace App\Controllers;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Admin extends BaseController {

   protected $env = 'production';

   public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger) {
      parent::initController($request, $response, $logger);
   }

   public function template($content = []) {
      $internalCss[] = $this->internalCss($content);
		$internalJs[] = $this->internalJs($content);
		
		$data['title'] = $content['title'];
      $data['internalCss'] = css_tag($internalCss);
		$data['internalJs'] = script_tag($internalJs);
      $data['webpack_js'] = $this->generateWebpackJS();
      $data['segment'] = $this->setSegment();
      $data['content'] = json_encode(@$content['content'] ? $content['content'] : []);
      // $data['users'] = json_encode($this->getUsersLogin());
      // $data['manifest_decode'] = json_decode(file_get_contents(ROOTPATH . 'public/manifest.json'), true);

		echo view('AdminTemplate', $data);
   }

   protected function generateWebpackJS() {
      if ($this->env === 'development') {
         return script_tag([
            'http://localhost:8081/runtime.js',
            'http://localhost:8081/content.js',
         ]);
      } else {
         $path = ROOTPATH . 'public/bundle/admin/manifest.json';
         $manifest = fopen($path, "r") or die("Unable to open file!");
         $content = json_decode(fread($manifest, filesize($path)), true);
         unset($content['index.html']);

         $set = [];
         foreach ($content as $key => $val) {
            $set[$key] = str_replace('auto', '', $val);
         }

         $script_tag[] = base_url("bundle/admin/{$set['runtime.js']}");
         $script_tag[] = base_url("bundle/admin/{$set['content.js']}");

         return script_tag($script_tag);
      }
   }

}