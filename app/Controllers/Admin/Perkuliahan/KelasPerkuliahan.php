<?php

namespace App\Controllers\Admin\Perkuliahan;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Models\Common;
use App\Controllers\Admin as Controller;
use App\Models\Admin\Perkuliahan\KelasPerkuliahan as Model;

class KelasPerkuliahan extends Controller {

   public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger) {
      parent::initController($request, $response, $logger);
   }

   public function index() {
      /* $db = \Config\Database::connect();
      $db2 = \Config\Database::connect('siakad');

      $page = (int) $this->getVar['page'];
      $limit = 20;
      $offset = $page * $limit;

      $table = $db->table('tb_unit_kelas');
      $table->select('id, kode_unit');
      $table->where('tahun_ajaran', 2021);
      $table->where('id_semester', 2);
      $table->limit($limit, $offset);

      $get = $table->get();
      $result = $get->getResultArray();

      if (count($result) > 0) {
         $kode_unit = [];
         $id_unit_kelas = [];
         foreach ($result as $data) {
            $id_unit_kelas[$data['kode_unit']] = $data['id'];
            $kode_unit[] = $data['kode_unit'];
         }
   
         $table2 = $db2->table('tbl_uniting_matakuliah tum');
         $table2->select('tum.id_uniting, tkd.id_mk, tkd.id_kurikulum, case
         when tum.id_uniting like concat(\'%\', tum.kode_mk, \'%\') then \'false\'
         else \'true\'
      end as is_join');
         $table2->join('tbl_kurikulum_detail tkd', 'tkd.kode_mk = tum.kode_mk');
         $table2->whereIn('tum.id_uniting', $kode_unit);
   
         $get2 = $table2->get();
         $result2 = $get2->getResultArray();
         
         $content = [];
         foreach ($result2 as $data) {
            array_push($content, [
               'id_unit_kelas' => $id_unit_kelas[$data['id_uniting']],
               'id_matakuliah' => $data['id_mk'],
               'is_join' => ($data['is_join'] === 'false' ? false : true),
               'id_kurikulum' => $data['id_kurikulum'],
            ]);
         }
         
         echo '<pre>';
         print_r($kode_unit);
         print_r($content);
         echo '</pre>';
   
         if (count($content) > 0) {
            $db->table('tb_unit_kelas_matakuliah')->ignore(true)->insertBatch($content);
         }

         echo '<script>window.location.href = "http://localhost:8080/admin/perkuliahan/kelaspekuliahan?page='.($page + 1).'";</script>';
      }
      die(); */


      $common = new Common();

      $this->data = [
         'title' => 'Kelas Perkuliahan',
         'internalCss' => datatable['css'],
         'internalJs' => datatable['js'],
         'content' => [
            'daftarPeriode' => $common->getDaftarPeriodeAktif(),
         ]
      ];

      $this->template($this->data);
   }

   public function getDropdownList() {
      $common = new Common();
      $content = [
         'daftarPeriode' => $common->getDaftarPeriode(),
      ];
      return $this->response->setJSON($content);
   }

   public function getData() {
      $model = new Model();
      $query = $model->getData($this->getVar);
   
      $output = array(
         'draw'            => intval(@$this->post['draw']),
         'recordsTotal'    => intval($model->countData($this->getVar)),
         'recordsFiltered' => intval($model->filteredData($this->getVar)),
         'data'            => $query
      );
      return $this->response->setJSON($output);
   }

}