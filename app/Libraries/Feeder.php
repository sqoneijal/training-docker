<?php

namespace App\Libraries;

class Feeder {

   protected $headers = [];
   protected $baseURI;
   protected $token;

   public function __construct() {
      $this->baseURI = 'https://neofeeder2022.ar-raniry.ac.id/ws/live2.php';
      $this->headers = [
         'Accept' => '*/*',
         'User-Agent' => 'SIAKAD ('.site_url().')',
         'Content-Type' => 'application/json'
      ];
      $this->token = $this->getToken();
   }

   public function getToken() {
      $client = \Config\Services::curlrequest();
      $response = $client->request('POST', $this->baseURI, [
         'verify' => false,
         'delay' => 2000,
         'http_errors' => false,
         'headers' => $this->headers,
         'connect_timeout' => 0,
         'json' => [
            'act' => 'GetToken',
            'username' => '201011',
            'password' => '*pddiktiuinar'
         ]
      ]);

      $body = $response->getBody();
      $content = json_decode($body, true);

      return $content['data']['token'];
   }

   public function act($params = []) {
      $client = \Config\Services::curlrequest();
      $response = $client->request('POST', $this->baseURI, [
         'verify' => false,
         'delay' => 2000,
         'http_errors' => false,
         'headers' => $this->headers,
         'connect_timeout' => 0,
         'json' => array_merge(['token' => $this->token], $params)
      ]);

      $body = $response->getBody();
      $content = json_decode($body, true);

      return $content['data'];
   }

}