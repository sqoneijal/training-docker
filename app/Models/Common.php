<?php

namespace App\Models;

use CodeIgniter\Model;

class Common extends Model {

   protected $db;

   public function __construct() {
      $this->db = \Config\Database::connect();
   }

   public function getDaftarKurikulumProdi($id_prodi) {
      try {
         $table = $this->db->table('tb_kurikulum tk');
         $table->select('tk.id, tk.id_kurikulum as id_feeder, tk.nama, tk.tahun_ajaran, tk.id_semester, tk.jumlah_sks_lulus,
         tk.jumlah_sks_wajib, tk.jumlah_sks_pilihan');
         $table->where('tk.id_prodi', $id_prodi);
         $table->orderBy('tk.id');

         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
         
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getDaftarMatkulKurikulum($id_kurikulum) {
      try {
         $table = $this->db->table('tb_matkul_kurikulum tmk');
         $table->select('tmk.id, tm.kode as kode_matkul, tm.nama as nama_matkul, tm.sks_mata_kuliah, tm.sks_tatap_muka, tm.sks_praktek,
         tm.sks_praktek_lapangan, tm.sks_simulasi, tmk.semester, tmk.apakah_wajib, tmk.is_syncron_feeder, tmk.id_kurikulum, tmk.id_matkul,
         coalesce(tmb.matkul_bersyarat, \'[]\') as matkul_bersyarat');
         $table->join('tb_matakuliah tm', 'tm.id = tmk.id_matkul');
         $table->join('(select tmb2.id_matkul_kurikulum, tmb2.id_kurikulum,
            json_agg(json_build_object(
               \'id_kurikulum\', tmb2.id_kurikulum,
               \'id_matkul_kurikulum\', tmb2.id_matkul_kurikulum,
               \'id_matkul\', tmb2.id_matkul,
               \'kode_matkul\', tm2.kode,
               \'nama_matkul\', tm2.nama,
               \'sks_mata_kuliah\', tm2.sks_mata_kuliah,
               \'semester\', tmk2.semester
            )) as matkul_bersyarat
         from tb_matkul_bersyarat tmb2
         join tb_matakuliah tm2 on tm2.id = tmb2.id_matkul
         join tb_matkul_kurikulum tmk2 on tmk2.id_kurikulum = tmb2.id_kurikulum and tmk2.id_matkul = tmb2.id_matkul
         group by tmb2.id_matkul_kurikulum, tmb2.id_kurikulum) tmb', 'tmb.id_matkul_kurikulum = tmk.id and tmb.id_kurikulum = tmk.id_kurikulum', 'left');
         $table->where('tmk.id_kurikulum', $id_kurikulum);
         $table->orderBy('tmk.semester, tm.nama');

         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
         
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               if ($field === 'matkul_bersyarat') {
                  $response[$key][$field] = json_decode($val[$field], true);
               } else {
                  $response[$key][$field] = trim($val[$field]);
               }
            }
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getDaftarPeriode() {
      try {
         $table = $this->db->table('tb_mst_periode');
         $table->select('id, tahun_ajaran, id_semester, nama_semester');
         $table->orderBy('concat(tahun_ajaran, id_semester) desc');

         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
         
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getDaftarPeriodeAktif() {
      try {
         $table = $this->db->table('tb_mst_periode');
         $table->select('id, tahun_ajaran, id_semester, nama_semester');
         $table->where('periode_aktif', true);
         $table->orderBy('concat(tahun_ajaran, id_semester)');

         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
         
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getDaftarProdi() {
      try {
         $table = $this->db->table('tb_mst_prodi tmp');
         $table->select('tmp.id, concat(tmjp.nama, \' \', tmp.nama) as nama');
         $table->join('tb_mst_jenjang_pend tmjp', 'tmjp.id = tmp.id_jenjang_pendidikan');
         $table->orderBy('nama');

         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
         
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getDaftarKelompokMatakuliah() {
      try {
         $table = $this->db->table('tb_mst_kelompok_matkul');
         $table->select('id, nama, keterangan');
         $table->orderBy('nama');

         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
         
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getDaftarModelPenilaian() {
      try {
         $table = $this->db->table('tb_mst_model_penilaian');
         $table->select('id, nama, keterangan');
         $table->orderBy('nama');

         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
         
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getDaftarJenisMatakuliah() {
      try {
         $table = $this->db->table('tb_mst_jenis_matkul');
         $table->select('id, nama');
         $table->orderBy('nama');

         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
         
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getDaftarFakultas() {
      try {
         $table = $this->db->table('tb_mst_fakultas');
         $table->select('id, nama');
         $table->orderBy('nama');

         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
         
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getJenisJenjangPendidikan() {
      try {
         $table = $this->db->table('tb_mst_jenjang_pend');
         $table->orderBy('id');

         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
         
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

}