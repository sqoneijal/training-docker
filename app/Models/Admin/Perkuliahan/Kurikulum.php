<?php

namespace App\Models\Admin\Perkuliahan;

use CodeIgniter\Model;

class Kurikulum extends Model {

   protected $db;

   public function __construct() {
      $this->db = \Config\Database::connect();
   }

   public function submitApakahWajibMatkul($post = []) {
      try {
         $table = $this->db->table('tb_matkul_kurikulum');
         $table->where('id', $post['id_matkul_kurikulum']);
         $table->update([
            'syncron_ulang' => true,
            'apakah_wajib' => $post['apakah_wajib'],
            'modified' => date('Y-m-d H:i:s'),
            'user_modified' => @$post['user_modified'],
         ]);

         return ['status' => true, 'msg_response' => 'Data berhasil diperbaharui.', 'content' => $this->getDetailRowKurikulum($post['id_kurikulum'])];
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function submitSalinMatkulKurikulum($post = []) {
      try {
         $content = json_decode($post['content'], true);

         if (count($content) > 0) {
            $table = $this->db->table('tb_matkul_kurikulum');
            $table->ignore(true)->insertBatch($content);

            return ['status' => true, 'msg_response' => 'Salin matakuliah kurikulum berhasil dilakukan.'];
         } else {
            return ['status' => false, 'msg_response' => 'Setidaknya anda harus memilih satu (1) matakuliah.'];
         }
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function submitMatkulBersyarat($post = []) {
      try {
         $id_matkul = json_decode($post['id_matkul'], true);

         if (count($id_matkul) > 0) {
            $checkExistMatkulBersyarat = $this->checkExistMatkulBersyarat($post['id_kurikulum'], $post['id_matkul_kurikulum']);

            $delete = [];
            foreach ($checkExistMatkulBersyarat as $id) {
               if (!in_array($id, $id_matkul)) {
                  $delete[] = $id;
               }
            }

            if (count($delete) > 0) {
               $this->hapusMatkulBersyarat([
                  'id_kurikulum' => $post['id_kurikulum'],
                  'id_matkul_kurikulum' => $post['id_matkul_kurikulum'],
                  'id_matkul' => $delete
               ]);
            }

            $content = [];
            foreach ($id_matkul as $id) {
               if (!in_array($id, $checkExistMatkulBersyarat)) {
                  array_push($content, [
                     'id_kurikulum' => $post['id_kurikulum'],
                     'id_matkul_kurikulum' => $post['id_matkul_kurikulum'],
                     'id_matkul' => $id,
                     'uploaded' => date('Y-m-d H:i:s'),
                     'user_modified' => @$post['user_modified']
                  ]);
               }
            }

            if (count($content) > 0) {
               $table = $this->db->table('tb_matkul_bersyarat');
               $table->insertBatch($content);
            }
         }
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function hapusMatkulBersyarat($post = []) {
      try {
         $table = $this->db->table('tb_matkul_bersyarat');
         $table->where('id_kurikulum', $post['id_kurikulum']);
         $table->where('id_matkul_kurikulum', $post['id_matkul_kurikulum']);
         $table->whereIn('id_matkul', $post['id_matkul']);
         $table->delete();
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function checkExistMatkulBersyarat($id_kurikulum, $id_matkul_kurikulum) {
      try {
         $table = $this->db->table('tb_matkul_bersyarat');
         $table->select('id_matkul');
         $table->where('id_kurikulum', $id_kurikulum);
         $table->where('id_matkul_kurikulum', $id_matkul_kurikulum);

         $get = $table->get();
         $result = $get->getResultArray();
         
         $response = [];
         foreach ($result as $data) {
            $response[] = $data['id_matkul'];
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function hapusMatkulKurikulum($post = []) {
      try {
         $table = $this->db->table('tb_matkul_kurikulum');
         $table->where('id', $post['id']);
         $table->delete();

         return $this->getDetailRowKurikulum($post['id_kurikulum']);
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function submitMatkulKurikulum($post = []) {
      try {
         $content = json_decode($post['content'], true);

         if (count($content) > 0) {
            $table = $this->db->table('tb_matkul_kurikulum');
            $table->insertBatch($content);
         }

         return [
            'status' => true,
            'msg_response' => 'Data berhasil disimpan.',
            'content' => $this->getDetailRowKurikulum($content[0]['id_kurikulum'])
         ];
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getDetailRowKurikulum($id_kurikulum) {
      try {
         $table = $this->db->table('tb_kurikulum tk');
         $table->select('tk.id, tk.tahun_ajaran, tk.id_semester, tk.nama, tk.id_prodi, concat(tmjp.nama, \' \', tmp.nama) as nama_prodi,
         tk.jumlah_sks_wajib, tk.jumlah_sks_pilihan, concat(tk.tahun_ajaran, tk.id_semester) as periode, tk.id_kurikulum');
         $table->join('tb_mst_prodi tmp', 'tmp.id = tk.id_prodi');
         $table->join('tb_mst_jenjang_pend tmjp', 'tmjp.id = tmp.id_jenjang_pendidikan');
         $table->where('tk.id', $id_kurikulum);

         $get = $table->get();
         $data = $get->getRowArray();
         $fieldNames = $get->getFieldNames();
         
         $response = [];
         if (isset($data)) {
            foreach ($fieldNames as $field) {
               $response[$field] = trim($data[$field]);
            }
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getDaftarMatakuliah($post = []) {
      try {
         $table = $this->db->table('tb_matakuliah tm');
         $table->select('tm.id, tm.kode, tm.nama, tm.sks_mata_kuliah, tmjm.nama as nama_jenis_matkul, tm.sks_tatap_muka, tm.sks_praktek,
         tm.sks_praktek_lapangan, tm.sks_simulasi, tm.id_jenis_mata_kuliah, tm.tanggal_mulai_efektif, tm.tanggal_akhir_efektif,
         tm.metode_kuliah, tm.id_model_nilai, tm.id_kelompok_mata_kuliah, tm.id_matkul, tmmp.nama as nama_model_penilaian');
         $table->join('tb_mst_jenis_matkul tmjm', 'tmjm.id = tm.id_jenis_mata_kuliah', 'left');
         $table->join('tb_mst_model_penilaian tmmp', 'tmmp.id = tm.id_model_nilai', 'left');
         $table->whereNotIn('tm.id', function($table) use ($post) {
            return $table->select('id_matkul')
               ->from('tb_matkul_kurikulum')
               ->where('id_kurikulum', $post['id_kurikulum']);
         });
         if (@$post['cari']) {
            $table->groupStart();
            $table->like('trim(lower(tm.kode))', trim(strtolower($post['cari'])));
            $table->orLike('trim(lower(tm.nama))', trim(strtolower($post['cari'])));
            $table->groupEnd();
         }
         $table->limit(20);

         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
         
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function hapus($post = []) {
      try {
         $table = $this->db->table('tb_kurikulum');
         $table->where('id', $post['id']);
         $table->delete();
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function submit($post = []) {
      try {
         $data = [];
         foreach ($post as $key => $val) {
            if ($val) $data[$key] = trim($val);
            else $data[$key] = is_numeric($val) ? $val : null;
         }
         unset($data['pageType'], $data['id']);

         $data['modified'] = date('Y-m-d H:i:s');
         $data['syncron_feeder'] = false;

         $table = $this->db->table('tb_kurikulum');
         if ($post['pageType'] === 'insert') {
            $data['uploaded'] = date('Y-m-d H:i:s');

            $table->insert($data);
         } else if ($post['pageType'] === 'update') {
            $table->where('id', $post['id']);
            $table->update($data);
         }
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getData($post = []) {
      try {
         $table = $this->_queryData($post);
         if ($post['length'] !== -1)
            $table->limit($post['length'], $post['start']);
   
         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
   
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch(\Exception $e) {
         die($e->getMessage());
      }
   }
   
   public function countData($post = []) {
      $table = $this->db->table('tb_kurikulum tk');
      $table->selectCount('*', 'jumlah');
      $table->join('tb_mst_prodi tmp', 'tmp.id = tk.id_prodi');
      $table->join('tb_mst_jenjang_pend tmjp', 'tmjp.id = tmp.id_jenjang_pendidikan');
   
      $get = $table->get();
      $data = $get->getRowArray();
   
      if (isset($data)) return $data['jumlah'];
      return 0;
   }
   
   public function filteredData($post = []) {
      $table = $this->_queryData($post);
      $get = $table->get();
      return count($get->getResultArray());
   }
   
   private function _queryData($post = []) {
      $table = $this->db->table('tb_kurikulum tk');
      $table->select('tk.id, tk.tahun_ajaran, tk.id_semester, tk.nama, tk.id_prodi, concat(tmjp.nama, \' \', tmp.nama) as nama_prodi,
      tk.jumlah_sks_wajib, tk.jumlah_sks_pilihan, (tk.jumlah_sks_wajib + tk.jumlah_sks_pilihan) as sks_lulus,
      concat(tk.tahun_ajaran, tk.id_semester) as periode, tk.id_kurikulum');
      $table->join('tb_mst_prodi tmp', 'tmp.id = tk.id_prodi');
      $table->join('tb_mst_jenjang_pend tmjp', 'tmjp.id = tmp.id_jenjang_pendidikan');
      if (@$post['id_prodi']) $table->where('tk.id_prodi', $post['id_prodi']);
      if (@$post['mulai_berlaku']) $table->where('concat(tk.tahun_ajaran, tk.id_semester)', $post['mulai_berlaku']);
   
      $i = 0;
      $column_search = ['tmjp.nama', 'tmp.nama', 'tk.nama'];
      foreach ($column_search as $item) {
         if (@$_POST['search']['value']) {
            if ($i === 0) {
               $table->groupStart();
               $table->like('trim(lower(cast('.$item.' as varchar)))', trim(strtolower($_POST['search']['value'])));
            } else {
               $table->orLike('trim(lower(cast('.$item.' as varchar)))', trim(strtolower($_POST['search']['value'])));
            }
   
            if (count($column_search) - 1 === $i)
               $table->groupEnd();
         }
         $i++;
      }
   
      $column_order = ['nama', 'nama_prodi', 'periode', 'jumlah_sks_wajib', 'jumlah_sks_pilihan', 'sks_lulus'];
      $column = @$_POST['order'][0]['column'];
      $dir = @$_POST['order'][0]['dir'];
      $table->orderBy($column_order[$column], $dir);
   
      return $table;
   }

}