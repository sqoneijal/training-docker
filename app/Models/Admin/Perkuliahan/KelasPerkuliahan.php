<?php

namespace App\Models\Admin\Perkuliahan;

use CodeIgniter\Model;

class KelasPerkuliahan extends Model {

   protected $db;

   public function __construct() {
      $this->db = \Config\Database::connect();
   }

   public function getData($post = []) {
      try {
         $table = $this->_queryData($post);
         if ($post['length'] !== -1)
            $table->limit($post['length'], $post['start']);
   
         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
   
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch(\Exception $e) {
         die($e->getMessage());
      }
   }
   
   public function countData($post = []) {
      $table = $this->db->table('tb_unit_kelas tuk');
      $table->selectCount('*', 'jumlah');
      // if (@$post['semester']) $table->where('concat(tuk.tahun_ajaran, tuk.id_semester)', $post['semester']);
   
      $get = $table->get();
      $data = $get->getRowArray();
   
      if (isset($data)) return $data['jumlah'];
      return 0;
   }
   
   public function filteredData($post = []) {
      $table = $this->_queryData($post);
      $get = $table->get();
      return count($get->getResultArray());
   }
   
   private function _queryData($post = []) {
      $table = $this->db->table('tb_unit_kelas tuk');
      $table->select('tuk.id, tuk.tahun_ajaran, tuk.id_semester, tm.kode as kode_matkul, tm.nama as nama_matkul, tuk.kode_unit,
      tm.sks_mata_kuliah, nama_lengkap(td.gelar_depan, td.nama, td.gelar_belakang) as dosen_pengajar, tuk.terisi as peserta_kelas,
      concat(tuk.tahun_ajaran, tuk.id_semester) as periode');
      $table->join('(select id_unit_kelas, id_matakuliah from tb_unit_kelas_matakuliah where is_join = false) tukm', 'tukm.id_unit_kelas = tuk.id', 'left');
      $table->join('tb_matakuliah tm', 'tm.id = tukm.id_matakuliah', 'left');
      $table->join('tb_roster tr', 'tr.id_unit_kelas = tuk.id', 'left');
      $table->join('tb_dosen td', 'td.id = tr.id_dosen', 'left');
      // if (@$post['semester']) $table->where('concat(tuk.tahun_ajaran, tuk.id_semester)', $post['semester']);
   
      $i = 0;
      $column_search = ['tm.kode', 'tm.nama', 'tuk.kode_unit', 'td.nama', 'td.nidn'];
      foreach ($column_search as $item) {
         if (@$_POST['search']['value']) {
            if ($i === 0) {
               $table->groupStart();
               $table->like('trim(lower(cast('.$item.' as varchar)))', trim(strtolower($_POST['search']['value'])));
            } else {
               $table->orLike('trim(lower(cast('.$item.' as varchar)))', trim(strtolower($_POST['search']['value'])));
            }
   
            if (count($column_search) - 1 === $i)
               $table->groupEnd();
         }
         $i++;
      }
   
      $column_order = ['periode', 'kode_unit', 'kode_matkul', 'nama_matkul', 'sks_mata_kuliah', 'dosen_pengajar', 'peserta_kelas'];
      $column = @$_POST['order'][0]['column'];
      $dir = @$_POST['order'][0]['dir'];
      $table->orderBy($column_order[$column], $dir);
   
      return $table;
   }

}