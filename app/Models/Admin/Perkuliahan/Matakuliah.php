<?php

namespace App\Models\Admin\Perkuliahan;

use CodeIgniter\Model;

class Matakuliah extends Model {

   protected $db;

   public function __construct() {
      $this->db = \Config\Database::connect();
   }

   public function hapus($post = []) {
      try {
         $table = $this->db->table('tb_matakuliah');
         $table->where('id', $post['id']);
         $table->delete();
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function submit($post = []) {
      try {
         $data = [];
         foreach ($post as $key => $val) {
            if ($val) $data[$key] = trim($val);
            else $data[$key] = is_numeric($val) ? $val : null;
         }
         unset($data['pageType'], $data['id']);

         $data['modified'] = date('Y-m-d H:i:s');
         $data['syncron_feeder'] = false;

         $table = $this->db->table('tb_matakuliah');
         if ($post['pageType'] === 'insert') {
            $data['uploaded'] = date('Y-m-d H:i:s');

            $table->insert($data);
         } else if ($post['pageType'] === 'update') {
            $table->where('id', $post['id']);
            $table->update($data);
         }
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getData($post = []) {
      try {
         $table = $this->_queryData($post);
         if ($post['length'] !== -1)
            $table->limit($post['length'], $post['start']);
   
         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
   
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch(\Exception $e) {
         die($e->getMessage());
      }
   }
   
   public function countData($post = []) {
      $table = $this->db->table('tb_matakuliah tm');
      $table->selectCount('*', 'jumlah');
   
      $get = $table->get();
      $data = $get->getRowArray();
   
      if (isset($data)) return $data['jumlah'];
      return 0;
   }
   
   public function filteredData($post = []) {
      $table = $this->_queryData($post);
      $get = $table->get();
      return count($get->getResultArray());
   }
   
   private function _queryData($post = []) {
      $table = $this->db->table('tb_matakuliah tm');
      $table->select('tm.id, tm.kode, tm.nama, tm.sks_mata_kuliah, tmjm.nama as nama_jenis_matkul, tm.sks_tatap_muka, tm.sks_praktek,
      tm.sks_praktek_lapangan, tm.sks_simulasi, tm.id_jenis_mata_kuliah, tm.tanggal_mulai_efektif, tm.tanggal_akhir_efektif,
      tm.metode_kuliah, tm.id_model_nilai, tm.id_kelompok_mata_kuliah, tm.id_matkul, tmmp.nama as nama_model_penilaian');
      $table->join('tb_mst_jenis_matkul tmjm', 'tmjm.id = tm.id_jenis_mata_kuliah', 'left');
      $table->join('tb_mst_model_penilaian tmmp', 'tmmp.id = tm.id_model_nilai', 'left');
      if (@$post['id_jenis_mata_kuliah']) $table->where('tm.id_jenis_mata_kuliah', $post['id_jenis_mata_kuliah']);
      if (@$post['id_kelompok_mata_kuliah']) $table->where('tm.id_kelompok_mata_kuliah', $post['id_kelompok_mata_kuliah']);
      if (@$post['id_model_nilai']) $table->where('tm.id_model_nilai', $post['id_model_nilai']);
   
      $i = 0;
      $column_search = ['tm.kode', 'tm.nama', 'tmjm.nama'];
      foreach ($column_search as $item) {
         if (@$_POST['search']['value']) {
            if ($i === 0) {
               $table->groupStart();
               $table->like('trim(lower(cast('.$item.' as varchar)))', trim(strtolower($_POST['search']['value'])));
            } else {
               $table->orLike('trim(lower(cast('.$item.' as varchar)))', trim(strtolower($_POST['search']['value'])));
            }
   
            if (count($column_search) - 1 === $i)
               $table->groupEnd();
         }
         $i++;
      }
   
      $column_order = ['kode', 'nama', 'sks_mata_kuliah', 'nama_jenis_matkul', 'id_model_nilai'];
      $column = @$_POST['order'][0]['column'];
      $dir = @$_POST['order'][0]['dir'];
      $table->orderBy($column_order[$column], $dir);
   
      return $table;
   }

}