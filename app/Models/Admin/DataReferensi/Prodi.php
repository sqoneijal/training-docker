<?php

namespace App\Models\Admin\DataReferensi;

use CodeIgniter\Model;

class Prodi extends Model {

   protected $db;

   public function __construct() {
      $this->db = \Config\Database::connect();
   }

   public function hapus($post = []) {
      try {
         $table = $this->db->table('tb_mst_prodi');
         $table->where('id', $post['id']);
         $table->delete();
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function submit($post = []) {
      try {
         $data = [];
         foreach ($post as $key => $val) {
            if ($val) $data[$key] = trim($val);
            else $data[$key] = is_numeric($val) ? $val : null;
         }
         unset($data['pageType'], $data['id']);

         $data['modified'] = date('Y-m-d H:i:s');

         $table = $this->db->table('tb_mst_prodi');
         if ($post['pageType'] === 'insert') {
            $data['uploaded'] = date('Y-m-d H:i:s');

            $table->insert($data);
         } else if ($post['pageType'] === 'update') {
            $table->where('id', $post['id']);
            $table->update($data);
         }
      } catch (\Exception $e) {
         die($e->getMessage());
      }
   }

   public function getData($post = []) {
      try {
         $table = $this->_queryData($post);
         if ($post['length'] !== -1)
            $table->limit($post['length'], $post['start']);
   
         $get = $table->get();
         $result = $get->getResultArray();
         $fieldNames = $get->getFieldNames();
   
         $response = [];
         foreach ($result as $key => $val) {
            foreach ($fieldNames as $field) {
               $response[$key][$field] = trim($val[$field]);
            }
         }
         return $response;
      } catch(\Exception $e) {
         die($e->getMessage());
      }
   }
   
   public function countData($post = []) {
      $table = $this->db->table('tb_mst_prodi tmp');
      $table->selectCount('*', 'jumlah');
      $table->join('tb_mst_jenjang_pend tmjp', 'tmjp.id = tmp.id_jenjang_pendidikan');
   
      $get = $table->get();
      $data = $get->getRowArray();
   
      if (isset($data)) return $data['jumlah'];
      return 0;
   }
   
   public function filteredData($post = []) {
      $table = $this->_queryData($post);
      $get = $table->get();
      return count($get->getResultArray());
   }
   
   private function _queryData($post = []) {
      $table = $this->db->table('tb_mst_prodi tmp');
      $table->select('tmp.id, tmp.singkatan, tmp.kode, tmp.nama, tmjp.nama as nama_jenjang_pend,
      coalesce(trp.jumlah, 0) as jml_mahasiswa, coalesce(td.jumlah, 0) as jml_dosen,
      nama_lengkap(td2.gelar_depan, td2.nama, td2.gelar_belakang) as ka_prodi, tmp.id_jenjang_pendidikan, tmp.id_kepala_prodi,
      tmp.id_jenjang_pendidikan, tmp.id_fakultas, tmf.nama as nama_fakultas, tmp.kelompok_ujian');
      $table->join('tb_mst_jenjang_pend tmjp', 'tmjp.id = tmp.id_jenjang_pendidikan', 'left');
      $table->join('(select id_prodi, count(*) as jumlah from tb_riwayat_pendidikan group by id_prodi) trp', 'trp.id_prodi = tmp.id', 'left');
      $table->join('(select id_prodi, count(*) as jumlah from tb_dosen group by id_prodi) td', 'td.id_prodi = tmp.id', 'left');
      $table->join('tb_dosen td2', 'td2.id = tmp.id_kepala_prodi', 'left');
      $table->join('tb_mst_fakultas tmf', 'tmf.id = tmp.id_fakultas', 'left');
      if (@$post['id_fakultas']) $table->where('tmp.id_fakultas', $post['id_fakultas']);
      if (@$post['id_jenjang_pendidikan']) $table->where('tmp.id_jenjang_pendidikan', $post['id_jenjang_pendidikan']);
   
      $i = 0;
      $column_search = ['tmp.singkatan', 'tmp.kode', 'tmp.nama', 'tmjp.nama', 'td2.nama', 'tmf.nama'];
      foreach ($column_search as $item) {
         if (@$_POST['search']['value']) {
            if ($i === 0) {
               $table->groupStart();
               $table->like('trim(lower(cast('.$item.' as varchar)))', trim(strtolower($_POST['search']['value'])));
            } else {
               $table->orLike('trim(lower(cast('.$item.' as varchar)))', trim(strtolower($_POST['search']['value'])));
            }
   
            if (count($column_search) - 1 === $i)
               $table->groupEnd();
         }
         $i++;
      }
   
      $column_order = ['singkatan', 'kode', 'nama', 'nama_jenjang_pend', 'jml_mahasiswa', 'jml_dosen', 'ka_prodi', 'nama_fakultas'];
      $column = @$_POST['order'][0]['column'];
      $dir = @$_POST['order'][0]['dir'];
      $table->orderBy($column_order[$column], $dir);
   
      return $table;
   }

}