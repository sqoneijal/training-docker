<?php

namespace App\Validation\Admin\DataReferensi;

class NilaiHuruf {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_nilai_huruf.nilai_huruf,id]',
            'label' => 'Nilai huruf'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'nilai_huruf' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|is_unique[tb_mst_nilai_huruf.lower(nilai_huruf),nilai_huruf]' : 'required|is_not_unique[tb_mst_nilai_huruf.lower(nilai_huruf),nilai_huruf]'),
            'label' => 'Nilai huruf',
            'errors' => [
               'is_unique' => 'Nilai huruf anda masukkan sudah terdaftar. Silahkan gunakan yang lain.'
            ],
         ],
         'nilai_mulai' => [
            'rules' => 'required|numeric',
            'label' => 'Nilai mulai'
         ],
         'nilai_sampai' => [
            'rules' => 'required|numeric',
            'label' => 'Nilai sampai'
         ],
         'bobot' => [
            'rules' => 'required|numeric',
            'label' => 'Bobot'
         ],
      ];
   }
   
}