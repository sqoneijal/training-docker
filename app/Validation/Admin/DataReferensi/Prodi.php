<?php

namespace App\Validation\Admin\DataReferensi;

class Prodi {
   
   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_prodi.id,id]',
            'label' => 'ID prodi'
         ]
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_prodi.id,id]'),
            'label' => 'ID prodi'
         ],
         'kode' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|is_unique[tb_mst_prodi.kode,kode]' : 'required|is_not_unique[tb_mst_prodi.kode,kode]'),
            'label' => 'Kode prodi',
            'errors' => [
               'is_unique' => 'Kode prodi anda masukkan sudah terdaftar, silahkan gunakan yang lain'
            ]
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama prodi'
         ],
         'id_jenjang_pendidikan' => [
            'rules' => 'required',
            'label' => 'Jenjang pendidikan prodi'
         ],
         'id_fakultas' => [
            'rules' => 'required',
            'label' => 'Fakultas'
         ],
         'singkatan' => [
            'rules' => 'required',
            'label' => 'Singkatan'
         ],
      ];
   }
   
}