<?php

namespace App\Validation\Admin\DataReferensi;

class StatusMahasiswa {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_status_mahasiswa.id,id]',
            'label' => 'ID status mahasiswa'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|is_unique[tb_mst_status_mahasiswa.lower(id),id]|max_length[1]' : 'required|is_not_unique[tb_mst_status_mahasiswa.lower(id),id]'),
            'label' => 'ID status mahasiswa',
            'errors' => [
               'is_unique' => 'ID status mahasiswa anda masukkan sudah terdaftar. Silahkan gunakan yang lain.'
            ]
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama status mahasiswa'
         ],
      ];
   }
   
}