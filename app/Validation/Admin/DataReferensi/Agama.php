<?php

namespace App\Validation\Admin\DataReferensi;

class Agama {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_agama.id,id]',
            'label' => 'ID agama'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_agama.id,id]'),
            'label' => 'ID agama'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama agama'
         ],
      ];
   }
   
}