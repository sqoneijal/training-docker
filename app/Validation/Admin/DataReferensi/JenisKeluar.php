<?php

namespace App\Validation\Admin\DataReferensi;

class JenisKeluar {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_jenis_keluar.id,id]',
            'label' => 'ID jenis keluar'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_jenis_keluar.id,id]'),
            'label' => 'ID jenis keluar'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama jenis keluar'
         ],
      ];
   }
   
}