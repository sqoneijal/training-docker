<?php

namespace App\Validation\Admin\DataReferensi;

class AlatTransportasi {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_alat_transportasi.id,id]',
            'label' => 'ID alat transportasi'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_alat_transportasi.id,id]'),
            'label' => 'ID alat transportasi'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama alat transportasi'
         ],
      ];
   }
   
}