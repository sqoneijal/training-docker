<?php

namespace App\Validation\Admin\DataReferensi;

class Pembiayaan {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_pembiayaan.id,id]',
            'label' => 'ID pembiayaan'
         ]
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_pembiayaan.id,id]'),
            'label' => 'ID pembiayaan'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama pembiayaan'
         ],
      ];
   }
   
}