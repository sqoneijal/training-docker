<?php

namespace App\Validation\Admin\DataReferensi;

class JenisEvaluasi {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_jenis_evaluasi.id,id]',
            'label' => 'ID jenis evaluasi'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_jenis_evaluasi.id,id]'),
            'label' => 'ID jenis evaluasi'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama jenis evaluasi'
         ],
      ];
   }
   
}