<?php

namespace App\Validation\Admin\DataReferensi;

class JenisJenjangPendidikan {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_jenjang_pend.id,id]',
            'label' => 'ID jenis jenjang pendidikan'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric'),
            'label' => 'ID jenis jenjang pendidikan'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama jenis jenjang pendidikan'
         ],
         'lama_kuliah' => [
            'rules' => 'permit_empty|numeric|greater_than[0]',
            'label' => 'Lama kuliah'
         ],
      ];
   }
   
}