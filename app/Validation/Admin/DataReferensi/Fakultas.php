<?php

namespace App\Validation\Admin\DataReferensi;

class Fakultas {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_fakultas.id,id]',
            'label' => 'ID fakultas'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|is_not_unique[tb_mst_fakultas.id,id]'),
            'label' => 'ID fakultas'
         ],
         'kode' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|numeric|exact_length[2]|is_unique[tb_mst_fakultas.kode,kode]' : 'required|numeric|exact_length[2]'),
            'label' => 'Kode fakultas'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama fakultas'
         ],
         'singkatan' => [
            'rules' => 'required',
            'label' => 'Singkatan fakultas'
         ],
      ];
   }
   
}