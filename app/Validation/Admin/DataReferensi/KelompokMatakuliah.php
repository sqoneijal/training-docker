<?php

namespace App\Validation\Admin\DataReferensi;

class KelompokMatakuliah {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_kelompok_matkul.id,id]',
            'label' => 'ID kelompok matakuliah'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|is_unique[tb_mst_kelompok_matkul.lower(id),id]' : 'required|is_not_unique[tb_mst_kelompok_matkul.lower(id),id]'),
            'label' => 'ID kelompok matakuliah',
            'errors' => [
               'is_unique' => 'ID kelompok matakuliah anda masukkan sudah terdaftar. Silahkan gunakan yang lain.'
            ],
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama kelompok matakuliah'
         ],
         'keterangan' => [
            'rules' => 'required',
            'label' => 'Keterangan kelompok matakuliah'
         ],
      ];
   }
   
}