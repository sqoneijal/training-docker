<?php

namespace App\Validation\Admin\DataReferensi;

class LembagaPengangkat {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_lembaga_pengangkat.id,id]',
            'label' => 'ID lembaga pengangkat'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|is_not_unique[tb_mst_lembaga_pengangkat.id,id]'),
            'label' => 'ID lembaga pengangkat',
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama lembaga pengangkat'
         ],
      ];
   }
   
}