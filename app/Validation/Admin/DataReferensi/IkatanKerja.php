<?php

namespace App\Validation\Admin\DataReferensi;

class IkatanKerja {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_ikatan_kerja.id,id]',
            'label' => 'ID ikatan kerja',
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|is_unique[tb_mst_ikatan_kerja.lower(id),id]' : 'required|is_not_unique[tb_mst_ikatan_kerja.lower(id),id]'),
            'label' => 'ID ikatan kerja',
            'errors' => [
               'is_unique' => 'ID ikatan kerja anda masukkan sudah terdaftar. Silahkan gunakan yang lain.'
            ]
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama ikatan kerja'
         ],
      ];
   }
   
}