<?php

namespace App\Validation\Admin\DataReferensi;

class Wilayah {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_wilayah.id,id]',
            'label' => 'ID wilayah'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|is_unique[tb_mst_wilayah.lower(id),id]' : 'required|is_not_unique[tb_mst_wilayah.lower(id),id]'),
            'label' => 'ID wilayah',
            'errors' => [
               'is_unique' => 'ID wilayah anda masukkan sudah terdaftar. Silahkan gunakan yang lain.'
            ]
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama wilayah'
         ],
      ];
   }
   
}