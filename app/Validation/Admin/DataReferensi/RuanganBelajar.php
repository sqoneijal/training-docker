<?php

namespace App\Validation\Admin\DataReferensi;

class RuanganBelajar {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_ruangan.id,id]',
            'label' => 'ID ruangan belajar'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_ruangan.id,id]'),
            'label' => 'ID ruangan belajar'
         ],
         'kode' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|is_unique[tb_mst_ruangan.kode,kode]' : 'required'),
            'label' => 'Kode ruangan belajar',
            'errors' => [
               'is_unique' => 'Kode ruangan anda masukkan sudah terdaftar. Silahkan gunakan yang lain.'
            ]
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama ruangan belajar'
         ],
         'id_fakultas' => [
            'rules' => 'required|numeric',
            'label' => 'Lokasi ruangan'
         ],
         'kapasitas' => [
            'rules' => 'required|numeric|greater_than[0]',
            'label' => 'Kapasitas ruangan'
         ],
      ];
   }
   
}