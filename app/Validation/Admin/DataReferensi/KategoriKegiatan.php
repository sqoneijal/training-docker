<?php

namespace App\Validation\Admin\DataReferensi;

class KategoriKegiatan {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_kategori_kegiatan.id,id]',
            'label' => 'ID kategori kegiatan'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|is_unique[tb_mst_kategori_kegiatan.lower(id),id]' : 'required|is_not_unique[tb_mst_kategori_kegiatan.lower(id),id]'),
            'label' => 'ID kategori kegiatan',
            'errors' => [
               'is_unique' => 'ID kategori kegiatan anda masukkan sudah terdaftar. Silahkan gunakan yang lain.'
            ]
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama kategori kegiatan'
         ],
      ];
   }
   
}