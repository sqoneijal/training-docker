<?php

namespace App\Validation\Admin\DataReferensi;

class Negara {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_negara.id,id]',
            'label' => 'ID negara'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|is_unique[tb_mst_negara.lower(id),id]' : 'required|is_not_unique[tb_mst_negara.lower(id),id]'),
            'label' => 'ID negara',
            'errors' => [
               'is_unique' => 'ID negara anda masukkan sudah terdaftar. Silahkan gunakan yang lain.'
            ],
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama negara'
         ],
      ];
   }
   
}