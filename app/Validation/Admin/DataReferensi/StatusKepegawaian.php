<?php

namespace App\Validation\Admin\DataReferensi;

class StatusKepegawaian {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_status_kepegawaian.id,id]',
            'label' => 'ID status kepegawaian'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_status_kepegawaian.id,id]'),
            'label' => 'ID status kepegawaian'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama status kepegawaian'
         ],
      ];
   }

}