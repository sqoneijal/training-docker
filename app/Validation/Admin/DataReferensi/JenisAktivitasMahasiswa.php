<?php

namespace App\Validation\Admin\DataReferensi;

class JenisAktivitasMahasiswa {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_jenis_aktivitas_mahasiswa.id,id]',
            'label' => 'ID jenis aktivitas'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_jenis_aktivitas_mahasiswa.id,id]'),
            'label' => 'ID jenis aktivitas'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama jenis aktivitas'
         ],
      ];
   }
   
}