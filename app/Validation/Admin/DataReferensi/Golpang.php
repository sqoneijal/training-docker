<?php

namespace App\Validation\Admin\DataReferensi;

class Golpang {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_golpang.id,id]',
            'label' => 'ID golongan pangkat'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_golpang.id,id]'),
            'label' => 'ID golongan pangkat'
         ],
         'kode' => [
            'rules' => 'required',
            'label' => 'Kode golongan'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama pangkat'
         ],
      ];
   }
   
}