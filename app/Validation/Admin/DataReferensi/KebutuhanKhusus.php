<?php

namespace App\Validation\Admin\DataReferensi;

class KebutuhanKhusus {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_kebutuhan_khusus.id,id]',
            'label' => 'ID kebutuhan khusus'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|is_not_unique[tb_mst_kebutuhan_khusus.id,id]'),
            'label' => 'ID kebutuhan khusus',
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama kebutuhan khusus'
         ],
      ];
   }
   
}