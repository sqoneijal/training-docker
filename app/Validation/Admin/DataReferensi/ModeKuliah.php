<?php

namespace App\Validation\Admin\DataReferensi;

class ModeKuliah {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_mode_kuliah.id,id]',
            'label' => 'ID mode kuliah'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|is_unique[tb_mst_mode_kuliah.lower(id),id]|max_length[1]' : 'required|is_not_unique[tb_mst_mode_kuliah.lower(id),id]'),
            'label' => 'ID mode kuliah',
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama mode kuliah'
         ],
      ];
   }
   
}