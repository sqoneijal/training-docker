<?php

namespace App\Validation\Admin\DataReferensi;

class JenisPegawai {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_jenis_pegawai.id,id]',
            'label' => 'ID jenis pegawai'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_jenis_pegawai.id,id]'),
            'label' => 'ID jenis pegawai'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama jenis pegawai'
         ],
      ];
   }
   
}