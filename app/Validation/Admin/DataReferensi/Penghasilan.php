<?php

namespace App\Validation\Admin\DataReferensi;

class Penghasilan {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_penghasilan.id,id]',
            'label' => 'ID penghasilan'
         ]
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_penghasilan.id,id]'),
            'label' => 'ID penghasilan'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama penghasilan'
         ],
      ];
   }
   
}