<?php

namespace App\Validation\Admin\DataReferensi;

class JenisSubstansi {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_jenis_substansi.id,id]',
            'label' => 'ID jenis substansi'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_jenis_substansi.id,id]'),
            'label' => 'ID jenis substansi'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama jenis substansi'
         ],
      ];
   }
   
}