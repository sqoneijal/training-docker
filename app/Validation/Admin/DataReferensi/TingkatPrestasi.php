<?php

namespace App\Validation\Admin\DataReferensi;

class TingkatPrestasi {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_tingkat_prestasi.id,id]',
            'label' => 'ID tingkat prestasi'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_tingkat_prestasi.id,id]'),
            'label' => 'ID tingkat prestasi'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama tingkat prestasi'
         ],
      ];
   }
   
}