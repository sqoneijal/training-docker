<?php

namespace App\Validation\Admin\DataReferensi;

class ModelPenilaian {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_model_penilaian.id,id]',
            'label' => 'ID model'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|is_not_unique[tb_mst_model_penilaian.id,id]'),
            'label' => 'ID model',
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama model'
         ],
         'keterangan' => [
            'rules' => 'required',
            'label' => 'Keterangan model'
         ],
      ];
   }
   
}