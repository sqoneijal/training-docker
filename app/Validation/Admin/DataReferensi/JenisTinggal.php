<?php

namespace App\Validation\Admin\DataReferensi;

class JenisTinggal {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_jenis_tinggal.id,id]',
            'label' => 'ID jenis tinggal'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_jenis_tinggal.id,id]'),
            'label' => 'ID jenis tinggal'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama jenis tinggal'
         ],
      ];
   }
   
}