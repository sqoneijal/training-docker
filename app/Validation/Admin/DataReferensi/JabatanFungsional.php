<?php

namespace App\Validation\Admin\DataReferensi;

class JabatanFungsional {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_jabatan_fungsional.id,id]',
            'label' => 'ID jabatan fungsional'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_jabatan_fungsional.id,id]'),
            'label' => 'ID jabatan fungsional'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama jabatan fungsional'
         ],
      ];
   }
   
}