<?php

namespace App\Validation\Admin\DataReferensi;

class JenisPendaftaran {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_jenis_pendaftaran.id,id]',
            'label' => 'ID jenis pendaftaran'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_jenis_pendaftaran.id,id]'),
            'label' => 'ID jenis pendaftaran'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama jenis pendaftaran'
         ],
      ];
   }
   
}