<?php

namespace App\Validation\Admin\DataReferensi;

class JenisPrestasi {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_jenis_prestasi.id,id]',
            'label' => 'ID jenis prestasi'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_jenis_prestasi.id,id]'),
            'label' => 'ID jenis prestasi'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama jenis prestasi'
         ],
      ];
   }
   
}