<?php

namespace App\Validation\Admin\DataReferensi;

class Pekerjaan {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_pekerjaan.id,id]',
            'label' => 'ID pekerjaan'
         ]
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_pekerjaan.id,id]'),
            'label' => 'ID pekerjaan'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama pekerjaan'
         ],
      ];
   }
   
}