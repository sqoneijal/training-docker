<?php

namespace App\Validation\Admin\DataReferensi;

class JenisMatakuliah {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|is_not_unique[tb_mst_jenis_matkul.id,id]',
            'label' => 'ID jenis matakuliah'
         ]
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|is_unique[tb_mst_jenis_matkul.lower(id),id]' : 'required|is_not_unique[tb_mst_jenis_matkul.lower(id),id]'),
            'label' => 'ID jenis matakuliah',
            'errors' => [
               'is_unique' => 'ID jenis matakuliah yang anda masukkan sudah terdaftar. Silahkan gunakan yang lain.'
            ],
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama jenis matakuliah'
         ],
      ];
   }
   
}