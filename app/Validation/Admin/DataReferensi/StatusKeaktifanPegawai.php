<?php

namespace App\Validation\Admin\DataReferensi;

class StatusKeaktifanPegawai {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_mst_status_keaktifan_pegawai.id,id]',
            'label' => 'ID keaktifan pegawai'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_mst_status_keaktifan_pegawai.id,id]'),
            'label' => 'ID keaktifan pegawai'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama keaktifan pegawai'
         ],
      ];
   }
   
}