<?php

namespace App\Validation\Admin\Perkuliahan;

class Matakuliah {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_matakuliah.id,id]'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_matakuliah.id,id]')
         ],
         'kode' => [
            'rules' => ($post['pageType'] === 'insert' ? 'required|is_unique[tb_matakuliah.kode,kode]' : 'required|is_not_unique[tb_matakuliah.kode,kode]'),
            'label' => 'Kode matakuliah',
            'errors' => [
               'is_unique' => 'Kode matakuliah anda masukkan sudah terdaftar. Silahkan gunakan yang lain.'
            ]
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama matakuliah'
         ],
         'id_jenis_mata_kuliah' => [
            'rules' => 'required',
            'label' => 'Jenis matakuliah'
         ],
         'id_kelompok_mata_kuliah' => [
            'rules' => 'required',
            'label' => 'Kelompok matakuliah'
         ],
         'sks_mata_kuliah' => [
            'rules' => 'required|numeric|greater_than[0]',
            'label' => 'Bobot matakuliah'
         ],
         'sks_tatap_muka' => [
            'rules' => 'permit_empty|numeric',
            'label' => 'Bobot tatap muka'
         ],
         'sks_praktek' => [
            'rules' => 'permit_empty|numeric',
            'label' => 'Bobot praktikum'
         ],
         'sks_praktek_lapangan' => [
            'rules' => 'permit_empty|numeric',
            'label' => 'Bobot praktek lapangan'
         ],
         'sks_simulasi' => [
            'rules' => 'permit_empty|numeric',
            'label' => 'Bobot simulasi'
         ],
         'tanggal_mulai_efektif' => [
            'rules' => 'permit_empty|valid_date[Y-m-d]',
            'label' => 'Tanggal mulai efektif'
         ],
         'tanggal_akhir_efektif' => [
            'rules' => 'permit_empty|valid_date[Y-m-d]',
            'label' => 'Tanggal akhir efektif'
         ],
         'id_model_nilai' => [
            'rules' => 'required|numeric',
            'label' => 'Model penilaian'
         ],
      ];
   }
   
}