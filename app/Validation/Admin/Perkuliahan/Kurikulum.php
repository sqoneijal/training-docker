<?php

namespace App\Validation\Admin\Perkuliahan;

class Kurikulum {

   public function hapusMatkulKurikulum() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_matkul_kurikulum.id,id]',
            'label' => 'ID matakuliah kurikulum'
         ],
      ];
   }

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_kurikulum.id,id]',
            'label' => 'ID kurikulum'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_kurikulum.id,id]'),
            'label' => 'ID kurikulum'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama kurikulum'
         ],
         'id_prodi' => [
            'rules' => 'required|numeric',
            'label' => 'Program studi'
         ],
         'tahun_ajaran' => [
            'rules' => 'required',
            'label' => 'Tahun ajaran'
         ],
         'id_semester' => [
            'rules' => 'required',
            'label' => 'ID semester'
         ],
      ];
   }
   
}