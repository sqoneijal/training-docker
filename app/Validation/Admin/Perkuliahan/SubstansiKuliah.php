<?php

namespace App\Validation\Admin\Perkuliahan;

class SubstansiKuliah {

   public function hapus() {
      return [
         'id' => [
            'rules' => 'required|numeric|is_not_unique[tb_substansi.id,id]',
            'label' => 'ID substansi kuliah'
         ],
      ];
   }

   public function submit($post = []) {
      return [
         'id' => [
            'rules' => ($post['pageType'] === 'insert' ? 'permit_empty' : 'required|numeric|is_not_unique[tb_substansi.id,id]'),
            'label' => 'ID substansi kuliah'
         ],
         'id_prodi' => [
            'rules' => 'required|numeric',
            'label' => 'Prodi'
         ],
         'nama' => [
            'rules' => 'required',
            'label' => 'Nama substansi'
         ],
         'sks_mata_kuliah' => [
            'rules' => 'required|numeric|greater_than[0]',
            'label' => 'Bobot sks matakuliah'
         ],
         'sks_tatap_muka' => [
            'rules' => 'permit_empty|numeric',
            'label' => 'Bobot sks tatap muka'
         ],
         'sks_praktek' => [
            'rules' => 'permit_empty|numeric',
            'label' => 'Bobot sks praktikum'
         ],
         'sks_praktek_lapangan' => [
            'rules' => 'permit_empty|numeric',
            'label' => 'Bobot sks praktek lapangan'
         ],
         'sks_simulasi' => [
            'rules' => 'permit_empty|numeric',
            'label' => 'Bobot sks simulasi'
         ],
      ];
   }
   
}