--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: after_delete_absensi(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_delete_absensi() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		delete from tb_jurnal_ajar where id_absensi = old.id;
		return old;
	END;
$$;


ALTER FUNCTION public.after_delete_absensi() OWNER TO postgres;

--
-- Name: after_delete_aktivitas_mahasiswa(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_delete_aktivitas_mahasiswa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		delete from tb_anggota_aktivitas_mahasiswa where id_aktivitas_mahasiswa = old.id;
		delete from tb_pembimbing where id_aktivitas_mahasiswa = old.id;
		delete from tb_penguji where id_aktivitas_mahasiswa = old.id;
		return old;
	END;
$$;


ALTER FUNCTION public.after_delete_aktivitas_mahasiswa() OWNER TO postgres;

--
-- Name: after_delete_krs_detail(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_delete_krs_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare jml_sks_dihapus numeric := 0;
	declare periode_aktif varchar;
	declare id_riwayat_mhs int;

	begin
		select
			concat(tk.tahun_ajaran, tk.id_semester)
			id_riwayat_pendidikan
			into
			periode_aktif,
			id_riwayat_mhs
		from tb_krs tk
		where tk.id = old.id_krs;
		
		delete from tb_unit_kelas_mahasiswa where id_krs_detail = old.id;
	
		select
			tm.sks_mata_kuliah
			into
			jml_sks_dihapus
		from tb_matakuliah tm where tm.id = old.id_matakuliah;
	
		update tb_akm
		set sks_semester = sks_semester - jml_sks_dihapus
		where id_riwayat_pendidikan = id_riwayat_mhs
			and concat(tahun_ajaran, id_semester) = periode_aktif;
	
		update tb_krs
		set sks_diambil = sks_diambil - jml_sks_dihapus
		where id = old.id_krs;
	
		return old;
	END;
$$;


ALTER FUNCTION public.after_delete_krs_detail() OWNER TO postgres;

--
-- Name: after_delete_lulus_dropout(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_delete_lulus_dropout() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	BEGIN
		update tb_riwayat_pendidikan set id_jenis_keluar = null
		where id = old.id_riwayat_pendidikan;
		return old;
	END;
$$;


ALTER FUNCTION public.after_delete_lulus_dropout() OWNER TO postgres;

--
-- Name: after_delete_unit_kelas_mahasiswa(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_delete_unit_kelas_mahasiswa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		update tb_unit_kelas set terisi = coalesce(terisi, 0) - 1
		where id = old.id_unit_kelas;
		
		return old;
	END;
$$;


ALTER FUNCTION public.after_delete_unit_kelas_mahasiswa() OWNER TO postgres;

--
-- Name: after_insert_akm(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_insert_akm() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		update tb_riwayat_pendidikan
		set id_status = new.id_status_mahasiswa
		where id = new.id_riwayat_pendidikan;
		return new;
	END;
$$;


ALTER FUNCTION public.after_insert_akm() OWNER TO postgres;

--
-- Name: after_insert_krs_detail(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_insert_krs_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare jumlah_sks_matakuliah numeric := 0;
	declare sks_diambil_sebelumnya numeric := 0;
	declare id_riwayat_mhs int;
	declare periode_aktif varchar;

	begin
		select
			tm.sks_mata_kuliah
			into
			jumlah_sks_matakuliah
		from tb_matakuliah tm
		where tm.id = new.id_matakuliah;
	
		select
			coalesce(tk.sks_diambil, 0),
			concat(tk.tahun_ajaran, tk.id_semester),
			tk.id_riwayat_pendidikan
			into
			sks_diambil_sebelumnya,
			periode_aktif,
			id_riwayat_mhs
		from tb_krs tk where tk.id = new.id_krs;
	
		update tb_krs set sks_diambil = sks_diambil_sebelumnya + jumlah_sks_matakuliah
		where id = new.id_krs;
	
		update tb_akm set sks_semester = sks_diambil_sebelumnya + jumlah_sks_matakuliah
		where id_riwayat_pendidikan = id_riwayat_mhs
			and concat(tahun_ajaran, id_semester) = periode_aktif;
		
		return new;
	END;
$$;


ALTER FUNCTION public.after_insert_krs_detail() OWNER TO postgres;

--
-- Name: after_insert_pembayaran(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_insert_pembayaran() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		if new.id_komponen_pembayaran = 2 then
			update tb_calon_mahasiswa set biaya_masuk = new.nilai_bayar where id = new.id_calon;
		end if;
		return new;
	END;
$$;


ALTER FUNCTION public.after_insert_pembayaran() OWNER TO postgres;

--
-- Name: after_insert_penugasan_dosen(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_insert_penugasan_dosen() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare id_prodi_penugasan numeric;
	declare id_penugasan_terbaru numeric;

	begin
		select max(id) into id_penugasan_terbaru from tb_penugasan_dosen where id_dosen = new.id_dosen;
		select id_mst_prodi into id_prodi_penugasan from tb_penugasan_dosen where id = id_penugasan_terbaru;
	
		update tb_dosen
		set id_prodi = id_prodi_penugasan
		where id = new.id_dosen;
		return new;
	END;
$$;


ALTER FUNCTION public.after_insert_penugasan_dosen() OWNER TO postgres;

--
-- Name: after_insert_peserta_pmb_lokal(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_insert_peserta_pmb_lokal() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare nomor_invoice numeric := 0;
	declare last_no_invoice numeric := 0;
	declare biaya_ipa numeric := 0;
	declare biaya_ips numeric := 0;
	declare biaya_ipc numeric := 0;
	declare nilai_bayar numeric := 0;

	begin
		select
			coalesce(max(tp.no_invoice), 0)
			into
			last_no_invoice
		from tb_pembayaran tp
		where tp.id_komponen_pembayaran = 2
			and concat(tp.tahun_ajaran, tp.id_semester) = cast(new.periode_masuk as varchar);
		
		select
			biaya_kelompok_ipa,
			biaya_kelompok_ips,
			biaya_kelompok_ipc
			into
			biaya_ipa,
			biaya_ips,
			biaya_ipc
		from tb_mst_komponen_pembayaran where id = 2;
	
		if last_no_invoice > 0 then
			nomor_invoice := last_no_invoice + 1;
		else
			nomor_invoice := concat('2', left(right(cast(new.periode_masuk as text), 3), 2), right(cast(new.periode_masuk as text), 1), '00001')::numeric;
		end if;
	
		if new.kelompok_studi = 'IPA' then
			nilai_bayar := biaya_ipa;
		elsif new.kelompok_studi = 'IPS' then
			nilai_bayar := biaya_ips;
		elsif new.kelompok_studi = 'IPC' then
			nilai_bayar := biaya_ipc;
		end if;
	
		INSERT INTO tb_pembayaran
		(sudah_bayar, nilai_bayar, tahun_ajaran, id_semester, id_komponen_pembayaran, uploaded, keterangan, no_invoice, id_calon)
		VALUES(false, nilai_bayar, left(cast(new.periode_masuk as text), 4)::numeric, right(cast(new.periode_masuk as text), 1)::numeric, 2, current_timestamp, 'Invoice pmb lokal', nomor_invoice, new.id_calon);
		return new;
	END;
$$;


ALTER FUNCTION public.after_insert_peserta_pmb_lokal() OWNER TO postgres;

--
-- Name: after_insert_unit_kelas_mahasiswa(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_insert_unit_kelas_mahasiswa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		update tb_unit_kelas set terisi = coalesce(terisi, 0) + 1
		where id = new.id_unit_kelas;
		
		return new;
	END;
$$;


ALTER FUNCTION public.after_insert_unit_kelas_mahasiswa() OWNER TO postgres;

--
-- Name: after_update_akm(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_update_akm() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare sks_total numeric := 0;

	begin
		select
			sum(sks_semester)
			into
			sks_total
		from tb_akm where id_riwayat_pendidikan = old.id_riwayat_pendidikan;
	
		new.total_sks := sks_total;
		new.ipk := hitung_ipk_akm(cast(old.id_riwayat_pendidikan as int));
		
		update tb_riwayat_pendidikan
		set id_status = new.id_status_mahasiswa
		where id = old.id_riwayat_pendidikan;
	
		return new;
	END;
$$;


ALTER FUNCTION public.after_update_akm() OWNER TO postgres;

--
-- Name: after_update_calon_mahasiswa(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_update_calon_mahasiswa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare last_no_invoice numeric := 0;
	declare nomor_invoice numeric := 0;
	declare apakah_jalur_pmb numeric := 0;
	declare apakah_sudah_bayar_spp_sebelumnya numeric := 0;

	begin
		select
			count(*) as jumlah
			into
			apakah_jalur_pmb
		from tb_calon_mahasiswa tcm
		join tb_mst_jalurmasuk_pmb tmjp on tmjp.id = tcm.id_jalurmasuk
		where tcm.no_pendaftaran = new.no_pendaftaran
			and tmjp.is_boleh_daftar = true;
		
		if apakah_jalur_pmb > 0 then
			update tb_peserta_pmb_lokal
			set sudah_pengumuman = true
			where periode_masuk  = new.periode_masuk
				and id_calon = new.id;
		end if;
	
		if new.complete_isi_biodata = true and new.status_approve = true and old.status_approve = false then
			select
				count(*)
				into
				apakah_sudah_bayar_spp_sebelumnya
			from tb_pembayaran tp
			where tp.id_komponen_pembayaran = 3
				and tp.id_calon = new.id
				and tp.sudah_bayar = true
				and concat(tp.tahun_ajaran, tp.id_semester)::text = cast(new.periode_masuk as text);
			
			if apakah_sudah_bayar_spp_sebelumnya < 1 then
				select
					coalesce(max(tp.no_invoice), 0)
					into
					last_no_invoice
				from tb_pembayaran tp
				where tp.id_komponen_pembayaran = 3
					and concat(tp.tahun_ajaran, tp.id_semester)::text = cast(new.periode_masuk as text);
				
				if last_no_invoice > 0 then
					nomor_invoice := last_no_invoice + 1;
				else
					nomor_invoice := concat('3', left(cast(new.periode_masuk as text), 2), right(cast(new.periode_masuk as text), 1), '00001')::numeric;
				end if;
			
				INSERT INTO tb_pembayaran
				(sudah_bayar, nilai_bayar, tahun_ajaran, id_semester, id_komponen_pembayaran, uploaded, keterangan, no_invoice, id_calon)
				VALUES(false, new.nilai_spp, left(cast(new.periode_masuk as text), 4)::numeric, right(cast(new.periode_masuk as text), 1)::numeric, 3, current_timestamp, 'Invoice mahasiswa baru', nomor_invoice, new.id);
			end if;
		end if;
		return new;
	END;
$$;


ALTER FUNCTION public.after_update_calon_mahasiswa() OWNER TO postgres;

--
-- Name: after_update_pembayaran(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_update_pembayaran() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare old_nim numeric := 0;
	declare new_nim varchar;
	declare last_no_pendaftaran numeric := 0;
	declare new_no_pendaftaran numeric := 0;
	declare kelompok_studi_calon varchar;
	declare id_jadwal_pmb numeric := 0;
	declare id_peserta_pmb numeric := 0;

	begin
		if new.id_komponen_pembayaran = 2 and new.sudah_bayar = true and old.sudah_bayar = false then
			select
				kelompok_studi,
				id
				into
				kelompok_studi_calon,
				id_peserta_pmb
			from tb_peserta_pmb_lokal
			where id_calon = new.id_calon
				and periode_masuk = cast(concat(new.tahun_ajaran, new.id_semester) as numeric);
		
			select
				coalesce(max(tcm.no_pendaftaran)::numeric, 0)
				into
				last_no_pendaftaran
			from tb_calon_mahasiswa tcm
			join tb_peserta_pmb_lokal tppl on tppl.id_calon = tcm.id and tppl.periode_masuk = tcm.periode_masuk
			where tcm.periode_masuk = cast(concat(new.tahun_ajaran, new.id_semester) as numeric)
				and tcm.id_jalurmasuk = (select id from tb_mst_jalurmasuk_pmb where is_boleh_daftar = true)
				and tppl.kelompok_studi = kelompok_studi_calon;
			
			if last_no_pendaftaran > 0 then
				new_no_pendaftaran := last_no_pendaftaran + 1;
			else
				if kelompok_studi_calon = 'IPA' then
					new_no_pendaftaran := concat(right(cast(new.tahun_ajaran as varchar), 2), cast(new.id_semester as varchar), '1', '0001');
				elsif kelompok_studi_calon = 'IPS' then
					new_no_pendaftaran := concat(right(cast(new.tahun_ajaran as varchar), 2), cast(new.id_semester as varchar), '2', '0001');
				elsif kelompok_studi_calon = 'IPC' then
					new_no_pendaftaran := concat(right(cast(new.tahun_ajaran as varchar), 2), cast(new.id_semester as varchar), '3', '0001');
				end if;
			end if;
		
			select
				tjup.id
				into
				id_jadwal_pmb
			from tb_jadwal_ujian_pmb tjup
			join tb_mst_ruangan_pmb tmrp on tmrp.id = tjup.id_ruangan
			left join (select id_jadwal, count(*) as terisi from tb_jadwal_ujian_peserta_pmb group by id_jadwal) tjupp on tjupp.id_jadwal = tjup.id
			where tjup.kelompok_studi = kelompok_studi_calon
				and tjup.tahun_ajaran = new.tahun_ajaran
				and tjup.id_semester = new.id_semester
				and tmrp.kapasitas > cast(coalesce(tjupp.terisi, 0) as numeric)
			order by tjup.id
			limit 1;
		
			insert into tb_jadwal_ujian_peserta_pmb (id_jadwal, id_peserta)
			values (id_jadwal_pmb, id_peserta_pmb);
			
			update tb_calon_mahasiswa
			set no_pendaftaran = new_no_pendaftaran
			where id = new.id_calon;
		end if;
		
		if new.id_komponen_pembayaran = 3 and new.sudah_bayar = true and old.sudah_bayar = false then
			select
				concat(right(concat(new.tahun_ajaran, new.id_semester), 2), tmf.kode, tmp.no_urut, '0001'),
				coalesce(max(nim::numeric), 0)
				into
				new_nim,
				old_nim
			from tb_calon_mahasiswa tcm
			join tb_mst_prodi tmp on tmp.id = tcm.id_prodi_lulus
			join tb_mst_fakultas tmf on tmf.id = tmp.id_fakultas
			where tcm.periode_masuk::text = concat(new.tahun_ajaran, new.id_semester)
				and tcm.id_prodi_lulus = (select id_prodi_lulus from tb_calon_mahasiswa where id = 4)
			group by tmf.kode, tmp.no_urut;
		
			if old_nim > 0 then
				update tb_calon_mahasiswa set nim = (old_nim::numeric + 1)::text where id = new.id_calon;
			else
				update tb_calon_mahasiswa set nim = new_nim::text where id = new.id_calon;
			end if;
		end if;
	
		if old.id_komponen_pembayaran = 1 and new.sudah_bayar = true and old.sudah_bayar = false then
			update tb_riwayat_pendidikan set id_status = 'A' where id = old.id_riwayat_pendidikan;
		end if;
		return new;
	END;
$$;


ALTER FUNCTION public.after_update_pembayaran() OWNER TO postgres;

--
-- Name: after_update_tb_matkul_kurikulum(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.after_update_tb_matkul_kurikulum() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare jumlah_sks_matakuliah numeric := 0;

	begin
		select
			tm.sks_mata_kuliah
			into
			jumlah_sks_matakuliah
		from tb_matakuliah tm
		where tm.id = old.id_matkul;
	
		if new.apakah_wajib = '1' then
			update tb_kurikulum
			set
				jumlah_sks_wajib = (jumlah_sks_wajib + jumlah_sks_matakuliah),
				jumlah_sks_pilihan = (jumlah_sks_pilihan - jumlah_sks_matakuliah)
			where id = new.id_kurikulum;
		else
			update tb_kurikulum set
			jumlah_sks_wajib = (jumlah_sks_wajib - jumlah_sks_matakuliah),
			jumlah_sks_pilihan = (jumlah_sks_pilihan + jumlah_sks_matakuliah)
			where id = new.id_kurikulum;
		end if;
		
		return new;
	END;
$$;


ALTER FUNCTION public.after_update_tb_matkul_kurikulum() OWNER TO postgres;

--
-- Name: alamat_kbbi(character varying, numeric, numeric, character varying, character varying, character varying, character varying, numeric); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.alamat_kbbi(jalan character varying, rt numeric, rw numeric, kelurahan character varying, kecamatan character varying, kabkota character varying, provinsi character varying, kode_pos numeric) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return concat(
			jalan,
			', RT ',
			case
				when rt > 0 then rt
				else 0
			end,
			', RW ',
			case
				when rw > 0 then rw
				else 0
			end,
			', ',
			kelurahan,
			', ',
			kecamatan,
			', ',
			kabkota,
			', ',
			provinsi,
			', ',
			kode_pos
		);
	END;
$$;


ALTER FUNCTION public.alamat_kbbi(jalan character varying, rt numeric, rw numeric, kelurahan character varying, kecamatan character varying, kabkota character varying, provinsi character varying, kode_pos numeric) OWNER TO postgres;

--
-- Name: before_insert_acara_mendatang(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.before_insert_acara_mendatang() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		new.slug := concat(new.slug, '-', new.id);
		return new;
	END;
$$;


ALTER FUNCTION public.before_insert_acara_mendatang() OWNER TO postgres;

--
-- Name: before_insert_akm(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.before_insert_akm() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare smt_angka varchar := null;
	declare sks_total numeric := 0;

	begin
		select
			coalesce(max(cast(semester_angka as numeric)), 0) + 1,
			sum(sks_semester)
			into
			smt_angka,
			sks_total
		from tb_akm where id_riwayat_pendidikan = new.id_riwayat_pendidikan;
	
		new.semester_angka := smt_angka;
		new.total_sks := sks_total;
		new.ipk := hitung_ipk_akm(cast(new.id_riwayat_pendidikan as int));
	
		return new;
	END;
$$;


ALTER FUNCTION public.before_insert_akm() OWNER TO postgres;

--
-- Name: before_insert_berita(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.before_insert_berita() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	BEGIN
		new.slug := concat(new.slug, '-', new.id);
		return new;
	END;
$$;


ALTER FUNCTION public.before_insert_berita() OWNER TO postgres;

--
-- Name: before_insert_krs(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.before_insert_krs() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare ip_semester_lalu numeric := 0;

	begin
		select
			round(coalesce(tk.ip::numeric, 0), 2)
			into
			ip_semester_lalu
		from tb_krs tk
		where tk.id_riwayat_pendidikan = new.id_riwayat_pendidikan
		order by tk.id_semester, tk.tahun_ajaran desc
		limit 1;
	
		if new.semester_angka <> 1 then
			new.sks_diberikan := (select
					tmck.total_sks
				from tb_riwayat_pendidikan trp
				join tb_mst_prodi tmp on tmp.id = trp.id_prodi
				join tb_mst_config_krs tmck on tmck.id_jenjang_pend = tmp.id_jenjang_pendidikan
				where trp.id = new.id_riwayat_pendidikan
					and tmck.ip_mulai <= ip_semester_lalu
					and tmck.ip_sampai >= ip_semester_lalu
				order by tmck.ip_sampai desc);		
		end if;

		return new;
	END;
$$;


ALTER FUNCTION public.before_insert_krs() OWNER TO postgres;

--
-- Name: before_insert_krs_detail(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.before_insert_krs_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare
	nilai_tidak_lulus varchar := '';
	apakah_lulus bool := false;
	begin
		select
			array_agg(tmnh.nilai_huruf)
			into
			nilai_tidak_lulus
		from tb_mst_nilai_huruf tmnh
		where tmnh.nilai_sampai < (select tmnh2.nilai_sampai from tb_mst_nilai_huruf tmnh2 where tmnh2.is_minimal = true);
		
		if new.nilai_huruf is not null and new.nilai_huruf <> 'T' and new.nilai_huruf <> '' then
			if new.nilai_huruf = any(nilai_tidak_lulus::text[]) then
				apakah_lulus := false;
			else
				apakah_lulus := true;
			end if;
		else
			apakah_lulus := false;
		end if;
	
		new.is_lulus := apakah_lulus;

		return new;
	END;
$$;


ALTER FUNCTION public.before_insert_krs_detail() OWNER TO postgres;

--
-- Name: before_insert_nilai_transfer(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.before_insert_nilai_transfer() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare new_nilai_akhir numeric := 0;
	declare new_nilai_bobot numeric := 0;

	begin
		select
			nilai_mulai,
			bobot
			into
			new_nilai_akhir,
			new_nilai_bobot
		from tb_mst_nilai_huruf where nilai_huruf = new.nilai_huruf;
		
		new.nilai_akhir := new_nilai_akhir;
		new.nilai_bobot := new_nilai_bobot * new.sks_matkul;
		new.id_perguruan_tinggi := (select id_perguruan_tinggi from tb_profil_pt);
		return new;
	END;
$$;


ALTER FUNCTION public.before_insert_nilai_transfer() OWNER TO postgres;

--
-- Name: before_insert_penugasan_dosen(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.before_insert_penugasan_dosen() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		new.id_perguruan_tinggi := (select id_perguruan_tinggi from tb_profil_pt);
		return new;
	END;
$$;


ALTER FUNCTION public.before_insert_penugasan_dosen() OWNER TO postgres;

--
-- Name: before_update_krs_detail(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.before_update_krs_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare
	nilai_tidak_lulus varchar := '';
	apakah_lulus bool := false;
	BEGIN
		select
			array_agg(tmnh.nilai_huruf)
			into
			nilai_tidak_lulus
		from tb_mst_nilai_huruf tmnh
		where tmnh.nilai_sampai < (select nilai_sampai from tb_mst_nilai_huruf  where is_minimal = true);
		
		if new.nilai_huruf is not null and new.nilai_huruf <> 'T' and new.nilai_huruf <> '' then
			if new.nilai_huruf = any(nilai_tidak_lulus::text[]) then
				apakah_lulus := false;
			else
				apakah_lulus := true;
			end if;
		
			new.is_lulus := apakah_lulus;
		else
			new.is_lulus := false;
		end if;

		return new;
	END;
$$;


ALTER FUNCTION public.before_update_krs_detail() OWNER TO postgres;

--
-- Name: before_update_nilai_transfer(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.before_update_nilai_transfer() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare new_nilai_akhir numeric := 0;
	declare new_nilai_bobot numeric := 0;

	begin
		select
			nilai_mulai,
			bobot
			into
			new_nilai_akhir,
			new_nilai_bobot
		from tb_mst_nilai_huruf where nilai_huruf = new.nilai_huruf;
		
		new.nilai_akhir := new_nilai_akhir;
		new.nilai_bobot := new_nilai_bobot * new.sks_matkul;
		return new;
	END;
$$;


ALTER FUNCTION public.before_update_nilai_transfer() OWNER TO postgres;

--
-- Name: delete_krs(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_krs() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		delete from tb_krs_detail where id_krs = old.id;
		
		return old;
	END;
$$;


ALTER FUNCTION public.delete_krs() OWNER TO postgres;

--
-- Name: delete_kurikulum(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_kurikulum() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	BEGIN
		delete from tb_matkul_kurikulum where id_kurikulum = old.id; 
		return old;
	END;
$$;


ALTER FUNCTION public.delete_kurikulum() OWNER TO postgres;

--
-- Name: delete_mahasiswa(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_mahasiswa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		delete from tb_riwayat_pendidikan where id_mahasiswa = old.id;
		return old;
	END;
$$;


ALTER FUNCTION public.delete_mahasiswa() OWNER TO postgres;

--
-- Name: delete_matkul_kurikulum(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_matkul_kurikulum() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare jenis_matakulih varchar := '';
	declare jumlah_sks_matakuliah numeric := 0;

	begin
		select
			tm.sks_mata_kuliah
			into
			jumlah_sks_matakuliah
		from tb_matakuliah tm
		where tm.id = old.id_matkul;
	
		update tb_kurikulum
		set
			jumlah_sks_lulus = (jumlah_sks_pilihan - jumlah_sks_matakuliah),
			jumlah_sks_wajib = (jumlah_sks_wajib - case when old.apakah_wajib = '1' then jumlah_sks_matakuliah else 0 end),
			jumlah_sks_pilihan = (jumlah_sks_pilihan - case when old.apakah_wajib = '0' then jumlah_sks_matakuliah else 0 end)
		where id = old.id_kurikulum;
	
		delete from tb_matkul_bersyarat
		where id_kurikulum = old.id_kurikulum and id_matkul_kurikulum = old.id and id_matkul = old.id_matkul;
		return old;
	END;
$$;


ALTER FUNCTION public.delete_matkul_kurikulum() OWNER TO postgres;

--
-- Name: delete_riwayat_pendidikan(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_riwayat_pendidikan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		delete from tb_akm where id_riwayat_pendidikan = old.id;
		delete from tb_krs where id_riwayat_pendidikan = old.id;
		delete from tb_users where id_parent = old.id and role = '5';
		return old;
	END;
$$;


ALTER FUNCTION public.delete_riwayat_pendidikan() OWNER TO postgres;

--
-- Name: delete_roster(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_roster() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	BEGIN
		delete from tb_roster_asisten where id_roster = old.id;
		return old;
	END;
$$;


ALTER FUNCTION public.delete_roster() OWNER TO postgres;

--
-- Name: delete_unit_kelas(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_unit_kelas() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	begin
		delete from tb_unit_kelas_matakuliah where id_unit_kelas = old.id;
		delete from tb_roster where id_unit_kelas = old.id;
		return old;
	END;
$$;


ALTER FUNCTION public.delete_unit_kelas() OWNER TO postgres;

--
-- Name: hitung_ipk_akm(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.hitung_ipk_akm(irp integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return (select
			round(coalesce(sum(tkd.nilai_bobot) / sum(tm.sks_mata_kuliah), 0), 2)
		from tb_krs tk
		join tb_krs_detail tkd on tkd.id_krs = tk.id
		join tb_riwayat_pendidikan trp on trp.id = tk.id_riwayat_pendidikan
		join tb_matkul_kurikulum tmk on tmk.id_kurikulum = trp.id_kurikulum and tmk.id_matkul = tkd.id_matakuliah
		join tb_matakuliah tm on tm.id = tmk.id_matkul
		where tk.id_riwayat_pendidikan = irp
			and tkd.is_digunakan = true
			and tk.is_active = true
			and tkd.nilai_huruf != 'T');
	END;
$$;


ALTER FUNCTION public.hitung_ipk_akm(irp integer) OWNER TO postgres;

--
-- Name: hitung_ipk_krs(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.hitung_ipk_krs(irp integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return (select
			round(coalesce(sum(tkd.nilai_bobot) / sum(tm.sks_mata_kuliah), 0), 2)
		from tb_krs tk
		join tb_krs_detail tkd on tkd.id_krs = tk.id
		join tb_matakuliah tm on tm.id = tkd.id_matakuliah
		where tk.id_riwayat_pendidikan = irp
			and tk.is_active = true
			and tkd.nilai_huruf != 'T');
	END;
$$;


ALTER FUNCTION public.hitung_ipk_krs(irp integer) OWNER TO postgres;

--
-- Name: hitung_total_sks_mhs(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.hitung_total_sks_mhs(irp integer) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
	BEGIN
--		irp = id_riwayat_pendidikan
		
		return (select sum(tk.sks_diambil) from tb_krs tk where tk.id_riwayat_pendidikan = irp);
	END;
$$;


ALTER FUNCTION public.hitung_total_sks_mhs(irp integer) OWNER TO postgres;

--
-- Name: insert_matkul_kurikulum(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.insert_matkul_kurikulum() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare jenis_matakulih varchar := '';
	declare jumlah_sks_matakuliah numeric := 0;

	begin
		select
			tm.sks_mata_kuliah
			into
			jumlah_sks_matakuliah
		from tb_matakuliah tm
		where tm.id = new.id_matkul;
	
		if new.semester % 2 > 0 then
			new.is_ganjil := true;
		else
			new.is_genap := true;
		end if;
	
		if new.apakah_wajib = '1' then
			update tb_kurikulum set jumlah_sks_wajib = jumlah_sks_wajib + jumlah_sks_matakuliah, jumlah_sks_lulus = jumlah_sks_lulus + jumlah_sks_matakuliah
			where id = new.id_kurikulum;
		else
			update tb_kurikulum set jumlah_sks_pilihan = jumlah_sks_pilihan + jumlah_sks_matakuliah, jumlah_sks_lulus = jumlah_sks_lulus + jumlah_sks_matakuliah
			where id = new.id_kurikulum;
		end if;
		return new;
	END;
$$;


ALTER FUNCTION public.insert_matkul_kurikulum() OWNER TO postgres;

--
-- Name: jam_belajar(character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.jam_belajar(jam_masuk character varying, jam_keluar character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return concat(left(jam_masuk, 5), ' s/d ', right(jam_keluar, 5));
	END;
$$;


ALTER FUNCTION public.jam_belajar(jam_masuk character varying, jam_keluar character varying) OWNER TO postgres;

--
-- Name: nama_lengkap(character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.nama_lengkap(depan character varying, tengah character varying, belakang character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
	begin
		return concat(
			case
				when length(trim(depan)) > 0 then concat(depan, '. ')
			end, tengah,
			case
				when length(trim(belakang)) > 0 then concat(', ', belakang)
			end);
	END;
$$;


ALTER FUNCTION public.nama_lengkap(depan character varying, tengah character varying, belakang character varying) OWNER TO postgres;

--
-- Name: status_pengisian_nilai(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.status_pengisian_nilai(status character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
	declare render_status varchar := '';
	begin
		if status = '1' then
			render_status := 'Pengisian nilai belum selesai seutuhnya';
		elsif status = '2' then
			render_status := 'Pengisian nilai sudah selesai untuk semua peserta kelas';
		else
			render_status := 'Belum diproses';
		end if;
	
		return render_status;
	END;
$$;


ALTER FUNCTION public.status_pengisian_nilai(status character varying) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: tb_mst_wilayah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_wilayah (
    id character varying NOT NULL,
    id_negara character varying,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_wilayah OWNER TO postgres;

--
-- Name: list_wilayah; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_wilayah AS
 SELECT tmw.id AS id_wilayah,
    "left"((tmw.id)::text, 2) AS id_provinsi,
    tmw2.nama AS nama_provinsi,
    "left"("right"((tmw.id)::text, 4), 2) AS id_kabkota,
    tmw3.nama AS nama_kabkota,
    "right"((tmw.id)::text, 2) AS id_kecamatan,
    tmw4.nama AS nama_kecamatan
   FROM (((public.tb_mst_wilayah tmw
     JOIN public.tb_mst_wilayah tmw2 ON (((tmw2.id)::text = concat("left"((tmw.id)::text, 2), '0000'))))
     JOIN public.tb_mst_wilayah tmw3 ON (((tmw3.id)::text = concat("left"((tmw.id)::text, 2), "left"("right"((tmw.id)::text, 4), 2), '00'))))
     JOIN public.tb_mst_wilayah tmw4 ON (((tmw4.id)::text = concat("left"((tmw.id)::text, 2), "left"("right"((tmw.id)::text, 4), 2), "right"((tmw.id)::text, 2)))))
  WHERE (("left"("right"((tmw.id)::text, 4), 2) <> '00'::text) AND ("right"((tmw.id)::text, 2) <> '00'::text));


ALTER TABLE public.list_wilayah OWNER TO postgres;

--
-- Name: tb_mahasiswa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mahasiswa (
    id bigint NOT NULL,
    nama_mahasiswa character varying(100) NOT NULL,
    jenis_kelamin character varying(1) NOT NULL,
    tempat_lahir character varying(32),
    tanggal_lahir date,
    id_mahasiswa uuid,
    id_agama integer,
    nik character varying(16),
    nisn character varying(10),
    npwp character varying(15),
    id_negara character varying(2),
    jalan character varying(80),
    dusun character varying(60),
    rt numeric(2,0) DEFAULT 0,
    rw numeric(2,0) DEFAULT 0,
    kelurahan character varying(60),
    kode_pos numeric(5,0),
    id_wilayah character varying(8),
    id_jenis_tinggal integer,
    id_alat_transportasi integer,
    telepon character varying(20),
    handphone character varying(20),
    email character varying(80),
    penerima_kps boolean,
    nomor_kps character varying(80),
    nik_ayah character varying(20),
    nama_ayah character varying(100),
    tanggal_lahir_ayah date,
    id_pendidikan_ayah integer DEFAULT 0,
    id_pekerjaan_ayah integer,
    id_penghasilan_ayah integer DEFAULT 0,
    nik_ibu character varying(20),
    nama_ibu_kandung character varying(100),
    tanggal_lahir_ibu date,
    id_pendidikan_ibu integer DEFAULT 0,
    id_pekerjaan_ibu integer,
    id_penghasilan_ibu integer DEFAULT 0,
    nama_wali character varying(100),
    tanggal_lahir_wali date,
    id_pendidikan_wali integer DEFAULT 0,
    id_pekerjaan_wali integer,
    id_penghasilan_wali integer DEFAULT 0,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    syncron_feeder boolean DEFAULT false,
    error_feeder text,
    date_syncron_feeder timestamp without time zone
);


ALTER TABLE public.tb_mahasiswa OWNER TO postgres;

--
-- Name: COLUMN tb_mahasiswa.jenis_kelamin; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.jenis_kelamin IS 'L: Laki-laki,
P: Perempuan,
*: Belum ada informasi';


--
-- Name: COLUMN tb_mahasiswa.id_mahasiswa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_mahasiswa IS 'ID Feeder';


--
-- Name: COLUMN tb_mahasiswa.id_agama; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_agama IS 'Web Service: GetAgama';


--
-- Name: COLUMN tb_mahasiswa.nik; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.nik IS 'Nomor Induk Kependudukan, wajib di isi';


--
-- Name: COLUMN tb_mahasiswa.nisn; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.nisn IS 'Nomor Induk Siswa Nasional';


--
-- Name: COLUMN tb_mahasiswa.npwp; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.npwp IS 'Nomor Pokok Wajib Pajak';


--
-- Name: COLUMN tb_mahasiswa.id_negara; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_negara IS 'Web Service: GetNegara';


--
-- Name: COLUMN tb_mahasiswa.id_wilayah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_wilayah IS 'ID Wilayah. Web Service: GetWilayah';


--
-- Name: COLUMN tb_mahasiswa.id_jenis_tinggal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_jenis_tinggal IS 'Web Service: GetJenisTinggal';


--
-- Name: COLUMN tb_mahasiswa.id_alat_transportasi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_alat_transportasi IS 'Web Service: GetAlatTransportasi';


--
-- Name: COLUMN tb_mahasiswa.penerima_kps; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.penerima_kps IS '0: Bukan penerima KPS, 1: Penerima KPS';


--
-- Name: COLUMN tb_mahasiswa.nomor_kps; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.nomor_kps IS 'Nomor KPS (KARTU PERLINDUNGAN SOSIAL)';


--
-- Name: COLUMN tb_mahasiswa.nik_ayah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.nik_ayah IS 'Nomor Induk Kependudukan, wajib di isi';


--
-- Name: COLUMN tb_mahasiswa.id_pendidikan_ayah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_pendidikan_ayah IS 'Web Service: GetJenjangPendidikan';


--
-- Name: COLUMN tb_mahasiswa.id_pekerjaan_ayah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_pekerjaan_ayah IS 'Web Service: GetPekerjaan';


--
-- Name: COLUMN tb_mahasiswa.id_penghasilan_ayah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_penghasilan_ayah IS 'Web Service: GetPenghasilan';


--
-- Name: COLUMN tb_mahasiswa.id_pendidikan_ibu; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_pendidikan_ibu IS 'Web Service: GetJenjangPendidikan';


--
-- Name: COLUMN tb_mahasiswa.id_pekerjaan_ibu; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_pekerjaan_ibu IS 'Web Service: GetPekerjaan';


--
-- Name: COLUMN tb_mahasiswa.id_penghasilan_ibu; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_penghasilan_ibu IS 'Web Service: GetPenghasilan';


--
-- Name: COLUMN tb_mahasiswa.id_pendidikan_wali; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_pendidikan_wali IS 'Web Service: GetJenjangPendidikan';


--
-- Name: COLUMN tb_mahasiswa.id_pekerjaan_wali; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_pekerjaan_wali IS 'Web Service: GetPekerjaan';


--
-- Name: COLUMN tb_mahasiswa.id_penghasilan_wali; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mahasiswa.id_penghasilan_wali IS 'Web Service: GetPenghasilan';


--
-- Name: tb_mst_agama_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_agama_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_agama_id_seq OWNER TO postgres;

--
-- Name: tb_mst_agama; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_agama (
    id integer DEFAULT nextval('public.tb_mst_agama_id_seq'::regclass) NOT NULL,
    nama character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    sumber character varying
);


ALTER TABLE public.tb_mst_agama OWNER TO postgres;

--
-- Name: tb_mst_alat_transportasi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_alat_transportasi (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_alat_transportasi OWNER TO postgres;

--
-- Name: tb_mst_jenis_tinggal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jenis_tinggal (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_jenis_tinggal OWNER TO postgres;

--
-- Name: tb_mst_jenjang_pend; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jenjang_pend (
    id integer NOT NULL,
    nama character varying,
    lama_kuliah numeric,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_jenjang_pend OWNER TO postgres;

--
-- Name: COLUMN tb_mst_jenjang_pend.lama_kuliah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_jenjang_pend.lama_kuliah IS 'hitungan dalam semester';


--
-- Name: COLUMN tb_mst_jenjang_pend.sumber; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_jenjang_pend.sumber IS 'apakah data ini bersumber dari pddikti';


--
-- Name: tb_mst_negara; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_negara (
    id character varying(2) NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_negara OWNER TO postgres;

--
-- Name: tb_mst_pekerjaan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_pekerjaan (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_pekerjaan OWNER TO postgres;

--
-- Name: tb_mst_penghasilan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_penghasilan (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_penghasilan OWNER TO postgres;

--
-- Name: detail_mahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.detail_mahasiswa AS
 SELECT tm.id,
    tm.nama_mahasiswa,
    tm.tempat_lahir,
    tm.jenis_kelamin,
    tm.nama_ibu_kandung,
    tm.tanggal_lahir,
    tm.id_agama,
    tma.nama AS agama,
    tm.id_negara,
    tmn.nama AS kewarganegaraan,
    tm.nik,
    tm.nisn,
    tm.npwp,
    tm.jalan,
    tm.dusun,
    tm.rt,
    tm.rw,
    tm.kelurahan,
    tm.kode_pos,
    tm.id_wilayah,
    concat(lw.nama_kecamatan, ' - ', lw.nama_kabkota, ' - ', lw.nama_provinsi) AS kecamatan,
    tm.id_jenis_tinggal,
    tmjt.nama AS jenis_tinggal,
    tm.id_alat_transportasi,
    tmat.nama AS alat_transportasi,
    tm.telepon,
    tm.handphone,
    tm.email,
    tm.penerima_kps,
    tm.nomor_kps,
    tm.nik_ayah,
    tm.nama_ayah,
    tm.tanggal_lahir_ayah,
    tm.id_pendidikan_ayah,
    tmjp.nama AS pendidikan_ayah,
    tm.id_pekerjaan_ayah,
    tmp.nama AS pekerjaan_ayah,
    tm.id_penghasilan_ayah,
    tmp2.nama AS penghasilan_ayah,
    tm.nik_ibu,
    tm.tanggal_lahir_ibu,
    tm.id_pendidikan_ibu,
    tmjp2.nama AS pendidikan_ibu,
    tm.id_pekerjaan_ibu,
    tmp3.nama AS pekerjaan_ibu,
    tm.id_penghasilan_ibu,
    tmp4.nama AS penghasilan_ibu,
    tm.nama_wali,
    tm.tanggal_lahir_wali,
    tm.id_pendidikan_wali,
    tmjp3.nama AS pendidikan_wali,
    tm.id_pekerjaan_wali,
    tmp5.nama AS pekerjaan_wali,
    tm.id_penghasilan_wali,
    tmp6.nama AS penghasilan_wali
   FROM ((((((((((((((public.tb_mahasiswa tm
     LEFT JOIN public.tb_mst_agama tma ON ((tma.id = tm.id_agama)))
     LEFT JOIN public.tb_mst_negara tmn ON (((tmn.id)::text = (tm.id_negara)::text)))
     LEFT JOIN public.list_wilayah lw ON (((lw.id_wilayah)::text = (tm.id_wilayah)::text)))
     LEFT JOIN public.tb_mst_jenis_tinggal tmjt ON ((tmjt.id = tm.id_jenis_tinggal)))
     LEFT JOIN public.tb_mst_alat_transportasi tmat ON ((tmat.id = tm.id_alat_transportasi)))
     LEFT JOIN public.tb_mst_jenjang_pend tmjp ON ((tmjp.id = tm.id_pendidikan_ayah)))
     LEFT JOIN public.tb_mst_pekerjaan tmp ON ((tmp.id = tm.id_pekerjaan_ayah)))
     LEFT JOIN public.tb_mst_penghasilan tmp2 ON ((tmp2.id = tm.id_penghasilan_ayah)))
     LEFT JOIN public.tb_mst_jenjang_pend tmjp2 ON ((tmjp2.id = tm.id_pendidikan_ibu)))
     LEFT JOIN public.tb_mst_pekerjaan tmp3 ON ((tmp3.id = tm.id_pekerjaan_ibu)))
     LEFT JOIN public.tb_mst_penghasilan tmp4 ON ((tmp4.id = tm.id_penghasilan_ibu)))
     LEFT JOIN public.tb_mst_jenjang_pend tmjp3 ON ((tmjp3.id = tm.id_pendidikan_wali)))
     LEFT JOIN public.tb_mst_pekerjaan tmp5 ON ((tmp5.id = tm.id_pekerjaan_wali)))
     LEFT JOIN public.tb_mst_penghasilan tmp6 ON ((tmp6.id = tm.id_penghasilan_wali)));


ALTER TABLE public.detail_mahasiswa OWNER TO postgres;

--
-- Name: tb_dosen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_dosen (
    id integer NOT NULL,
    id_dosen uuid,
    gelar_depan character varying,
    gelar_belakang character varying,
    nama character varying,
    tempat_lahir character varying,
    tanggal_lahir date,
    id_agama integer,
    nama_ibu_kandung character varying,
    id_status_aktif integer,
    nidn character varying,
    nik character varying,
    nip character varying,
    npwp character varying,
    id_ikatan_kerja character varying,
    id_status_pegawai integer,
    id_golpang integer,
    jalan character varying,
    dusun character varying,
    rt numeric(2,0),
    rw numeric(2,0),
    kelurahan character varying,
    kodepos numeric(5,0),
    id_wilayah character varying,
    telepon character varying,
    handphone character varying,
    email character varying,
    id_prodi integer,
    jenis_kelamin character varying(1),
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    no_sk_cpns character varying,
    tanggal_sk_cpns date,
    no_sk_pengangkatan character varying,
    mulai_sk_pengangkatan date,
    id_lembaga_pengangkatan integer,
    status_pernikahan character varying(1),
    nama_suami_istri character varying,
    nip_suami_istri character varying,
    tanggal_mulai_pns date,
    id_pekerjaan_suami_istri integer,
    sumber_gaji character varying,
    id_jenis_pegawai integer
);


ALTER TABLE public.tb_dosen OWNER TO postgres;

--
-- Name: COLUMN tb_dosen.id_dosen; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.id_dosen IS 'ID feeder';


--
-- Name: COLUMN tb_dosen.id_agama; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.id_agama IS 'tb_mst_agama';


--
-- Name: COLUMN tb_dosen.id_status_aktif; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.id_status_aktif IS 'tb_mst_status_keaktifan_pegawai';


--
-- Name: COLUMN tb_dosen.nidn; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.nidn IS 'NIDN / NUP / NIDK';


--
-- Name: COLUMN tb_dosen.id_ikatan_kerja; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.id_ikatan_kerja IS 'tb_mst_ikatan_kerja';


--
-- Name: COLUMN tb_dosen.id_status_pegawai; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.id_status_pegawai IS 'tb_mst_status_kepegawaian';


--
-- Name: COLUMN tb_dosen.id_golpang; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.id_golpang IS 'tb_mst_golpang';


--
-- Name: COLUMN tb_dosen.id_wilayah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.id_wilayah IS 'tb_mst_wilayah';


--
-- Name: COLUMN tb_dosen.id_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.id_prodi IS 'tb_mst_prodi';


--
-- Name: COLUMN tb_dosen.id_lembaga_pengangkatan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.id_lembaga_pengangkatan IS 'tb_mst_lembaga_pengangkatan';


--
-- Name: COLUMN tb_dosen.status_pernikahan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.status_pernikahan IS '0 => belum menikah;

1 => sudah menikah;';


--
-- Name: COLUMN tb_dosen.id_pekerjaan_suami_istri; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.id_pekerjaan_suami_istri IS 'tb_mst_pekerjaan';


--
-- Name: COLUMN tb_dosen.id_jenis_pegawai; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_dosen.id_jenis_pegawai IS 'tb_mst_jenis_pegawai';


--
-- Name: tb_mst_status_keaktifan_pegawai; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_status_keaktifan_pegawai (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_status_keaktifan_pegawai OWNER TO postgres;

--
-- Name: tb_roster_asisten; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_roster_asisten (
    id bigint NOT NULL,
    id_roster integer,
    id_dosen integer,
    id_unit_kelas integer
);


ALTER TABLE public.tb_roster_asisten OWNER TO postgres;

--
-- Name: TABLE tb_roster_asisten; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.tb_roster_asisten IS 'mencatat tim/asisten dosen yang ada di roster ini';


--
-- Name: COLUMN tb_roster_asisten.id_roster; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster_asisten.id_roster IS 'tb_roster';


--
-- Name: COLUMN tb_roster_asisten.id_dosen; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster_asisten.id_dosen IS 'tb_dosen;
ini merupakan id asisten';


--
-- Name: COLUMN tb_roster_asisten.id_unit_kelas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster_asisten.id_unit_kelas IS 'tb_unit_kelas';


--
-- Name: detail_roster_asisten; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.detail_roster_asisten AS
 SELECT tra.id,
    tra.id_roster,
    tra.id_dosen,
    public.nama_lengkap(td.gelar_depan, td.nama, td.gelar_belakang) AS nama_dosen,
    td.nidn,
    td.id_status_aktif,
    tmskp.nama AS nama_status_aktif,
    td.nip,
    tra.id_unit_kelas
   FROM ((public.tb_roster_asisten tra
     JOIN public.tb_dosen td ON ((td.id = tra.id_dosen)))
     LEFT JOIN public.tb_mst_status_keaktifan_pegawai tmskp ON ((tmskp.id = td.id_status_aktif)));


ALTER TABLE public.detail_roster_asisten OWNER TO postgres;

--
-- Name: tb_aktivitas_mahasiswa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_aktivitas_mahasiswa (
    id integer NOT NULL,
    id_jenis_aktivitas integer,
    judul character varying,
    lokasi character varying,
    nomor_sk_tugas character varying,
    tanggal_sk_tugas date,
    id_jenis_anggota character varying,
    keterangan character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    id_aktivitas uuid,
    id_prodi integer,
    tahun_ajaran numeric(4,0),
    id_semester numeric(1,0),
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_aktivitas_mahasiswa OWNER TO postgres;

--
-- Name: COLUMN tb_aktivitas_mahasiswa.id_jenis_aktivitas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_aktivitas_mahasiswa.id_jenis_aktivitas IS 'tb_mst_jenis_aktivitas_mahasiswa';


--
-- Name: COLUMN tb_aktivitas_mahasiswa.id_jenis_anggota; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_aktivitas_mahasiswa.id_jenis_anggota IS '0 => personal;

1 => kelompok;';


--
-- Name: COLUMN tb_aktivitas_mahasiswa.syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_aktivitas_mahasiswa.syncron_feeder IS 'apakah sudah melakukan syncron feeder';


--
-- Name: COLUMN tb_aktivitas_mahasiswa.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_aktivitas_mahasiswa.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: COLUMN tb_aktivitas_mahasiswa.id_aktivitas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_aktivitas_mahasiswa.id_aktivitas IS 'id feeder';


--
-- Name: COLUMN tb_aktivitas_mahasiswa.id_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_aktivitas_mahasiswa.id_prodi IS 'tb_mst_prodi';


--
-- Name: tb_mst_prodi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_prodi (
    id integer NOT NULL,
    id_prodi uuid,
    kode character varying,
    nama character varying,
    status character varying(1),
    id_jenjang_pendidikan integer,
    id_fakultas integer,
    user_modified character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    singkatan character varying,
    no_urut character varying(2),
    kelompok_ujian character varying,
    id_kepala_prodi integer
);


ALTER TABLE public.tb_mst_prodi OWNER TO postgres;

--
-- Name: COLUMN tb_mst_prodi.id_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_prodi.id_prodi IS 'ID feeder';


--
-- Name: COLUMN tb_mst_prodi.kelompok_ujian; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_prodi.kelompok_ujian IS 'IPA; IPS; IPC;';


--
-- Name: COLUMN tb_mst_prodi.id_kepala_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_prodi.id_kepala_prodi IS 'tb_dosen';


--
-- Name: insertaktivitasmahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertaktivitasmahasiswa AS
 SELECT tam.id AS id_aktivitas_mhs,
    tam.id_prodi AS id_prodi_master,
    tam.id_jenis_anggota AS jenis_anggota,
    tam.id_jenis_aktivitas,
    tmp.id_prodi,
    concat(tam.tahun_ajaran, tam.id_semester) AS id_semester,
    tam.judul,
    tam.keterangan,
    tam.lokasi,
    tam.nomor_sk_tugas AS sk_tugas,
    tam.tanggal_sk_tugas
   FROM (public.tb_aktivitas_mahasiswa tam
     JOIN public.tb_mst_prodi tmp ON ((tmp.id = tam.id_prodi)))
  WHERE (tam.syncron_feeder = false);


ALTER TABLE public.insertaktivitasmahasiswa OWNER TO postgres;

--
-- Name: tb_anggota_aktivitas_mahasiswa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_anggota_aktivitas_mahasiswa (
    id integer NOT NULL,
    id_aktivitas_mahasiswa integer,
    id_riwayat_pendidikan integer,
    jenis_peran character varying(1),
    id_anggota uuid,
    syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    uploaded timestamp without time zone,
    user_modified character varying,
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_anggota_aktivitas_mahasiswa OWNER TO postgres;

--
-- Name: COLUMN tb_anggota_aktivitas_mahasiswa.id_aktivitas_mahasiswa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_anggota_aktivitas_mahasiswa.id_aktivitas_mahasiswa IS 'tb_aktivitas_mahasiswa';


--
-- Name: COLUMN tb_anggota_aktivitas_mahasiswa.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_anggota_aktivitas_mahasiswa.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_anggota_aktivitas_mahasiswa.jenis_peran; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_anggota_aktivitas_mahasiswa.jenis_peran IS '1: Ketua, 2: Anggota, 3: Personal';


--
-- Name: COLUMN tb_anggota_aktivitas_mahasiswa.id_anggota; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_anggota_aktivitas_mahasiswa.id_anggota IS 'id feeder';


--
-- Name: COLUMN tb_anggota_aktivitas_mahasiswa.syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_anggota_aktivitas_mahasiswa.syncron_feeder IS 'apakah sudah melakuan syncron feeder';


--
-- Name: COLUMN tb_anggota_aktivitas_mahasiswa.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_anggota_aktivitas_mahasiswa.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: tb_riwayat_pendidikan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_riwayat_pendidikan (
    id bigint NOT NULL,
    id_mahasiswa integer,
    id_registrasi_mahasiswa uuid,
    nim character varying(24),
    id_jenis_daftar integer,
    id_jalur_daftar integer,
    id_periode_masuk character varying(5),
    tanggal_daftar date,
    id_perguruan_tinggi uuid,
    id_prodi integer,
    id_bidang_minat integer,
    sks_diakui numeric(3,0),
    id_perguruan_tinggi_asal uuid,
    id_prodi_asal uuid,
    id_pembiayaan integer,
    biaya_masuk numeric(16,0),
    id_status character varying(1),
    ta_masuk numeric(4,0),
    id_kurikulum integer,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    syncron_feeder boolean DEFAULT false,
    error_feeder text,
    id_spp integer,
    id_ukt numeric,
    id_jenis_keluar integer,
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_riwayat_pendidikan OWNER TO postgres;

--
-- Name: COLUMN tb_riwayat_pendidikan.id_mahasiswa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_mahasiswa IS 'tb_mahasiswa';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_registrasi_mahasiswa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_registrasi_mahasiswa IS 'ID feeder';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_jenis_daftar; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_jenis_daftar IS 'Web Service: GetJenisPendaftaran; tb_mst_jenis_pendaftaran';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_jalur_daftar; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_jalur_daftar IS 'Web Service: GetJalurMasuk; tb_mst_jalurmasuk';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_periode_masuk; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_periode_masuk IS 'ID Mulai Semester. Web Service: GetSemester';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_perguruan_tinggi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_perguruan_tinggi IS 'ID Perguruan Tinggi. Web Service: GetProfilPT';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_prodi IS 'ID Prodi. Web Service: GetProdi';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_bidang_minat; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_bidang_minat IS 'ID Bidang Minat. Web Service: GetListBidangMinat';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_perguruan_tinggi_asal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_perguruan_tinggi_asal IS 'ID Perguruan Tinggi. Web Service: GetAllPT';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_prodi_asal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_prodi_asal IS 'ID Prodi. Web Service: GetAllProdi';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_pembiayaan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_pembiayaan IS 'ID Pembiayaan Awal. Web Service: GetPembiayaan';


--
-- Name: COLUMN tb_riwayat_pendidikan.biaya_masuk; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.biaya_masuk IS 'Total biaya masuk kuliah mahasiswa';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_spp; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_spp IS 'tb_mst_spp_prodi';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_ukt; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_ukt IS '1 sampai 10';


--
-- Name: COLUMN tb_riwayat_pendidikan.id_jenis_keluar; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pendidikan.id_jenis_keluar IS 'tb_mst_jenis_keluar';


--
-- Name: insertanggotaaktivitasmahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertanggotaaktivitasmahasiswa AS
 SELECT taam.id,
    trp.id_prodi,
    tam.id_aktivitas,
    trp.id_registrasi_mahasiswa,
    taam.jenis_peran
   FROM ((public.tb_anggota_aktivitas_mahasiswa taam
     JOIN public.tb_aktivitas_mahasiswa tam ON ((tam.id = taam.id_aktivitas_mahasiswa)))
     JOIN public.tb_riwayat_pendidikan trp ON ((trp.id = taam.id_riwayat_pendidikan)))
  WHERE (taam.syncron_feeder = false);


ALTER TABLE public.insertanggotaaktivitasmahasiswa OWNER TO postgres;

--
-- Name: tb_pembimbing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_pembimbing (
    id integer NOT NULL,
    id_aktivitas_mahasiswa integer,
    id_dosen integer,
    id_kategori_kegiatan character varying,
    pembimbing_ke numeric,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    id_bimbing_mahasiswa uuid,
    syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_pembimbing OWNER TO postgres;

--
-- Name: COLUMN tb_pembimbing.id_aktivitas_mahasiswa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pembimbing.id_aktivitas_mahasiswa IS 'tb_aktivitas_mahasiswa';


--
-- Name: COLUMN tb_pembimbing.id_dosen; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pembimbing.id_dosen IS 'tb_dosen';


--
-- Name: COLUMN tb_pembimbing.id_kategori_kegiatan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pembimbing.id_kategori_kegiatan IS 'tb_mst_kategori_kegiatan';


--
-- Name: COLUMN tb_pembimbing.id_bimbing_mahasiswa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pembimbing.id_bimbing_mahasiswa IS 'id feeder';


--
-- Name: COLUMN tb_pembimbing.syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pembimbing.syncron_feeder IS 'apakah sudah melakukan syncron feeder';


--
-- Name: COLUMN tb_pembimbing.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pembimbing.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: insertbimbingmahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertbimbingmahasiswa AS
 SELECT tp.id,
    tam.id_prodi,
    tam.id_aktivitas,
    tp.id_kategori_kegiatan,
    td.id_dosen,
    tp.pembimbing_ke
   FROM ((public.tb_pembimbing tp
     JOIN public.tb_aktivitas_mahasiswa tam ON ((tam.id = tp.id_aktivitas_mahasiswa)))
     JOIN public.tb_dosen td ON ((td.id = tp.id_dosen)))
  WHERE (tp.syncron_feeder = false);


ALTER TABLE public.insertbimbingmahasiswa OWNER TO postgres;

--
-- Name: insertbiodatamahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertbiodatamahasiswa AS
 SELECT tm.id AS id_mahasiswa,
    tm.nama_mahasiswa,
    tm.jenis_kelamin,
    tm.tempat_lahir,
    tm.tanggal_lahir,
    tm.id_agama,
    tm.nik,
    tm.nisn,
    tm.npwp,
    tm.id_negara AS kewarganegaraan,
    tm.jalan,
    tm.dusun,
    tm.rt,
    tm.rw,
    tm.kelurahan,
    tm.kode_pos,
    tm.id_wilayah,
    tm.id_jenis_tinggal,
    tm.id_alat_transportasi,
    tm.telepon,
    tm.handphone,
    tm.email,
        CASE
            WHEN (tm.penerima_kps = true) THEN 1
            ELSE 0
        END AS penerima_kps,
    tm.nomor_kps,
    tm.nik_ayah,
    tm.nama_ayah,
    tm.tanggal_lahir_ayah,
    tm.id_pendidikan_ayah,
    tm.id_pekerjaan_ayah,
    tm.id_penghasilan_ayah,
    tm.nik_ibu,
    tm.nama_ibu_kandung,
    tm.tanggal_lahir_ibu,
    tm.id_pendidikan_ibu,
    tm.id_pekerjaan_ibu,
    tm.id_penghasilan_ibu,
    tm.nama_wali,
    tm.tanggal_lahir_wali,
    tm.id_pendidikan_wali,
    tm.id_pekerjaan_wali,
    tm.id_penghasilan_wali,
    0 AS id_kebutuhan_khusus_mahasiswa,
    0 AS id_kebutuhan_khusus_ayah,
    0 AS id_kebutuhan_khusus_ibu,
    trp.id_prodi
   FROM (public.tb_riwayat_pendidikan trp
     JOIN public.tb_mahasiswa tm ON ((tm.id = trp.id_mahasiswa)))
  WHERE (tm.syncron_feeder = false);


ALTER TABLE public.insertbiodatamahasiswa OWNER TO postgres;

--
-- Name: tb_matakuliah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_matakuliah (
    id bigint NOT NULL,
    kode character varying,
    nama character varying,
    id_jenis_mata_kuliah character varying,
    id_kelompok_mata_kuliah character varying,
    sks_mata_kuliah numeric(5,0) DEFAULT 0,
    sks_tatap_muka numeric(5,0) DEFAULT 0,
    sks_praktek numeric(5,0) DEFAULT 0,
    sks_praktek_lapangan numeric(5,0) DEFAULT 0,
    sks_simulasi numeric(5,0) DEFAULT 0,
    metode_kuliah character varying(50),
    ada_sap boolean DEFAULT false,
    ada_silabus boolean DEFAULT false,
    ada_bahan_ajar boolean DEFAULT false,
    ada_acara_praktek boolean DEFAULT false,
    ada_diktat boolean DEFAULT false,
    tanggal_mulai_efektif date,
    tanggal_akhir_efektif date,
    id_matkul uuid,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    syncron_feeder boolean DEFAULT false,
    error_feeder text,
    id_model_nilai integer,
    apakah_unit boolean DEFAULT true,
    id_prodi integer,
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_matakuliah OWNER TO postgres;

--
-- Name: COLUMN tb_matakuliah.id_jenis_mata_kuliah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matakuliah.id_jenis_mata_kuliah IS 'tb_mst_jenis_matkul';


--
-- Name: COLUMN tb_matakuliah.id_kelompok_mata_kuliah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matakuliah.id_kelompok_mata_kuliah IS 'tb_mst_kelompok_matkul';


--
-- Name: COLUMN tb_matakuliah.id_matkul; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matakuliah.id_matkul IS 'ID feeder';


--
-- Name: COLUMN tb_matakuliah.id_model_nilai; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matakuliah.id_model_nilai IS 'tb_mst_model_penilaian';


--
-- Name: COLUMN tb_matakuliah.apakah_unit; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matakuliah.apakah_unit IS 'apakah matakuliah ini memiliki unit kelas dan roster';


--
-- Name: COLUMN tb_matakuliah.id_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matakuliah.id_prodi IS 'tb_mst_prodi';


--
-- Name: tb_penugasan_dosen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_penugasan_dosen (
    id integer NOT NULL,
    id_registrasi_dosen uuid,
    id_dosen_feeder uuid,
    id_tahun_ajaran numeric(4,0),
    id_perguruan_tinggi uuid,
    id_prodi uuid,
    nomor_surat_tugas character varying,
    tanggal_surat_tugas date,
    mulai_surat_tugas date,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    id_mst_prodi integer,
    id_dosen integer
);


ALTER TABLE public.tb_penugasan_dosen OWNER TO postgres;

--
-- Name: COLUMN tb_penugasan_dosen.id_mst_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_penugasan_dosen.id_mst_prodi IS 'tb_mst_prodi';


--
-- Name: COLUMN tb_penugasan_dosen.id_dosen; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_penugasan_dosen.id_dosen IS 'tb_dosen';


--
-- Name: tb_roster; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_roster (
    id bigint NOT NULL,
    id_unit_kelas integer,
    apa_untuk_pditt boolean DEFAULT false,
    lingkup numeric,
    mode character varying(1),
    tanggal_tutup_daftar date,
    id_ruangan integer,
    tanggal_mulai_masuk date,
    id_jam_masuk integer,
    id_jam_keluar integer,
    id_dosen smallint,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    id_hari character(1),
    tanggal_mulai_efektif date,
    tanggal_akhir_efektif date,
    kapasitas numeric,
    syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    id_aktivitas_mengajar uuid,
    jumlah_minggu_pertemuan numeric,
    is_approve_jurnal boolean DEFAULT false,
    date_syncron timestamp without time zone,
    id_jenis_evaluasi integer,
    id_substansi integer
);


ALTER TABLE public.tb_roster OWNER TO postgres;

--
-- Name: COLUMN tb_roster.id_unit_kelas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.id_unit_kelas IS 'tb_unit_kelas';


--
-- Name: COLUMN tb_roster.apa_untuk_pditt; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.apa_untuk_pditt IS 'ditujukan bagi Perguruan Tinggi yang menyelenggarakan kelas pada Kampus Merdeka';


--
-- Name: COLUMN tb_roster.lingkup; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.lingkup IS '1: Internal, 2: External, 3: Campuran';


--
-- Name: COLUMN tb_roster.mode; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.mode IS 'O: Online, F: Offline, M: Campuran';


--
-- Name: COLUMN tb_roster.tanggal_tutup_daftar; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.tanggal_tutup_daftar IS 'Wajib diisi jika kelas diperuntukkan Kampus Merdeka';


--
-- Name: COLUMN tb_roster.id_ruangan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.id_ruangan IS 'tb_mst_ruangan';


--
-- Name: COLUMN tb_roster.id_dosen; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.id_dosen IS 'tb_dosen';


--
-- Name: COLUMN tb_roster.id_hari; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.id_hari IS 'tb_mst_hari';


--
-- Name: COLUMN tb_roster.tanggal_mulai_efektif; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.tanggal_mulai_efektif IS 'wajib diisi jika untuk kampus merdeka';


--
-- Name: COLUMN tb_roster.tanggal_akhir_efektif; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.tanggal_akhir_efektif IS 'wajib diisi jika untuk kampus merdeka';


--
-- Name: COLUMN tb_roster.kapasitas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.kapasitas IS 'wajib diisi jika untuk kampus merdeka';


--
-- Name: COLUMN tb_roster.syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.syncron_feeder IS 'apakah sudah melakukan syncron feeder';


--
-- Name: COLUMN tb_roster.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: COLUMN tb_roster.id_aktivitas_mengajar; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.id_aktivitas_mengajar IS 'id feeder';


--
-- Name: COLUMN tb_roster.jumlah_minggu_pertemuan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.jumlah_minggu_pertemuan IS 'jumlah masuk kelas/pertemuan';


--
-- Name: COLUMN tb_roster.is_approve_jurnal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.is_approve_jurnal IS 'apakah jurnal ajar dosen telah disetujui oleh pihak akademik fakultas';


--
-- Name: COLUMN tb_roster.id_jenis_evaluasi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.id_jenis_evaluasi IS 'tb_mst_jenis_evaluasi';


--
-- Name: COLUMN tb_roster.id_substansi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_roster.id_substansi IS 'tb_substansi';


--
-- Name: tb_unit_kelas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_unit_kelas (
    id bigint NOT NULL,
    tahun_ajaran numeric,
    id_semester numeric(1,0),
    id_prodi integer,
    kode_unit character varying,
    kapasitas numeric,
    terisi numeric,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    id_kelas_kuliah uuid,
    syncron_feeder boolean DEFAULT false,
    error_feeder text,
    status_pengisian character varying,
    is_boleh_isi_nilai boolean DEFAULT false,
    id_kurikulum integer,
    nama_kelas character varying(5),
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_unit_kelas OWNER TO postgres;

--
-- Name: COLUMN tb_unit_kelas.id_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas.id_prodi IS 'tb_mst_prodi';


--
-- Name: COLUMN tb_unit_kelas.kapasitas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas.kapasitas IS 'kapasitas mahasiswa di unit ini';


--
-- Name: COLUMN tb_unit_kelas.terisi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas.terisi IS 'jumlah mahasiswa terisi di unit ini';


--
-- Name: COLUMN tb_unit_kelas.id_kelas_kuliah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas.id_kelas_kuliah IS 'ID feeder';


--
-- Name: COLUMN tb_unit_kelas.status_pengisian; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas.status_pengisian IS 'status pengisian nilai

1 => Pengisian nilai belum selesai seutuhnya;
2 => Pengisian nilai sudah selesai untuk semua peserta kelas;';


--
-- Name: COLUMN tb_unit_kelas.id_kurikulum; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas.id_kurikulum IS 'tb_kurikulum';


--
-- Name: tb_unit_kelas_matakuliah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_unit_kelas_matakuliah (
    id bigint NOT NULL,
    id_unit_kelas integer,
    id_matakuliah integer,
    is_join boolean DEFAULT false,
    uploaded timestamp without time zone,
    user_modified character varying,
    id_kurikulum integer
);


ALTER TABLE public.tb_unit_kelas_matakuliah OWNER TO postgres;

--
-- Name: COLUMN tb_unit_kelas_matakuliah.id_unit_kelas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas_matakuliah.id_unit_kelas IS 'tb_unit_kelas';


--
-- Name: COLUMN tb_unit_kelas_matakuliah.id_matakuliah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas_matakuliah.id_matakuliah IS 'tb_matakuliah, tb_matkul_kurikulum';


--
-- Name: COLUMN tb_unit_kelas_matakuliah.is_join; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas_matakuliah.is_join IS 'apakah matakuliah hasil join dengan kurikulum lain atau tidak';


--
-- Name: COLUMN tb_unit_kelas_matakuliah.id_kurikulum; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas_matakuliah.id_kurikulum IS 'tb_kurikulum;



jika is join maka nilai id_matakuliah bisa jadi dari sumber kurikulum yang lain';


--
-- Name: insertdosenpengajarkelaskuliah; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertdosenpengajarkelaskuliah AS
 SELECT tr.id AS id_roster,
    tuk.id_prodi,
    tpd.id_registrasi_dosen,
    tuk.id_kelas_kuliah,
    NULL::text AS id_substansi,
    tm.sks_mata_kuliah AS sks_substansi_total,
    tr.jumlah_minggu_pertemuan AS rencana_minggu_pertemuan,
    tr.jumlah_minggu_pertemuan AS realisasi_minggu_pertemuan,
    1 AS id_jenis_evaluasi
   FROM (((((public.tb_roster tr
     JOIN public.tb_unit_kelas tuk ON ((tuk.id = tr.id_unit_kelas)))
     JOIN public.tb_dosen td ON ((td.id = tr.id_dosen)))
     JOIN public.tb_unit_kelas_matakuliah tukm ON ((tukm.id_unit_kelas = tuk.id)))
     JOIN public.tb_matakuliah tm ON ((tm.id = tukm.id_matakuliah)))
     LEFT JOIN ( SELECT max(tb_penugasan_dosen.id_tahun_ajaran) AS max,
            tb_penugasan_dosen.id_registrasi_dosen,
            tb_penugasan_dosen.id_dosen_feeder AS id_dosen
           FROM public.tb_penugasan_dosen
          GROUP BY tb_penugasan_dosen.id_registrasi_dosen, tb_penugasan_dosen.id_dosen_feeder) tpd ON ((tpd.id_dosen = td.id_dosen)))
  WHERE (tr.syncron_feeder = false);


ALTER TABLE public.insertdosenpengajarkelaskuliah OWNER TO postgres;

--
-- Name: insertkelaskuliah; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertkelaskuliah AS
 SELECT tuk.id AS id_unit_kelas,
    tuk.id_prodi AS id_prodi_master,
    tmp.id_prodi,
    concat(tuk.tahun_ajaran, tuk.id_semester) AS id_semester,
    tm.id_matkul,
    split_part((tuk.kode_unit)::text, '/'::text, 4) AS nama_kelas_kuliah,
    NULL::text AS bahasan,
    tr.tanggal_mulai_efektif,
    tr.tanggal_akhir_efektif,
    tr.tanggal_tutup_daftar,
        CASE
            WHEN (tr.apa_untuk_pditt = true) THEN 1
            ELSE 0
        END AS apa_untuk_pditt,
    COALESCE(tr.kapasitas, (0)::numeric) AS kapasitas,
    tr.lingkup,
    tr.mode
   FROM ((((public.tb_unit_kelas tuk
     JOIN public.tb_mst_prodi tmp ON ((tmp.id = tuk.id_prodi)))
     JOIN public.tb_unit_kelas_matakuliah tukm ON ((tukm.id_unit_kelas = tuk.id)))
     JOIN public.tb_matakuliah tm ON ((tm.id = tukm.id_matakuliah)))
     LEFT JOIN public.tb_roster tr ON ((tr.id_unit_kelas = tuk.id)))
  WHERE (tuk.syncron_feeder = false);


ALTER TABLE public.insertkelaskuliah OWNER TO postgres;

--
-- Name: tb_kurikulum; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_kurikulum (
    id integer NOT NULL,
    id_kurikulum uuid,
    nama character varying,
    id_prodi integer,
    tahun_ajaran numeric(4,0),
    id_semester numeric(1,0),
    jumlah_sks_lulus numeric(3,0) DEFAULT 0,
    jumlah_sks_wajib numeric(3,0) DEFAULT 0,
    jumlah_sks_pilihan numeric(3,0) DEFAULT 0,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    syncron_feeder boolean DEFAULT false,
    error_feeder text,
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_kurikulum OWNER TO postgres;

--
-- Name: COLUMN tb_kurikulum.id_kurikulum; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kurikulum.id_kurikulum IS 'ID feeder';


--
-- Name: insertkurikulum; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertkurikulum AS
 SELECT tk.id AS id_kurikulum,
    tk.id_prodi AS id_prodi_master,
    tk.nama AS nama_kurikulum,
    tmp.id_prodi,
    concat(tk.tahun_ajaran, tk.id_semester) AS id_semester,
    tk.jumlah_sks_lulus,
    tk.jumlah_sks_wajib,
    tk.jumlah_sks_pilihan
   FROM (public.tb_kurikulum tk
     JOIN public.tb_mst_prodi tmp ON ((tmp.id = tk.id_prodi)))
  WHERE (tk.syncron_feeder = false);


ALTER TABLE public.insertkurikulum OWNER TO postgres;

--
-- Name: tb_lulus_dropout; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_lulus_dropout (
    id integer NOT NULL,
    id_riwayat_pendidikan integer,
    id_jenis_keluar integer,
    tanggal_keluar date,
    tahun_ajaran numeric(4,0),
    id_semester numeric(1,0),
    keterangan character varying,
    nomor_sk character varying,
    tanggal_sk date,
    ipk numeric(4,2),
    no_ijazah character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    is_syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    bulan_awal_bimbingan date,
    bulan_akhir_bimbingan date,
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_lulus_dropout OWNER TO postgres;

--
-- Name: COLUMN tb_lulus_dropout.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_lulus_dropout.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_lulus_dropout.id_jenis_keluar; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_lulus_dropout.id_jenis_keluar IS 'tb_mst_jenis_keluar';


--
-- Name: COLUMN tb_lulus_dropout.nomor_sk; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_lulus_dropout.nomor_sk IS 'nomor sk yudisium';


--
-- Name: COLUMN tb_lulus_dropout.tanggal_sk; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_lulus_dropout.tanggal_sk IS 'tanggal sk yudisium';


--
-- Name: COLUMN tb_lulus_dropout.is_syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_lulus_dropout.is_syncron_feeder IS 'apakah sudah melakukan syncron ke feeder';


--
-- Name: COLUMN tb_lulus_dropout.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_lulus_dropout.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: insertmahasiswalulusdo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertmahasiswalulusdo AS
 SELECT tld.id,
    trp.id_prodi,
    trp.id_registrasi_mahasiswa,
    tld.id_jenis_keluar,
    tld.tanggal_keluar,
    concat(tld.tahun_ajaran, tld.id_semester) AS id_periode_keluar,
    tld.keterangan,
    tld.nomor_sk AS nomor_sk_yudisium,
    tld.tanggal_sk AS tanggal_sk_yudisium,
    tld.ipk,
    tld.no_ijazah AS nomor_ijazah,
    tld.bulan_awal_bimbingan,
    tld.bulan_akhir_bimbingan
   FROM (public.tb_lulus_dropout tld
     JOIN public.tb_riwayat_pendidikan trp ON ((trp.id = tld.id_riwayat_pendidikan)))
  WHERE (tld.is_syncron_feeder = false);


ALTER TABLE public.insertmahasiswalulusdo OWNER TO postgres;

--
-- Name: tb_mst_jenis_matkul; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jenis_matkul (
    id character varying NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_jenis_matkul OWNER TO postgres;

--
-- Name: insertmatakuliah; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertmatakuliah AS
 SELECT tm.id AS id_matkul,
    tm.id_prodi AS id_master_prodi,
    tm.kode AS kode_mata_kuliah,
    tm.nama AS nama_mata_kuliah,
    tmp.id_prodi,
    tm.id_jenis_mata_kuliah,
    tm.id_kelompok_mata_kuliah,
    tm.sks_mata_kuliah,
    tm.sks_tatap_muka,
    tm.sks_praktek,
    tm.sks_praktek_lapangan,
    tm.sks_simulasi,
    tm.metode_kuliah,
        CASE
            WHEN (tm.ada_sap = true) THEN 1
            ELSE 0
        END AS ada_sap,
        CASE
            WHEN (tm.ada_silabus = true) THEN 1
            ELSE 0
        END AS ada_silabus,
        CASE
            WHEN (tm.ada_bahan_ajar = true) THEN 1
            ELSE 0
        END AS ada_bahan_ajar,
        CASE
            WHEN (tm.ada_acara_praktek = true) THEN 1
            ELSE 0
        END AS ada_acara_praktek,
        CASE
            WHEN (tm.ada_diktat = true) THEN 1
            ELSE 0
        END AS ada_diktat,
    tm.tanggal_mulai_efektif,
    tm.tanggal_akhir_efektif
   FROM (((public.tb_matakuliah tm
     JOIN public.tb_mst_prodi tmp ON ((tmp.id = tm.id_prodi)))
     JOIN public.tb_mst_jenjang_pend tmjp ON ((tmjp.id = tmp.id_jenjang_pendidikan)))
     JOIN public.tb_mst_jenis_matkul tmjm ON (((tmjm.id)::text = (tm.id_jenis_mata_kuliah)::text)))
  WHERE (tm.syncron_feeder = false);


ALTER TABLE public.insertmatakuliah OWNER TO postgres;

--
-- Name: tb_nilai_transfer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_nilai_transfer (
    id integer NOT NULL,
    id_riwayat_pendidikan integer,
    matkul_asal character varying,
    sks_matkul_asal numeric(2,0),
    nilai_huruf_asal character varying(3),
    id_matkul integer,
    sks_matkul numeric(2,0),
    nilai_huruf character varying(3),
    nilai_akhir numeric(5,2),
    id_perguruan_tinggi uuid,
    id_transfer uuid,
    is_syncron_feeder boolean,
    keterangan_error character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    nilai_bobot numeric(5,2),
    kode_matkul_asal character varying,
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_nilai_transfer OWNER TO postgres;

--
-- Name: COLUMN tb_nilai_transfer.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_nilai_transfer.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_nilai_transfer.matkul_asal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_nilai_transfer.matkul_asal IS 'nama matakuliah asal';


--
-- Name: COLUMN tb_nilai_transfer.sks_matkul_asal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_nilai_transfer.sks_matkul_asal IS 'sks matakuliah asal';


--
-- Name: COLUMN tb_nilai_transfer.nilai_huruf_asal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_nilai_transfer.nilai_huruf_asal IS 'nilai huru asal';


--
-- Name: COLUMN tb_nilai_transfer.id_matkul; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_nilai_transfer.id_matkul IS 'tb_matakuliah';


--
-- Name: COLUMN tb_nilai_transfer.sks_matkul; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_nilai_transfer.sks_matkul IS 'sks matakuliah diakui';


--
-- Name: COLUMN tb_nilai_transfer.nilai_huruf; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_nilai_transfer.nilai_huruf IS 'nilai huruf diakui';


--
-- Name: COLUMN tb_nilai_transfer.nilai_akhir; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_nilai_transfer.nilai_akhir IS 'nilai angka diakui';


--
-- Name: COLUMN tb_nilai_transfer.id_transfer; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_nilai_transfer.id_transfer IS 'id dari feeder';


--
-- Name: COLUMN tb_nilai_transfer.is_syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_nilai_transfer.is_syncron_feeder IS 'apakah sudah syncron dengan feeder';


--
-- Name: COLUMN tb_nilai_transfer.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_nilai_transfer.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: COLUMN tb_nilai_transfer.kode_matkul_asal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_nilai_transfer.kode_matkul_asal IS 'kode matakuliah asal';


--
-- Name: insertnilaitransferpendidikanmahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertnilaitransferpendidikanmahasiswa AS
 SELECT tnt.id AS id_nilai_transfer,
    trp.id_prodi,
    trp.id_registrasi_mahasiswa,
    tnt.kode_matkul_asal AS kode_mata_kuliah_asal,
    tnt.matkul_asal AS nama_mata_kuliah_asal,
    tnt.sks_matkul_asal AS sks_mata_kuliah_asal,
    tnt.nilai_huruf_asal,
    tm.id_matkul,
    tnt.sks_matkul AS sks_mata_kuliah_diakui,
    tnt.nilai_huruf AS nilai_huruf_diakui,
    tnt.nilai_akhir AS nilai_angka_diakui,
    tnt.id_perguruan_tinggi
   FROM ((public.tb_nilai_transfer tnt
     JOIN public.tb_riwayat_pendidikan trp ON ((trp.id = tnt.id_riwayat_pendidikan)))
     JOIN public.tb_matakuliah tm ON ((tm.id = tnt.id_matkul)))
  WHERE (tnt.is_syncron_feeder = false);


ALTER TABLE public.insertnilaitransferpendidikanmahasiswa OWNER TO postgres;

--
-- Name: tb_akm; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_akm (
    id bigint NOT NULL,
    id_riwayat_pendidikan integer,
    id_status_mahasiswa character varying,
    ips numeric DEFAULT 0,
    ipk numeric DEFAULT 0,
    sks_semester numeric DEFAULT 0,
    total_sks numeric DEFAULT 0,
    biaya_kuliah_smt numeric DEFAULT 0,
    tahun_ajaran numeric,
    id_semester numeric(1,0),
    semester_angka character varying,
    modified timestamp without time zone,
    user_modified character varying,
    uploaded timestamp without time zone,
    is_syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_akm OWNER TO postgres;

--
-- Name: COLUMN tb_akm.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_akm.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_akm.id_status_mahasiswa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_akm.id_status_mahasiswa IS 'tb_mst_status_mahasiswa';


--
-- Name: COLUMN tb_akm.ips; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_akm.ips IS 'Indeks Prestasi Semester

dihitung berdasarkan matakuliah yang dipilih';


--
-- Name: COLUMN tb_akm.ipk; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_akm.ipk IS 'Indeks Prestasi Kumulatif

yang dihitung berdasarkan semua matakuliah yang dipilih atau dianggap lulus atau nilai terbaik dari matakuliah oleh prodi';


--
-- Name: COLUMN tb_akm.sks_semester; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_akm.sks_semester IS 'Total SKS yang diambil mahasiswa pada semester ini;

field ini diisi saat tutup periode tahun ajaran';


--
-- Name: COLUMN tb_akm.total_sks; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_akm.total_sks IS 'Total SKS yang diambil mahasiswa sejak awal masuk,

field ini diisi saat tutup periode tahun ajaran';


--
-- Name: COLUMN tb_akm.biaya_kuliah_smt; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_akm.biaya_kuliah_smt IS 'Total biaya kuliah mahasiswa pada semester ini';


--
-- Name: COLUMN tb_akm.semester_angka; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_akm.semester_angka IS 'semester berlangsung kuliah';


--
-- Name: COLUMN tb_akm.is_syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_akm.is_syncron_feeder IS 'apakah sudah melakukan syncron ke feeder';


--
-- Name: COLUMN tb_akm.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_akm.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: insertperkuliahanmahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertperkuliahanmahasiswa AS
 SELECT ta.id AS id_akm,
    trp.id_prodi,
    trp.id_registrasi_mahasiswa,
    concat(ta.tahun_ajaran, ta.id_semester) AS id_semester,
    ta.id_status_mahasiswa,
    ta.ips,
    ta.ipk,
    ta.sks_semester,
    ta.total_sks,
        CASE
            WHEN ((ta.id_status_mahasiswa)::text = 'A'::text) THEN ta.biaya_kuliah_smt
            ELSE (0)::numeric
        END AS biaya_kuliah_smt
   FROM (public.tb_akm ta
     JOIN public.tb_riwayat_pendidikan trp ON ((trp.id = ta.id_riwayat_pendidikan)))
  WHERE (ta.is_syncron_feeder = false);


ALTER TABLE public.insertperkuliahanmahasiswa OWNER TO postgres;

--
-- Name: tb_unit_kelas_mahasiswa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_unit_kelas_mahasiswa (
    id bigint NOT NULL,
    id_riwayat_pendidikan bigint,
    id_unit_kelas bigint,
    id_krs_detail bigint,
    syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_unit_kelas_mahasiswa OWNER TO postgres;

--
-- Name: COLUMN tb_unit_kelas_mahasiswa.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas_mahasiswa.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_unit_kelas_mahasiswa.id_unit_kelas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas_mahasiswa.id_unit_kelas IS 'tb_unit_kelas';


--
-- Name: COLUMN tb_unit_kelas_mahasiswa.id_krs_detail; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas_mahasiswa.id_krs_detail IS 'tb_krs_detail';


--
-- Name: COLUMN tb_unit_kelas_mahasiswa.syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas_mahasiswa.syncron_feeder IS 'apakah sudah melakukan syncron feeder';


--
-- Name: COLUMN tb_unit_kelas_mahasiswa.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_unit_kelas_mahasiswa.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: insertpesertakelaskuliah; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertpesertakelaskuliah AS
 SELECT tukm.id AS id_unit_kelas_mhs,
    tuk.id_prodi,
    tuk.id_kelas_kuliah,
    trp.id_registrasi_mahasiswa
   FROM ((public.tb_unit_kelas_mahasiswa tukm
     JOIN public.tb_riwayat_pendidikan trp ON ((trp.id = tukm.id_riwayat_pendidikan)))
     JOIN public.tb_unit_kelas tuk ON ((tuk.id = tukm.id_unit_kelas)))
  WHERE (tukm.syncron_feeder = false);


ALTER TABLE public.insertpesertakelaskuliah OWNER TO postgres;

--
-- Name: tb_mst_jenis_prestasi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jenis_prestasi (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_jenis_prestasi OWNER TO postgres;

--
-- Name: tb_mst_tingkat_prestasi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_tingkat_prestasi (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_tingkat_prestasi OWNER TO postgres;

--
-- Name: tb_prestasi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_prestasi (
    id integer NOT NULL,
    id_mahasiswa integer,
    id_aktivitas_mahasiswa integer,
    id_jenis_prestasi integer,
    id_tingkat_prestasi integer,
    nama character varying,
    tahun numeric(4,0),
    penyelenggara character varying,
    peringkat numeric,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    is_syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    id_prestasi uuid,
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_prestasi OWNER TO postgres;

--
-- Name: COLUMN tb_prestasi.id_mahasiswa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_prestasi.id_mahasiswa IS 'tb_mahasiswa';


--
-- Name: COLUMN tb_prestasi.id_aktivitas_mahasiswa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_prestasi.id_aktivitas_mahasiswa IS 'tb_aktivitas_mahasiswa';


--
-- Name: COLUMN tb_prestasi.id_jenis_prestasi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_prestasi.id_jenis_prestasi IS 'tb_mst_jenis_prestasi';


--
-- Name: COLUMN tb_prestasi.id_tingkat_prestasi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_prestasi.id_tingkat_prestasi IS 'tb_mst_tingkat_prestasi';


--
-- Name: COLUMN tb_prestasi.is_syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_prestasi.is_syncron_feeder IS 'apakah sudah melakukan syncron feeder';


--
-- Name: COLUMN tb_prestasi.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_prestasi.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: COLUMN tb_prestasi.id_prestasi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_prestasi.id_prestasi IS 'id feeder';


--
-- Name: insertprestasimahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertprestasimahasiswa AS
 SELECT tp.id AS id_prestasi,
    trp.id_prodi,
    tm.id_mahasiswa,
    tp.id_jenis_prestasi,
    tp.id_tingkat_prestasi,
    tp.nama AS nama_prestasi,
    tp.tahun AS tahun_prestasi,
    tp.penyelenggara,
    tp.peringkat,
    NULL::text AS id_aktivitas,
    tp.id_prestasi AS id_feeder
   FROM ((((public.tb_prestasi tp
     JOIN public.tb_mst_jenis_prestasi tmjp ON ((tmjp.id = tp.id_jenis_prestasi)))
     JOIN public.tb_mst_tingkat_prestasi tmtp ON ((tmtp.id = tp.id_tingkat_prestasi)))
     JOIN public.tb_mahasiswa tm ON ((tm.id = tp.id_mahasiswa)))
     JOIN public.tb_riwayat_pendidikan trp ON ((trp.id_mahasiswa = tp.id_mahasiswa)))
  WHERE (tp.is_syncron_feeder = false);


ALTER TABLE public.insertprestasimahasiswa OWNER TO postgres;

--
-- Name: tb_rencana_pembelajaran_matkul; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_rencana_pembelajaran_matkul (
    id bigint NOT NULL,
    pertemuan numeric,
    id_matakuliah integer,
    materi text,
    materi_inggris text,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    is_syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    id_rencana_ajar uuid,
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_rencana_pembelajaran_matkul OWNER TO postgres;

--
-- Name: COLUMN tb_rencana_pembelajaran_matkul.id_matakuliah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_rencana_pembelajaran_matkul.id_matakuliah IS 'tb_matakuliah';


--
-- Name: COLUMN tb_rencana_pembelajaran_matkul.is_syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_rencana_pembelajaran_matkul.is_syncron_feeder IS 'apakah sudah melakukan syncron feeder';


--
-- Name: COLUMN tb_rencana_pembelajaran_matkul.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_rencana_pembelajaran_matkul.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: COLUMN tb_rencana_pembelajaran_matkul.id_rencana_ajar; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_rencana_pembelajaran_matkul.id_rencana_ajar IS 'id feeder';


--
-- Name: insertrencanapembelajaran; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertrencanapembelajaran AS
 SELECT trpm.id AS id_rencana_ajar_master,
    tm.id_prodi,
    tm.id_matkul,
    trpm.pertemuan,
    trpm.materi AS materi_indonesia,
    trpm.materi_inggris
   FROM (public.tb_rencana_pembelajaran_matkul trpm
     JOIN public.tb_matakuliah tm ON ((tm.id = trpm.id_matakuliah)))
  WHERE (trpm.is_syncron_feeder = false);


ALTER TABLE public.insertrencanapembelajaran OWNER TO postgres;

--
-- Name: insertriwayatpendidikanmahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertriwayatpendidikanmahasiswa AS
 SELECT trp.id AS id_riwayat_pendidikan,
    trp.id_prodi AS id_master_prodi,
    tm.id_mahasiswa,
    trp.nim,
    trp.id_jenis_daftar,
    trp.id_jalur_daftar,
    trp.id_periode_masuk,
    trp.tanggal_daftar,
    trp.id_perguruan_tinggi,
    tmp.id_prodi,
    trp.id_bidang_minat,
    trp.sks_diakui,
    trp.id_perguruan_tinggi_asal,
    trp.id_prodi_asal,
    trp.id_pembiayaan,
    trp.biaya_masuk
   FROM ((public.tb_riwayat_pendidikan trp
     JOIN public.tb_mahasiswa tm ON ((tm.id = trp.id_mahasiswa)))
     JOIN public.tb_mst_prodi tmp ON ((tmp.id = trp.id_prodi)))
  WHERE (trp.syncron_feeder = false);


ALTER TABLE public.insertriwayatpendidikanmahasiswa OWNER TO postgres;

--
-- Name: tb_penguji; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_penguji (
    id integer NOT NULL,
    id_aktivitas_mahasiswa integer,
    id_dosen integer,
    id_kategori_kegiatan character varying,
    penguji_ke numeric(2,0),
    id_uji uuid,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    date_syncron timestamp without time zone
);


ALTER TABLE public.tb_penguji OWNER TO postgres;

--
-- Name: COLUMN tb_penguji.id_aktivitas_mahasiswa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_penguji.id_aktivitas_mahasiswa IS 'tb_aktivitas_mahasiswa';


--
-- Name: COLUMN tb_penguji.id_dosen; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_penguji.id_dosen IS 'tb_dosen';


--
-- Name: COLUMN tb_penguji.id_kategori_kegiatan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_penguji.id_kategori_kegiatan IS 'tb_mst_kategori_kegiatan';


--
-- Name: COLUMN tb_penguji.id_uji; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_penguji.id_uji IS 'id feeder';


--
-- Name: COLUMN tb_penguji.syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_penguji.syncron_feeder IS 'apakah sudah melakukan syncron feeder';


--
-- Name: COLUMN tb_penguji.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_penguji.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: insertujimahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.insertujimahasiswa AS
 SELECT tp.id,
    tam.id_prodi,
    tam.id_aktivitas,
    tp.id_kategori_kegiatan,
    td.id_dosen,
    tp.penguji_ke
   FROM ((public.tb_penguji tp
     JOIN public.tb_aktivitas_mahasiswa tam ON ((tam.id = tp.id_aktivitas_mahasiswa)))
     JOIN public.tb_dosen td ON ((td.id = tp.id_dosen)))
  WHERE (tp.syncron_feeder = false);


ALTER TABLE public.insertujimahasiswa OWNER TO postgres;

--
-- Name: tb_krs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_krs (
    id bigint NOT NULL,
    id_riwayat_pendidikan integer,
    tahun_ajaran numeric,
    id_semester numeric(1,0),
    sks_diberikan numeric DEFAULT 24,
    sks_diambil numeric DEFAULT 0,
    bobot numeric DEFAULT 0,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    semester_angka numeric,
    krs_disetujui boolean DEFAULT false,
    ip numeric DEFAULT 0,
    ipk numeric DEFAULT 0,
    is_active boolean DEFAULT false,
    is_syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    sudah_isi_survey boolean DEFAULT false
);


ALTER TABLE public.tb_krs OWNER TO postgres;

--
-- Name: COLUMN tb_krs.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_krs.bobot; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs.bobot IS 'sum nilai_bobot yang ada di krs detail';


--
-- Name: COLUMN tb_krs.ipk; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs.ipk IS 'dihitung berdasarkan semua matakuliah yang telah diambil oleh mahasiswa';


--
-- Name: COLUMN tb_krs.is_active; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs.is_active IS 'row krs yang digunakan untuk transkrip, field ini di update jadi true pada saat tutup periode tahun ajaran';


--
-- Name: COLUMN tb_krs.is_syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs.is_syncron_feeder IS 'apakah sudah syncron ke feeder';


--
-- Name: COLUMN tb_krs.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: COLUMN tb_krs.sudah_isi_survey; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs.sudah_isi_survey IS 'apakah mahasiswa telah melakukan pengisian survey';


--
-- Name: tb_krs_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_krs_detail (
    id bigint NOT NULL,
    id_krs integer,
    id_matakuliah integer,
    semester_matkul numeric,
    nilai_akhir numeric,
    nilai_huruf character varying(2) DEFAULT 'T'::character varying,
    nilai_bobot numeric,
    syncron_feeder boolean DEFAULT false,
    error_feeder text,
    uploaded timestamp without time zone,
    user_modified character varying,
    nilai_json json,
    modified timestamp without time zone,
    is_lulus boolean,
    status_pengajuan boolean DEFAULT false,
    is_digunakan boolean DEFAULT true,
    date_approve timestamp without time zone,
    user_approve character varying,
    date_syncron timestamp without time zone,
    is_pemutihan boolean DEFAULT false
);


ALTER TABLE public.tb_krs_detail OWNER TO postgres;

--
-- Name: COLUMN tb_krs_detail.id_krs; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_detail.id_krs IS 'tb_krs';


--
-- Name: COLUMN tb_krs_detail.id_matakuliah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_detail.id_matakuliah IS 'tb_matakuliah';


--
-- Name: COLUMN tb_krs_detail.nilai_bobot; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_detail.nilai_bobot IS 'bobot nilai huruf * sks matakuliah';


--
-- Name: COLUMN tb_krs_detail.is_lulus; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_detail.is_lulus IS 'apakah matakuliah ini lulus';


--
-- Name: COLUMN tb_krs_detail.status_pengajuan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_detail.status_pengajuan IS 'apakah status pengajuan matakuliah ini telah disetujui oleh prodi atau tidak';


--
-- Name: COLUMN tb_krs_detail.is_digunakan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_detail.is_digunakan IS 'daftar matakuliah yang digunakan untuk transkrip';


--
-- Name: COLUMN tb_krs_detail.is_pemutihan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_detail.is_pemutihan IS 'apakah nilai akhir dan huruf hasil pemutihan';


--
-- Name: tb_mst_periode; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_periode (
    id integer NOT NULL,
    tahun_ajaran numeric(4,0),
    id_semester numeric(1,0),
    nama_semester character varying,
    periode_aktif boolean DEFAULT true,
    tanggal_mulai date,
    tanggal_selesai date,
    user_modified character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.tb_mst_periode OWNER TO postgres;

--
-- Name: ip_ipk_akm_periode_aktif; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.ip_ipk_akm_periode_aktif AS
 SELECT tk.id AS id_krs,
    tk.semester_angka,
    COALESCE(tkd.nilai_bobot, (0)::numeric) AS bobot,
    COALESCE(tkd.sks_mata_kuliah, (0)::numeric) AS sks_dicapai,
    COALESCE(round((tkd.nilai_bobot / tkd.sks_mata_kuliah), 2), (0)::numeric) AS ip,
        CASE
            WHEN (tk.semester_angka = (1)::numeric) THEN COALESCE(round((tkd.nilai_bobot / tkd.sks_mata_kuliah), 2), (0)::numeric)
            ELSE public.hitung_ipk_akm(tk.id_riwayat_pendidikan)
        END AS ipk,
    ta.id AS id_akm,
    tk.sks_diambil
   FROM ((public.tb_krs tk
     LEFT JOIN ( SELECT tkd2.id_krs,
            COALESCE(sum(tkd2.nilai_bobot), (0)::numeric) AS nilai_bobot,
            COALESCE(sum(tm.sks_mata_kuliah), (0)::numeric) AS sks_mata_kuliah
           FROM (public.tb_krs_detail tkd2
             JOIN public.tb_matakuliah tm ON ((tm.id = tkd2.id_matakuliah)))
          WHERE (((tkd2.nilai_huruf)::text <> 'T'::text) AND (tkd2.is_digunakan = true))
          GROUP BY tkd2.id_krs) tkd ON ((tkd.id_krs = tk.id)))
     JOIN public.tb_akm ta ON (((ta.id_riwayat_pendidikan = tk.id_riwayat_pendidikan) AND (concat(ta.tahun_ajaran, ta.id_semester) = concat(tk.tahun_ajaran, tk.id_semester)))))
  WHERE (concat(tk.tahun_ajaran, tk.id_semester) = ( SELECT concat(tb_mst_periode.tahun_ajaran, tb_mst_periode.id_semester) AS concat
           FROM public.tb_mst_periode
          WHERE (tb_mst_periode.periode_aktif = true)));


ALTER TABLE public.ip_ipk_akm_periode_aktif OWNER TO postgres;

--
-- Name: ip_ipk_krs_periode_aktif; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.ip_ipk_krs_periode_aktif AS
 SELECT tk.id AS id_krs,
    tk.semester_angka,
    COALESCE(tkd.nilai_bobot, (0)::numeric) AS nilai_bobot,
    COALESCE(tkd.sks_matakuliah, (0)::numeric) AS sks_matakuliah,
    COALESCE(round((tkd.nilai_bobot / tkd.sks_matakuliah), 2), (0)::numeric) AS ip,
        CASE
            WHEN (tk.semester_angka = (1)::numeric) THEN COALESCE(round((tkd.nilai_bobot / tkd.sks_matakuliah), 2), (0)::numeric)
            ELSE public.hitung_ipk_krs(tk.id_riwayat_pendidikan)
        END AS ipk
   FROM (public.tb_krs tk
     LEFT JOIN ( SELECT tkd2.id_krs,
            COALESCE(sum(tkd2.nilai_bobot), (0)::numeric) AS nilai_bobot,
            COALESCE(sum(tm.sks_mata_kuliah), (0)::numeric) AS sks_matakuliah
           FROM (public.tb_krs_detail tkd2
             JOIN public.tb_matakuliah tm ON ((tm.id = tkd2.id_matakuliah)))
          WHERE ((tkd2.nilai_huruf)::text <> 'T'::text)
          GROUP BY tkd2.id_krs) tkd ON ((tkd.id_krs = tk.id)))
  WHERE (concat(tk.tahun_ajaran, tk.id_semester) = ( SELECT concat(tb_mst_periode.tahun_ajaran, tb_mst_periode.id_semester) AS concat
           FROM public.tb_mst_periode
          WHERE (tb_mst_periode.periode_aktif = true)));


ALTER TABLE public.ip_ipk_krs_periode_aktif OWNER TO postgres;

--
-- Name: ip_ipk_periode_aktif; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.ip_ipk_periode_aktif AS
 SELECT tk.id AS id_krs,
    tk.semester_angka,
    COALESCE(tkd.nilai_bobot, (0)::numeric) AS bobot,
    COALESCE(tkd2.sks_matakuliah, (0)::numeric) AS sks_dicapai,
    round(COALESCE((tkd.nilai_bobot / tkd2.sks_matakuliah), (0)::numeric), 2) AS ip,
        CASE
            WHEN (tk.semester_angka = (1)::numeric) THEN round(COALESCE((tkd.nilai_bobot / tkd2.sks_matakuliah), (0)::numeric), 2)
            ELSE public.hitung_ipk_krs(tk.id_riwayat_pendidikan)
        END AS ipk
   FROM ((public.tb_krs tk
     LEFT JOIN ( SELECT tkd_1.id_krs,
            sum(tkd_1.nilai_bobot) AS nilai_bobot
           FROM public.tb_krs_detail tkd_1
          WHERE ((tkd_1.nilai_huruf)::text <> 'T'::text)
          GROUP BY tkd_1.id_krs) tkd ON ((tkd.id_krs = tk.id)))
     LEFT JOIN ( SELECT tkd2_1.id_krs,
            sum(tm.sks_mata_kuliah) AS sks_matakuliah
           FROM (public.tb_krs_detail tkd2_1
             JOIN public.tb_matakuliah tm ON ((tm.id = tkd2_1.id_matakuliah)))
          WHERE ((tkd2_1.nilai_huruf)::text <> 'T'::text)
          GROUP BY tkd2_1.id_krs) tkd2 ON ((tkd2.id_krs = tk.id)))
  WHERE ((tk.is_active = true) AND (concat(tk.tahun_ajaran, tk.id_semester) = ( SELECT concat(tmp.tahun_ajaran, tmp.id_semester) AS concat
           FROM public.tb_mst_periode tmp
          WHERE (tmp.periode_aktif = true))))
  ORDER BY tk.id;


ALTER TABLE public.ip_ipk_periode_aktif OWNER TO postgres;

--
-- Name: last_no_pendaftaran; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.last_no_pendaftaran (
    "coalesce" numeric
);


ALTER TABLE public.last_no_pendaftaran OWNER TO postgres;

--
-- Name: tb_absensi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_absensi (
    id bigint NOT NULL,
    id_roster integer,
    tanggal_absen date,
    is_sudah_absen boolean DEFAULT false,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_absensi OWNER TO postgres;

--
-- Name: TABLE tb_absensi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.tb_absensi IS 'absensi dosen';


--
-- Name: COLUMN tb_absensi.id_roster; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_absensi.id_roster IS 'tb_roster';


--
-- Name: COLUMN tb_absensi.is_sudah_absen; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_absensi.is_sudah_absen IS 'apakah sudah dosen atau asisten sudah melakukan absen';


--
-- Name: tb_absensi_mahasiswa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_absensi_mahasiswa (
    id bigint NOT NULL,
    id_roster integer,
    id_riwayat_pendidikan integer,
    tanggal_absen timestamp without time zone,
    user_modified character varying,
    modified timestamp without time zone
);


ALTER TABLE public.tb_absensi_mahasiswa OWNER TO postgres;

--
-- Name: COLUMN tb_absensi_mahasiswa.id_roster; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_absensi_mahasiswa.id_roster IS 'tb_roster';


--
-- Name: COLUMN tb_absensi_mahasiswa.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_absensi_mahasiswa.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: tb_mst_jam_belajar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jam_belajar (
    id integer NOT NULL,
    jam_belajar character varying
);


ALTER TABLE public.tb_mst_jam_belajar OWNER TO postgres;

--
-- Name: list_absensi_mahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_absensi_mahasiswa AS
 SELECT tr.id AS id_roster,
    tukm.id_riwayat_pendidikan,
    tuk.tahun_ajaran,
    tuk.id_semester,
    tuk.kode_unit,
    tm.kode AS kode_matkul,
    tm.nama AS nama_matkul,
    ta.tanggal_absen,
    tam.tanggal_absen AS tanggal_absen_mhs,
    public.jam_belajar(tmjb.jam_belajar, tmjb2.jam_belajar) AS waktu_absensi
   FROM ((((((((public.tb_unit_kelas_mahasiswa tukm
     JOIN public.tb_unit_kelas tuk ON ((tuk.id = tukm.id_unit_kelas)))
     JOIN public.tb_roster tr ON ((tr.id_unit_kelas = tukm.id_unit_kelas)))
     JOIN public.tb_absensi ta ON ((ta.id_roster = tr.id)))
     JOIN public.tb_krs_detail tkd ON ((tkd.id = tukm.id_krs_detail)))
     JOIN public.tb_matakuliah tm ON ((tm.id = tkd.id_matakuliah)))
     LEFT JOIN public.tb_absensi_mahasiswa tam ON (((tam.id_riwayat_pendidikan = tukm.id_riwayat_pendidikan) AND (tam.id_roster = tr.id) AND ((to_char(tam.tanggal_absen, 'YYYY-MM-DD'::text))::date = ta.tanggal_absen))))
     JOIN public.tb_mst_jam_belajar tmjb ON ((tmjb.id = tr.id_jam_masuk)))
     JOIN public.tb_mst_jam_belajar tmjb2 ON ((tmjb2.id = tr.id_jam_keluar)))
  WHERE (tkd.status_pengajuan = true);


ALTER TABLE public.list_absensi_mahasiswa OWNER TO postgres;

--
-- Name: tb_mst_status_mahasiswa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_status_mahasiswa (
    id character varying(1) NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_status_mahasiswa OWNER TO postgres;

--
-- Name: list_aktivitas_perkuliahan; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_aktivitas_perkuliahan AS
 SELECT ta.id_riwayat_pendidikan,
    trp.id_mahasiswa,
    trp.nim,
    tm.nama_mahasiswa,
    trp.ta_masuk AS angkatan,
    ta.tahun_ajaran,
    ta.id_semester,
    ta.id_status_mahasiswa,
    tmsm.nama AS nama_status_mahasiswa,
    ta.ips,
    ta.ipk,
    ta.sks_semester,
    ta.total_sks,
    trp.id_prodi,
    concat(tmjp.nama, ' ', tmp.nama) AS nama_prodi,
    concat(ta.tahun_ajaran, ta.id_semester) AS periode,
    ta.id AS id_akm
   FROM (((((public.tb_akm ta
     JOIN public.tb_riwayat_pendidikan trp ON ((trp.id = ta.id_riwayat_pendidikan)))
     JOIN public.tb_mahasiswa tm ON ((tm.id = trp.id_mahasiswa)))
     JOIN public.tb_mst_status_mahasiswa tmsm ON (((tmsm.id)::text = (ta.id_status_mahasiswa)::text)))
     JOIN public.tb_mst_prodi tmp ON ((tmp.id = trp.id_prodi)))
     JOIN public.tb_mst_jenjang_pend tmjp ON ((tmjp.id = tmp.id_jenjang_pendidikan)))
  WHERE (trp.id_jenis_keluar IS NULL);


ALTER TABLE public.list_aktivitas_perkuliahan OWNER TO postgres;

--
-- Name: list_dosen; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_dosen AS
 SELECT td.id,
    public.nama_lengkap(td.gelar_depan, td.nama, td.gelar_belakang) AS nama_dosen,
    td.nidn,
    td.nip,
    td.jenis_kelamin,
    td.id_agama,
    tma.nama AS nama_agama,
    td.tanggal_lahir,
    td.id_status_aktif,
    tmskp.nama AS nama_status_aktif,
    td.id_prodi,
    concat(tmjp.nama, ' ', tmp.nama) AS nama_prodi,
    td.email,
    tmp.id_fakultas
   FROM ((((public.tb_dosen td
     LEFT JOIN public.tb_mst_agama tma ON ((tma.id = td.id_agama)))
     LEFT JOIN public.tb_mst_status_keaktifan_pegawai tmskp ON ((tmskp.id = td.id_status_aktif)))
     LEFT JOIN public.tb_mst_prodi tmp ON ((tmp.id = td.id_prodi)))
     LEFT JOIN public.tb_mst_jenjang_pend tmjp ON ((tmjp.id = tmp.id_jenjang_pendidikan)));


ALTER TABLE public.list_dosen OWNER TO postgres;

--
-- Name: list_histori_nilai_mahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_histori_nilai_mahasiswa AS
 SELECT tk.id_riwayat_pendidikan,
    tm.kode AS kode_mk,
    tm.nama AS nama_mk,
    tm.sks_mata_kuliah,
    COALESCE(tkd.nilai_akhir, (0)::numeric) AS nilai_akhir,
    tkd.nilai_huruf,
    COALESCE(round((tkd.nilai_bobot / tm.sks_mata_kuliah), 2), (0)::numeric) AS indeks,
    COALESCE(tkd.nilai_bobot, (0)::numeric) AS nilai_bobot,
    concat(tk.tahun_ajaran, tk.id_semester) AS periode,
    tkd.is_digunakan
   FROM ((public.tb_krs tk
     JOIN public.tb_krs_detail tkd ON ((tkd.id_krs = tk.id)))
     JOIN public.tb_matakuliah tm ON ((tm.id = tkd.id_matakuliah)))
  WHERE (tk.is_active = true);


ALTER TABLE public.list_histori_nilai_mahasiswa OWNER TO postgres;

--
-- Name: tb_mst_all_prodi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_all_prodi (
    id uuid NOT NULL,
    id_perguruan_tinggi uuid,
    kode character varying,
    nama character varying,
    status character varying(1),
    id_jenjang_pendidikan integer
);


ALTER TABLE public.tb_mst_all_prodi OWNER TO postgres;

--
-- Name: tb_mst_all_pt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_all_pt (
    id uuid NOT NULL,
    kode character varying,
    nama character varying,
    nama_singkat character varying
);


ALTER TABLE public.tb_mst_all_pt OWNER TO postgres;

--
-- Name: tb_mst_jalurmasuk; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jalurmasuk (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_jalurmasuk OWNER TO postgres;

--
-- Name: tb_mst_jenis_pendaftaran; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jenis_pendaftaran (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_jenis_pendaftaran OWNER TO postgres;

--
-- Name: tb_mst_pembiayaan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_pembiayaan (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_pembiayaan OWNER TO postgres;

--
-- Name: list_histori_pend_mhs; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_histori_pend_mhs AS
 SELECT trp.id,
    trp.id_mahasiswa,
    trp.nim,
    trp.id_jenis_daftar,
    tmjp.nama AS jenis_pendaftaran,
    trp.id_jalur_daftar,
    tmj.nama AS jalurmasuk,
    trp.tanggal_daftar,
    trp.id_prodi,
    concat(tmjp2.nama, ' ', tmp.nama) AS prodi,
    trp.id_bidang_minat,
    trp.sks_diakui,
    trp.id_perguruan_tinggi_asal,
    tmap.nama AS nama_perguruan_tinggi_asal,
    trp.id_prodi_asal,
    tmap2.nama AS nama_prodi_asal,
    trp.id_pembiayaan,
    tmp2.nama AS pembiayaan,
    trp.biaya_masuk,
    trp.id_status,
    tmsm.nama AS status_mahasiswa,
    trp.ta_masuk,
    trp.id_periode_masuk,
    trp.id_kurikulum,
    tk.nama AS nama_kurikulum,
    tm.nama_mahasiswa,
    trp.id_spp,
    trp.id_ukt
   FROM ((((((((((public.tb_riwayat_pendidikan trp
     LEFT JOIN public.tb_mst_jenis_pendaftaran tmjp ON ((tmjp.id = trp.id_jenis_daftar)))
     LEFT JOIN public.tb_mst_jalurmasuk tmj ON ((tmj.id = trp.id_jalur_daftar)))
     LEFT JOIN public.tb_mst_prodi tmp ON ((tmp.id = trp.id_prodi)))
     LEFT JOIN public.tb_mst_jenjang_pend tmjp2 ON ((tmjp2.id = tmp.id_jenjang_pendidikan)))
     LEFT JOIN public.tb_mst_all_pt tmap ON ((tmap.id = trp.id_perguruan_tinggi_asal)))
     LEFT JOIN public.tb_mst_all_prodi tmap2 ON ((tmap2.id = trp.id_prodi_asal)))
     LEFT JOIN public.tb_mst_pembiayaan tmp2 ON ((tmp2.id = trp.id_pembiayaan)))
     LEFT JOIN public.tb_mst_status_mahasiswa tmsm ON (((tmsm.id)::text = (trp.id_status)::text)))
     LEFT JOIN public.tb_kurikulum tk ON ((tk.id = trp.id_kurikulum)))
     JOIN public.tb_mahasiswa tm ON ((tm.id = trp.id_mahasiswa)));


ALTER TABLE public.list_histori_pend_mhs OWNER TO postgres;

--
-- Name: list_lama_kuliah; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_lama_kuliah AS
 SELECT tm.nama_mahasiswa,
    trp.nim,
    ta.total_sks,
    tmsm.nama AS sts_mahasiswa,
    trp.ta_masuk AS angkatan,
    tmjp.lama_kuliah AS ideal_kuliah,
    ta.lama_studi,
    concat(tmjp.nama, ' ', tmp.nama) AS nama_prodi,
    trp.id_prodi,
    tmp.id_fakultas
   FROM (((((public.tb_riwayat_pendidikan trp
     JOIN public.tb_mahasiswa tm ON ((tm.id = trp.id_mahasiswa)))
     LEFT JOIN ( SELECT tb_akm.id_riwayat_pendidikan,
            sum(tb_akm.sks_semester) AS total_sks,
            count(*) AS lama_studi
           FROM public.tb_akm
          GROUP BY tb_akm.id_riwayat_pendidikan) ta ON ((ta.id_riwayat_pendidikan = trp.id)))
     JOIN public.tb_mst_status_mahasiswa tmsm ON (((tmsm.id)::text = (trp.id_status)::text)))
     JOIN public.tb_mst_prodi tmp ON ((tmp.id = trp.id_prodi)))
     JOIN public.tb_mst_jenjang_pend tmjp ON ((tmjp.id = tmp.id_jenjang_pendidikan)))
  WHERE (trp.id_jenis_keluar IS NULL);


ALTER TABLE public.list_lama_kuliah OWNER TO postgres;

--
-- Name: tb_mst_jenis_keluar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jenis_keluar (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_jenis_keluar OWNER TO postgres;

--
-- Name: list_lulus_dropout; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_lulus_dropout AS
 SELECT tld.id AS id_lulus_dropout,
    trp.nim,
    tm.nama_mahasiswa,
    trp.ta_masuk AS angkatan,
    tld.id_jenis_keluar,
    tmjk.nama AS nama_jenis_keluar,
    tld.tanggal_keluar,
    tld.tahun_ajaran,
    tld.id_semester,
    concat(tld.tahun_ajaran, tld.id_semester) AS periode,
    trp.id_prodi,
    concat(tmjp.nama, ' ', tmp.nama) AS nama_prodi,
    tld.keterangan,
    tld.nomor_sk,
    tld.tanggal_sk,
    tld.ipk,
    tld.no_ijazah,
    tld.id_riwayat_pendidikan,
    tmp.id_fakultas,
    trp.id_mahasiswa
   FROM (((((public.tb_lulus_dropout tld
     JOIN public.tb_riwayat_pendidikan trp ON ((trp.id = tld.id_riwayat_pendidikan)))
     JOIN public.tb_mahasiswa tm ON ((tm.id = trp.id_mahasiswa)))
     JOIN public.tb_mst_jenis_keluar tmjk ON ((tmjk.id = tld.id_jenis_keluar)))
     JOIN public.tb_mst_prodi tmp ON ((tmp.id = trp.id_prodi)))
     JOIN public.tb_mst_jenjang_pend tmjp ON ((tmjp.id = tmp.id_jenjang_pendidikan)));


ALTER TABLE public.list_lulus_dropout OWNER TO postgres;

--
-- Name: list_mahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_mahasiswa AS
 SELECT tm.id,
    tm.nama_mahasiswa,
    trp.nim,
    tm.jenis_kelamin,
    tm.id_agama,
    tma.nama AS agama,
    tm.tanggal_lahir,
    ta.total_sks AS total_sks_diambil,
    trp.id_prodi,
    concat(tmjp.nama, ' ', tmp.nama) AS prodi,
    trp.id_status AS id_status_mahasiswa,
    tmsm.nama AS status_mahasiswa,
    trp.ta_masuk,
    trp.id AS id_riwayat_pendidikan,
    tm.email,
    tmp.id_fakultas,
    trp.id_jenis_keluar,
    tmjk.nama AS nama_jenis_keluar
   FROM (((((((public.tb_mahasiswa tm
     LEFT JOIN public.tb_riwayat_pendidikan trp ON ((trp.id_mahasiswa = tm.id)))
     LEFT JOIN public.tb_mst_prodi tmp ON ((tmp.id = trp.id_prodi)))
     LEFT JOIN public.tb_mst_jenjang_pend tmjp ON ((tmjp.id = tmp.id_jenjang_pendidikan)))
     LEFT JOIN public.tb_mst_status_mahasiswa tmsm ON (((tmsm.id)::text = (trp.id_status)::text)))
     LEFT JOIN public.tb_mst_agama tma ON ((tma.id = tm.id_agama)))
     LEFT JOIN ( SELECT tb_akm.id_riwayat_pendidikan,
            max(tb_akm.total_sks) AS total_sks
           FROM public.tb_akm
          GROUP BY tb_akm.id_riwayat_pendidikan) ta ON ((ta.id_riwayat_pendidikan = trp.id)))
     LEFT JOIN public.tb_mst_jenis_keluar tmjk ON ((tmjk.id = trp.id_jenis_keluar)));


ALTER TABLE public.list_mahasiswa OWNER TO postgres;

--
-- Name: tb_mst_model_penilaian; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_model_penilaian (
    id integer NOT NULL,
    nama character varying,
    keterangan character varying,
    is_default boolean DEFAULT false,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_model_penilaian OWNER TO postgres;

--
-- Name: list_matakuliah; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_matakuliah AS
 SELECT tm.id AS id_matakuliah,
    tm.kode,
    tm.nama,
    (((tm.sks_tatap_muka + tm.sks_praktek) + tm.sks_praktek_lapangan) + tm.sks_simulasi) AS bobot_matakuliah,
    tm.id_jenis_mata_kuliah,
    tmjm.nama AS jenis_matakuliah,
    tm.id_kelompok_mata_kuliah,
    tm.sks_mata_kuliah,
    tm.sks_tatap_muka,
    tm.sks_praktek,
    tm.sks_praktek_lapangan,
    tm.sks_simulasi,
    tm.metode_kuliah,
    tm.tanggal_mulai_efektif,
    tm.tanggal_akhir_efektif,
    tm.id_model_nilai,
    tmmp.nama AS nama_model_penilaian,
    tm.apakah_unit,
    tm.id_prodi,
    concat(tmjp.nama, ' ', tmp.nama) AS nama_prodi
   FROM ((((public.tb_matakuliah tm
     LEFT JOIN public.tb_mst_jenis_matkul tmjm ON (((tmjm.id)::text = (tm.id_jenis_mata_kuliah)::text)))
     LEFT JOIN public.tb_mst_model_penilaian tmmp ON ((tmmp.id = tm.id_model_nilai)))
     LEFT JOIN public.tb_mst_prodi tmp ON ((tmp.id = tm.id_prodi)))
     LEFT JOIN public.tb_mst_jenjang_pend tmjp ON ((tmjp.id = tmp.id_jenjang_pendidikan)));


ALTER TABLE public.list_matakuliah OWNER TO postgres;

--
-- Name: tb_matkul_kurikulum; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_matkul_kurikulum (
    id bigint NOT NULL,
    id_kurikulum integer,
    id_matkul integer,
    semester numeric(2,0),
    apakah_wajib character varying(1) DEFAULT 0,
    modified timestamp without time zone,
    user_modified character varying,
    is_genap boolean DEFAULT false,
    is_ganjil boolean DEFAULT false,
    is_syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    date_syncron timestamp without time zone,
    syncron_ulang boolean DEFAULT false
);


ALTER TABLE public.tb_matkul_kurikulum OWNER TO postgres;

--
-- Name: COLUMN tb_matkul_kurikulum.id_kurikulum; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_kurikulum.id_kurikulum IS 'tb_kurikulum';


--
-- Name: COLUMN tb_matkul_kurikulum.id_matkul; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_kurikulum.id_matkul IS 'tb_matakuliah';


--
-- Name: COLUMN tb_matkul_kurikulum.semester; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_kurikulum.semester IS 'semester matakuliah (1,2,3...8)';


--
-- Name: COLUMN tb_matkul_kurikulum.apakah_wajib; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_kurikulum.apakah_wajib IS '1:Wajib, 0:Tidak Wajib';


--
-- Name: COLUMN tb_matkul_kurikulum.is_genap; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_kurikulum.is_genap IS 'apakah matakuliah di genap';


--
-- Name: COLUMN tb_matkul_kurikulum.is_ganjil; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_kurikulum.is_ganjil IS 'apakah matakuliah di ganjil';


--
-- Name: COLUMN tb_matkul_kurikulum.is_syncron_feeder; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_kurikulum.is_syncron_feeder IS 'apakah sudah melakukan syncron feeder';


--
-- Name: COLUMN tb_matkul_kurikulum.keterangan_error; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_kurikulum.keterangan_error IS 'keterangan error dari feeder';


--
-- Name: COLUMN tb_matkul_kurikulum.syncron_ulang; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_kurikulum.syncron_ulang IS 'lakukan syncron ulang ke pddikti jika bernilai true';


--
-- Name: list_matkul_kurikulum_mhs; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_matkul_kurikulum_mhs AS
 SELECT trp.id AS id_riwayat_pendidikan,
    tm.kode AS kode_matkul,
    tm.nama AS nama_matkul,
    tmk.semester,
    tm.sks_mata_kuliah,
    tm.sks_tatap_muka,
    tm.sks_praktek,
    tm.sks_praktek_lapangan,
    tm.sks_simulasi,
    tkd.nilai_akhir,
    tkd.nilai_huruf,
    tkd.is_lulus
   FROM ((((public.tb_riwayat_pendidikan trp
     JOIN public.tb_matkul_kurikulum tmk ON ((tmk.id_kurikulum = trp.id_kurikulum)))
     JOIN public.tb_matakuliah tm ON ((tm.id = tmk.id_matkul)))
     LEFT JOIN public.tb_krs tk ON (((tk.id_riwayat_pendidikan = trp.id) AND (tk.semester_angka = tmk.semester))))
     LEFT JOIN public.tb_krs_detail tkd ON (((tkd.id_krs = tk.id) AND (tkd.id_matakuliah = tmk.id_matkul) AND (tkd.semester_matkul = tmk.semester))))
  ORDER BY tmk.semester;


ALTER TABLE public.list_matkul_kurikulum_mhs OWNER TO postgres;

--
-- Name: list_matkul_prodi; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_matkul_prodi AS
 SELECT tm.id AS id_matkul,
    tm.id_prodi,
    tm.kode AS kode_matkul,
    tm.nama AS nama_matkul,
    tm.sks_mata_kuliah,
    (((tm.sks_tatap_muka + tm.sks_praktek) + tm.sks_praktek_lapangan) + tm.sks_simulasi) AS bobot_matakuliah,
    tm.id_jenis_mata_kuliah,
    tmjm.nama AS jenis_matakuliah,
    concat(tmjp.nama, ' ', tmp.nama) AS nama_prodi,
    tm.sks_tatap_muka,
    tm.sks_praktek,
    tm.sks_praktek_lapangan,
    tm.sks_simulasi,
    tm.metode_kuliah,
    tm.tanggal_mulai_efektif,
    tm.tanggal_akhir_efektif,
    tm.id_matkul AS id_matkul_feeder
   FROM (((public.tb_matakuliah tm
     LEFT JOIN public.tb_mst_jenis_matkul tmjm ON (((tmjm.id)::text = (tm.id_jenis_mata_kuliah)::text)))
     LEFT JOIN public.tb_mst_prodi tmp ON ((tmp.id = tm.id_prodi)))
     LEFT JOIN public.tb_mst_jenjang_pend tmjp ON ((tmjp.id = tmp.id_jenjang_pendidikan)));


ALTER TABLE public.list_matkul_prodi OWNER TO postgres;

--
-- Name: list_pengajuan_krs; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_pengajuan_krs AS
 SELECT tk.id AS id_krs,
    trp.nim,
    tm.nama_mahasiswa,
    tk.sks_diberikan,
    tk.sks_diambil,
    tk.krs_disetujui,
    trp.id_prodi,
    concat(tmjp.nama, ' ', tmp.nama) AS nama_prodi,
    tk.tahun_ajaran,
    tk.id_semester,
    concat(tk.tahun_ajaran, tk.id_semester) AS periode,
    COALESCE(tkd.jumlah, (0)::bigint) AS jumlah_matkul_diambil,
    tk.semester_angka,
    tmj.nama AS jalurmasuk,
    trp.id_kurikulum,
    trp.id AS id_riwayat_pendidikan
   FROM ((((((public.tb_krs tk
     LEFT JOIN public.tb_riwayat_pendidikan trp ON ((trp.id = tk.id_riwayat_pendidikan)))
     LEFT JOIN public.tb_mahasiswa tm ON ((tm.id = trp.id_mahasiswa)))
     LEFT JOIN public.tb_mst_prodi tmp ON ((tmp.id = trp.id_prodi)))
     LEFT JOIN public.tb_mst_jenjang_pend tmjp ON ((tmjp.id = tmp.id_jenjang_pendidikan)))
     LEFT JOIN ( SELECT tkd_1.id_krs,
            count(*) AS jumlah
           FROM public.tb_krs_detail tkd_1
          GROUP BY tkd_1.id_krs) tkd ON ((tkd.id_krs = tk.id)))
     LEFT JOIN public.tb_mst_jalurmasuk tmj ON ((tmj.id = trp.id_jalur_daftar)));


ALTER TABLE public.list_pengajuan_krs OWNER TO postgres;

--
-- Name: tb_pengajuan_surat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_pengajuan_surat (
    id integer NOT NULL,
    id_riwayat_pendidikan integer,
    nomor_surat character varying,
    tanggal_surat date,
    jabatan_ttd character varying,
    nama_pejabat_ttd character varying,
    nip_pejabat_ttd character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    status character varying,
    keterangan character varying,
    jenis_surat character(1),
    tahun_ajaran numeric(4,0),
    id_semester numeric(1,0),
    alasan text,
    id_jenis_aktivitas_mhs integer,
    tujuan_surat text,
    judul_aktivitas text,
    id_jenis_anggota character varying(1)
);


ALTER TABLE public.tb_pengajuan_surat OWNER TO postgres;

--
-- Name: COLUMN tb_pengajuan_surat.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pengajuan_surat.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_pengajuan_surat.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pengajuan_surat.status IS 'status pengajuan surat



1 => proses pengajuan oleh mahasiswa;

2 => sudah disetujui oleh pihak fakultas;

3 => ditolak oleh pihak fakultas dengan keterangan;';


--
-- Name: COLUMN tb_pengajuan_surat.keterangan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pengajuan_surat.keterangan IS 'diisi jika status 3';


--
-- Name: COLUMN tb_pengajuan_surat.jenis_surat; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pengajuan_surat.jenis_surat IS '1 => surat aktif kuliah;

2 => surat cuti kuliah;

3 => surat penelitian;';


--
-- Name: COLUMN tb_pengajuan_surat.alasan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pengajuan_surat.alasan IS 'alasan yang diisikan oleh mahasiswa';


--
-- Name: COLUMN tb_pengajuan_surat.id_jenis_aktivitas_mhs; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pengajuan_surat.id_jenis_aktivitas_mhs IS 'tb_mst_jenis_aktivitas_mahasiswa';


--
-- Name: COLUMN tb_pengajuan_surat.id_jenis_anggota; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pengajuan_surat.id_jenis_anggota IS '0 => personal

1 => kelompok';


--
-- Name: list_pengajuan_surat; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_pengajuan_surat AS
 SELECT tps.id AS id_pengajuan,
    trp.nim,
    tm.nama_mahasiswa,
    trp.ta_masuk AS angkatan,
    tps.nomor_surat,
    tps.tanggal_surat,
    tps.jabatan_ttd,
    tps.nama_pejabat_ttd,
    tps.nip_pejabat_ttd,
    tps.status AS status_pengajuan,
    tps.keterangan,
    tps.jenis_surat,
    tps.tahun_ajaran,
    tps.id_semester,
    concat(tps.tahun_ajaran, tps.id_semester) AS periode,
    trp.id_prodi,
    tmp.id_fakultas,
    concat(tmjp.nama, ' ', tmp.nama) AS nama_prodi,
    tps.alasan,
    tps.id_riwayat_pendidikan
   FROM ((((public.tb_pengajuan_surat tps
     JOIN public.tb_riwayat_pendidikan trp ON ((trp.id = tps.id_riwayat_pendidikan)))
     JOIN public.tb_mst_prodi tmp ON ((tmp.id = trp.id_prodi)))
     JOIN public.tb_mahasiswa tm ON ((tm.id = trp.id_mahasiswa)))
     JOIN public.tb_mst_jenjang_pend tmjp ON ((tmjp.id = tmp.id_jenjang_pendidikan)));


ALTER TABLE public.list_pengajuan_surat OWNER TO postgres;

--
-- Name: list_pengisian_nilai; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_pengisian_nilai AS
 SELECT tr.id,
    tr.id_unit_kelas,
    tuk.kode_unit,
    public.nama_lengkap(td.gelar_depan, td.nama, td.gelar_belakang) AS nama_dosen,
    td.nidn,
    tm.kode AS kode_matkul,
    tm.nama AS nama_matkul,
    tm.sks_mata_kuliah,
    tuk.terisi,
    tuk.tahun_ajaran,
    tuk.id_semester,
    tuk.id_prodi,
    tukm.id_matakuliah,
    COALESCE(tuk.status_pengisian, ''::character varying) AS status_pengisian,
    public.status_pengisian_nilai(tuk.status_pengisian) AS nama_status_pengisian,
    tuk.is_boleh_isi_nilai,
    concat(tuk.tahun_ajaran, tuk.id_semester) AS periode,
    tr.id_dosen
   FROM ((((public.tb_roster tr
     LEFT JOIN public.tb_unit_kelas tuk ON ((tuk.id = tr.id_unit_kelas)))
     LEFT JOIN public.tb_dosen td ON ((td.id = tr.id_dosen)))
     LEFT JOIN public.tb_unit_kelas_matakuliah tukm ON (((tukm.id_unit_kelas = tr.id_unit_kelas) AND (tukm.is_join = false))))
     LEFT JOIN public.tb_matakuliah tm ON ((tm.id = tukm.id_matakuliah)));


ALTER TABLE public.list_pengisian_nilai OWNER TO postgres;

--
-- Name: list_peserta_kelas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_peserta_kelas AS
 SELECT tr.id AS id_roster,
    tuk.tahun_ajaran,
    tuk.id_semester,
    trp.nim,
    tm.nama_mahasiswa,
    ta.semester_angka,
    trp.ta_masuk AS angkatan
   FROM (((((public.tb_unit_kelas_mahasiswa tukm
     JOIN public.tb_roster tr ON ((tr.id_unit_kelas = tukm.id_unit_kelas)))
     JOIN public.tb_unit_kelas tuk ON ((tuk.id = tr.id_unit_kelas)))
     JOIN public.tb_riwayat_pendidikan trp ON ((trp.id = tukm.id_riwayat_pendidikan)))
     JOIN public.tb_mahasiswa tm ON ((tm.id = trp.id_mahasiswa)))
     LEFT JOIN public.tb_akm ta ON (((ta.id_riwayat_pendidikan = trp.id) AND (ta.tahun_ajaran = tuk.tahun_ajaran) AND (ta.id_semester = tuk.id_semester))));


ALTER TABLE public.list_peserta_kelas OWNER TO postgres;

--
-- Name: tb_mst_mode_kuliah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_mode_kuliah (
    id character varying(1),
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_mode_kuliah OWNER TO postgres;

--
-- Name: tb_mst_ruangan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_ruangan (
    id integer NOT NULL,
    kode character varying,
    nama character varying,
    keterangan character varying,
    id_fakultas integer,
    kapasitas numeric(5,0),
    user_modified character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.tb_mst_ruangan OWNER TO postgres;

--
-- Name: list_roster; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_roster AS
 SELECT tr.id,
    tuk.tahun_ajaran,
    tuk.id_semester,
    tuk.kode_unit,
    tuk.id_prodi,
    concat(tmjp.nama, ' ', tmp.nama) AS nama_prodi,
    tukm.id_matakuliah,
    tm.kode AS kode_matakuliah,
    tm.nama AS nama_matakuliah,
    tm.sks_mata_kuliah,
    tr.id_dosen,
    public.nama_lengkap(td.gelar_depan, td.nama, td.gelar_belakang) AS nama_dosen_pengasuh,
    public.jam_belajar(tmjb.jam_belajar, tmjb2.jam_belajar) AS jam_ngajar,
    tr.tanggal_mulai_masuk,
    COALESCE(tra.jumlah_asisten, (0)::bigint) AS jumlah_asisten,
    tuk.terisi AS jumlah_mahasiswa,
    tr.id_unit_kelas,
    tmr.id AS id_ruangan,
    tmr.kode AS kode_ruangan,
    tmr.nama AS nama_ruangan,
    tmr.keterangan AS keterangan_ruangan,
    date_part('dow'::text, tr.tanggal_mulai_masuk) AS nama_hari,
    tk.nama AS nama_kurikulum,
    tmmk.nama AS mode_kuliah,
    concat(tuk.tahun_ajaran, tuk.id_semester) AS periode,
    tr.id_aktivitas_mengajar
   FROM ((((((((((((public.tb_roster tr
     JOIN public.tb_unit_kelas tuk ON ((tuk.id = tr.id_unit_kelas)))
     JOIN public.tb_mst_prodi tmp ON ((tmp.id = tuk.id_prodi)))
     JOIN public.tb_mst_jenjang_pend tmjp ON ((tmjp.id = tmp.id_jenjang_pendidikan)))
     JOIN public.tb_unit_kelas_matakuliah tukm ON (((tukm.id_unit_kelas = tr.id_unit_kelas) AND (tukm.is_join = false))))
     JOIN public.tb_matakuliah tm ON ((tm.id = tukm.id_matakuliah)))
     JOIN public.tb_dosen td ON ((td.id = tr.id_dosen)))
     JOIN public.tb_mst_jam_belajar tmjb ON ((tmjb.id = tr.id_jam_masuk)))
     JOIN public.tb_mst_jam_belajar tmjb2 ON ((tmjb2.id = tr.id_jam_keluar)))
     JOIN public.tb_mst_ruangan tmr ON ((tmr.id = tr.id_ruangan)))
     LEFT JOIN ( SELECT tra_1.id_roster,
            count(*) AS jumlah_asisten
           FROM public.tb_roster_asisten tra_1
          GROUP BY tra_1.id_roster) tra ON ((tra.id_roster = tr.id)))
     JOIN public.tb_kurikulum tk ON ((tk.id = tuk.id_kurikulum)))
     LEFT JOIN public.tb_mst_mode_kuliah tmmk ON (((tmmk.id)::text = (tr.mode)::text)));


ALTER TABLE public.list_roster OWNER TO postgres;

--
-- Name: list_unit_kelas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.list_unit_kelas AS
 SELECT tuk.id AS id_unit_kelas,
    tuk.tahun_ajaran,
    tuk.id_semester,
    tuk.id_prodi,
    tuk.kode_unit,
    tuk.kapasitas,
    tuk.terisi,
    tm.kode,
    tm.nama AS nama_matakuliah,
    tm.sks_mata_kuliah,
    tm.id_jenis_mata_kuliah,
    tukm.id_kurikulum,
    tmk.semester,
    tm.id AS id_matkul,
    tuk.syncron_feeder,
    tuk.error_feeder AS keterangan_error,
    concat(tuk.tahun_ajaran, tuk.id_semester) AS periode,
    public.nama_lengkap(td.gelar_depan, td.nama, td.gelar_belakang) AS nama_dosen,
    tr.apa_untuk_pditt AS kampus_merdeka,
    tk.nama AS nama_kurikulum,
    tuk.nama_kelas
   FROM ((((((public.tb_unit_kelas tuk
     JOIN public.tb_unit_kelas_matakuliah tukm ON (((tukm.id_unit_kelas = tuk.id) AND (tukm.is_join = false))))
     JOIN public.tb_matakuliah tm ON ((tm.id = tukm.id_matakuliah)))
     JOIN public.tb_matkul_kurikulum tmk ON (((tmk.id_kurikulum = tukm.id_kurikulum) AND (tmk.id_matkul = tukm.id_matakuliah))))
     LEFT JOIN public.tb_roster tr ON ((tr.id_unit_kelas = tuk.id)))
     LEFT JOIN public.tb_dosen td ON ((td.id = tr.id_dosen)))
     JOIN public.tb_kurikulum tk ON ((tk.id = tuk.id_kurikulum)));


ALTER TABLE public.list_unit_kelas OWNER TO postgres;

--
-- Name: login_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.login_session (
    id character varying(128) NOT NULL,
    ip_address inet NOT NULL,
    "timestamp" timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    data bytea DEFAULT '\x'::bytea NOT NULL
);


ALTER TABLE public.login_session OWNER TO postgres;

--
-- Name: tb_absensi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_absensi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_absensi_id_seq OWNER TO postgres;

--
-- Name: tb_absensi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_absensi_id_seq OWNED BY public.tb_absensi.id;


--
-- Name: tb_absensi_mahasiswa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_absensi_mahasiswa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_absensi_mahasiswa_id_seq OWNER TO postgres;

--
-- Name: tb_absensi_mahasiswa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_absensi_mahasiswa_id_seq OWNED BY public.tb_absensi_mahasiswa.id;


--
-- Name: tb_acara_mendatang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_acara_mendatang (
    id integer NOT NULL,
    judul character varying,
    slug character varying,
    deskripsi text,
    tanggal_mulai date,
    tanggal_selesai date,
    kategori character varying,
    lokasi character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_acara_mendatang OWNER TO postgres;

--
-- Name: tb_acara_mendatang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_acara_mendatang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_acara_mendatang_id_seq OWNER TO postgres;

--
-- Name: tb_acara_mendatang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_acara_mendatang_id_seq OWNED BY public.tb_acara_mendatang.id;


--
-- Name: tb_akm_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_akm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_akm_id_seq OWNER TO postgres;

--
-- Name: tb_akm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_akm_id_seq OWNED BY public.tb_akm.id;


--
-- Name: tb_aktivitas_mahasiswa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_aktivitas_mahasiswa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_aktivitas_mahasiswa_id_seq OWNER TO postgres;

--
-- Name: tb_aktivitas_mahasiswa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_aktivitas_mahasiswa_id_seq OWNED BY public.tb_aktivitas_mahasiswa.id;


--
-- Name: tb_anggota_aktivitas_mahasiswa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_anggota_aktivitas_mahasiswa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_anggota_aktivitas_mahasiswa_id_seq OWNER TO postgres;

--
-- Name: tb_anggota_aktivitas_mahasiswa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_anggota_aktivitas_mahasiswa_id_seq OWNED BY public.tb_anggota_aktivitas_mahasiswa.id;


--
-- Name: tb_app_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_app_config (
    judul_kop_surat text,
    alamat_kop_surat character varying
);


ALTER TABLE public.tb_app_config OWNER TO postgres;

--
-- Name: tb_berita; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_berita (
    id integer NOT NULL,
    judul character varying,
    content text,
    slug character varying,
    thumbnail character varying DEFAULT 'avatar-placeholder.png'::character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_berita OWNER TO postgres;

--
-- Name: tb_berita_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_berita_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_berita_id_seq OWNER TO postgres;

--
-- Name: tb_berita_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_berita_id_seq OWNED BY public.tb_berita.id;


--
-- Name: tb_calon_mahasiswa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_calon_mahasiswa (
    id bigint NOT NULL,
    no_pendaftaran character varying(100),
    nama_mahasiswa character varying(100),
    jenis_kelamin character varying(1),
    tempat_lahir character varying(32),
    tanggal_lahir date,
    id_agama integer,
    nik character varying(16),
    nisn character varying(10),
    npwp character varying(15),
    id_negara character varying(2),
    jalan character varying(80),
    dusun character varying(60),
    rt numeric(2,0) DEFAULT 0,
    rw numeric(2,0) DEFAULT 0,
    kelurahan character varying(60),
    kode_pos numeric(5,0),
    id_wilayah character varying(8),
    id_jenis_tinggal integer,
    id_alat_transportasi integer,
    telepon character varying(20),
    handphone character varying(20),
    email character varying(80),
    penerima_kps boolean,
    nomor_kps character varying(80),
    nik_ayah character varying(20),
    nama_ayah character varying(100),
    tanggal_lahir_ayah date,
    id_pendidikan_ayah integer DEFAULT 0,
    id_pekerjaan_ayah integer,
    id_penghasilan_ayah integer DEFAULT 0,
    nik_ibu character varying(20),
    nama_ibu_kandung character varying(100),
    tanggal_lahir_ibu date,
    id_pendidikan_ibu integer DEFAULT 0,
    id_pekerjaan_ibu integer,
    id_penghasilan_ibu integer DEFAULT 0,
    nama_wali character varying(100),
    tanggal_lahir_wali date,
    id_pendidikan_wali integer DEFAULT 0,
    id_pekerjaan_wali integer,
    id_penghasilan_wali integer DEFAULT 0,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    status_lulus character varying(1),
    keterangan character varying(255),
    id_jalurmasuk integer,
    periode_masuk numeric(5,0),
    id_prodi_lulus integer,
    complete_isi_biodata boolean DEFAULT false,
    status_approve boolean DEFAULT false,
    id_spp integer,
    id_ukt numeric(2,0),
    user_approve character varying,
    nilai_spp numeric(20,0),
    nim character varying,
    id_jenis_pendaftaran integer,
    id_pembiayaan integer,
    id_pt_asal uuid,
    id_prodi_asal uuid,
    sks_diakui numeric,
    syncron_siakad boolean DEFAULT false,
    keterangan_error character varying,
    biaya_masuk numeric
);


ALTER TABLE public.tb_calon_mahasiswa OWNER TO postgres;

--
-- Name: COLUMN tb_calon_mahasiswa.no_pendaftaran; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_calon_mahasiswa.no_pendaftaran IS 'atau nomor ujian atau nomor registrasi';


--
-- Name: COLUMN tb_calon_mahasiswa.status_lulus; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_calon_mahasiswa.status_lulus IS 'L => lulus;

T => tidak lulus;';


--
-- Name: COLUMN tb_calon_mahasiswa.id_jalurmasuk; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_calon_mahasiswa.id_jalurmasuk IS 'tb_mst_jalurmasuk_pmb;tb_mst_jalurmasuk';


--
-- Name: COLUMN tb_calon_mahasiswa.status_approve; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_calon_mahasiswa.status_approve IS 'apakah biodata ini sudah di approve oleh pihak panitia penerimaan calon mahasiswa baru';


--
-- Name: COLUMN tb_calon_mahasiswa.id_spp; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_calon_mahasiswa.id_spp IS 'tb_mst_spp_prodi';


--
-- Name: COLUMN tb_calon_mahasiswa.id_jenis_pendaftaran; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_calon_mahasiswa.id_jenis_pendaftaran IS 'tb_mst_jenis_pendaftaran';


--
-- Name: COLUMN tb_calon_mahasiswa.id_pembiayaan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_calon_mahasiswa.id_pembiayaan IS 'tb_mst_pembiayaan';


--
-- Name: COLUMN tb_calon_mahasiswa.id_pt_asal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_calon_mahasiswa.id_pt_asal IS 'tb_mst_all_pt';


--
-- Name: COLUMN tb_calon_mahasiswa.id_prodi_asal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_calon_mahasiswa.id_prodi_asal IS 'tb_mst_all_prodi';


--
-- Name: COLUMN tb_calon_mahasiswa.syncron_siakad; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_calon_mahasiswa.syncron_siakad IS 'apakah data sudah di import ke siakad';


--
-- Name: tb_calon_mahasiswa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_calon_mahasiswa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_calon_mahasiswa_id_seq OWNER TO postgres;

--
-- Name: tb_calon_mahasiswa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_calon_mahasiswa_id_seq OWNED BY public.tb_calon_mahasiswa.id;


--
-- Name: tb_dosen_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_dosen_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_dosen_id_seq OWNER TO postgres;

--
-- Name: tb_dosen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_dosen_id_seq OWNED BY public.tb_dosen.id;


--
-- Name: tb_informasi_dan_jadwal_pmb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_informasi_dan_jadwal_pmb (
    slug character varying NOT NULL,
    content text,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    tgl_mulai_daftar_ulang date,
    tgl_akhir_daftar_ulang date,
    tgl_mulai_pembayaran_ptn date,
    tgl_akhir_pembayaran_ptn date,
    tgl_mulai_pembayaran_spp date,
    tgl_akhir_pembayaran_spp date
);


ALTER TABLE public.tb_informasi_dan_jadwal_pmb OWNER TO postgres;

--
-- Name: COLUMN tb_informasi_dan_jadwal_pmb.tgl_mulai_daftar_ulang; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_informasi_dan_jadwal_pmb.tgl_mulai_daftar_ulang IS 'jika is_boleh_daftar true maka field ini dianggap tanggal mulai dibuka pendaftaran secara mandiri';


--
-- Name: COLUMN tb_informasi_dan_jadwal_pmb.tgl_akhir_daftar_ulang; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_informasi_dan_jadwal_pmb.tgl_akhir_daftar_ulang IS 'jika is_boleh_daftar true maka field ini dianggap tanggal akhir ditutup pendaftaran secara mandiri';


--
-- Name: tb_jabfung_dosen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_jabfung_dosen (
    id integer NOT NULL,
    id_dosen integer,
    id_jabfung integer,
    sk_jabatan_fungsional character varying,
    mulai_sk_jabatan date,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_jabfung_dosen OWNER TO postgres;

--
-- Name: COLUMN tb_jabfung_dosen.id_dosen; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jabfung_dosen.id_dosen IS 'tb_dosen';


--
-- Name: COLUMN tb_jabfung_dosen.id_jabfung; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jabfung_dosen.id_jabfung IS 'tb_mst_jabatan_fungsional';


--
-- Name: tb_jabfung_dosen_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_jabfung_dosen_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_jabfung_dosen_id_seq OWNER TO postgres;

--
-- Name: tb_jabfung_dosen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_jabfung_dosen_id_seq OWNED BY public.tb_jabfung_dosen.id;


--
-- Name: tb_jadwal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_jadwal (
    id integer NOT NULL,
    tahun_ajaran numeric,
    id_semester numeric,
    id_jenis_jadwal integer,
    tgl_mulai date,
    tgl_sampai date,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_jadwal OWNER TO postgres;

--
-- Name: COLUMN tb_jadwal.id_jenis_jadwal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jadwal.id_jenis_jadwal IS 'tb_mst_jenis_jadwal';


--
-- Name: tb_jadwal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_jadwal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_jadwal_id_seq OWNER TO postgres;

--
-- Name: tb_jadwal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_jadwal_id_seq OWNED BY public.tb_jadwal.id;


--
-- Name: tb_jadwal_pembayaran; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_jadwal_pembayaran (
    id integer NOT NULL,
    id_komponen_pembayaran integer,
    berlaku character(1),
    tanggal_mulai date,
    tanggal_selesai date,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    tahun_ajaran numeric,
    id_semester numeric
);


ALTER TABLE public.tb_jadwal_pembayaran OWNER TO postgres;

--
-- Name: COLUMN tb_jadwal_pembayaran.id_komponen_pembayaran; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jadwal_pembayaran.id_komponen_pembayaran IS 'tb_mst_komponen_pembayaran';


--
-- Name: COLUMN tb_jadwal_pembayaran.berlaku; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jadwal_pembayaran.berlaku IS '1 = berlaku berdasarkan tanggal mulai dan selesai

2 = berlaku sampai periode tutup';


--
-- Name: tb_jadwal_pembayaran_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_jadwal_pembayaran_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_jadwal_pembayaran_id_seq OWNER TO postgres;

--
-- Name: tb_jadwal_pembayaran_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_jadwal_pembayaran_id_seq OWNED BY public.tb_jadwal_pembayaran.id;


--
-- Name: tb_jadwal_ujian_peserta_pmb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_jadwal_ujian_peserta_pmb (
    id integer NOT NULL,
    id_jadwal integer,
    id_peserta integer
);


ALTER TABLE public.tb_jadwal_ujian_peserta_pmb OWNER TO postgres;

--
-- Name: COLUMN tb_jadwal_ujian_peserta_pmb.id_jadwal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jadwal_ujian_peserta_pmb.id_jadwal IS 'tb_jadwal_ujian_pmb';


--
-- Name: COLUMN tb_jadwal_ujian_peserta_pmb.id_peserta; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jadwal_ujian_peserta_pmb.id_peserta IS 'tb_peserta_pmb_lokal';


--
-- Name: tb_jadwal_ujian_peserta_pmb_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_jadwal_ujian_peserta_pmb_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_jadwal_ujian_peserta_pmb_id_seq OWNER TO postgres;

--
-- Name: tb_jadwal_ujian_peserta_pmb_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_jadwal_ujian_peserta_pmb_id_seq OWNED BY public.tb_jadwal_ujian_peserta_pmb.id;


--
-- Name: tb_jadwal_ujian_pmb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_jadwal_ujian_pmb (
    id integer NOT NULL,
    id_ruangan integer,
    sesi numeric,
    tanggal date,
    jam_mulai time without time zone,
    jam_selesai time without time zone,
    kelompok_studi character varying,
    tahun_ajaran numeric,
    id_semester numeric,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    nip_pengawas numeric(20,0),
    nama_pengawas character varying
);


ALTER TABLE public.tb_jadwal_ujian_pmb OWNER TO postgres;

--
-- Name: COLUMN tb_jadwal_ujian_pmb.id_ruangan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jadwal_ujian_pmb.id_ruangan IS 'tb_mst_ruangan_pmb';


--
-- Name: COLUMN tb_jadwal_ujian_pmb.kelompok_studi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jadwal_ujian_pmb.kelompok_studi IS 'IPA;IPS;IPC;';


--
-- Name: tb_jadwal_ujian_pmb_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_jadwal_ujian_pmb_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_jadwal_ujian_pmb_id_seq OWNER TO postgres;

--
-- Name: tb_jadwal_ujian_pmb_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_jadwal_ujian_pmb_id_seq OWNED BY public.tb_jadwal_ujian_pmb.id;


--
-- Name: tb_jawaban_survey; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_jawaban_survey (
    id bigint NOT NULL,
    id_pertanyaan integer,
    id_riwayat_pendidikan integer,
    tahun_ajaran numeric,
    id_semester numeric,
    rating numeric(1,0),
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    id_krs integer,
    id_matakuliah integer,
    id_unit_kelas integer
);


ALTER TABLE public.tb_jawaban_survey OWNER TO postgres;

--
-- Name: COLUMN tb_jawaban_survey.id_pertanyaan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jawaban_survey.id_pertanyaan IS 'tb_pertanyaan_survey';


--
-- Name: COLUMN tb_jawaban_survey.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jawaban_survey.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_jawaban_survey.id_krs; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jawaban_survey.id_krs IS 'tb_krs';


--
-- Name: COLUMN tb_jawaban_survey.id_matakuliah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jawaban_survey.id_matakuliah IS 'tb_krs_detail';


--
-- Name: COLUMN tb_jawaban_survey.id_unit_kelas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jawaban_survey.id_unit_kelas IS 'tb_unit_kelas';


--
-- Name: tb_jawaban_survey_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_jawaban_survey_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_jawaban_survey_id_seq OWNER TO postgres;

--
-- Name: tb_jawaban_survey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_jawaban_survey_id_seq OWNED BY public.tb_jawaban_survey.id;


--
-- Name: tb_mst_jenis_aktivitas_mahasiswa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jenis_aktivitas_mahasiswa (
    id integer NOT NULL,
    nama character varying,
    untuk_kampus_merdeka boolean DEFAULT false,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_jenis_aktivitas_mahasiswa OWNER TO postgres;

--
-- Name: tb_jenis_aktivitas_mahasiswa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_jenis_aktivitas_mahasiswa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_jenis_aktivitas_mahasiswa_id_seq OWNER TO postgres;

--
-- Name: tb_jenis_aktivitas_mahasiswa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_jenis_aktivitas_mahasiswa_id_seq OWNED BY public.tb_mst_jenis_aktivitas_mahasiswa.id;


--
-- Name: tb_join_matkul_kurikulum; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_join_matkul_kurikulum (
    id bigint NOT NULL,
    id_kurikulum integer,
    angkatan numeric,
    id_matkul integer,
    modified timestamp without time zone,
    user_modified character varying,
    tahun_ajaran numeric,
    id_semester numeric,
    id_prodi integer
);


ALTER TABLE public.tb_join_matkul_kurikulum OWNER TO postgres;

--
-- Name: COLUMN tb_join_matkul_kurikulum.id_kurikulum; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_join_matkul_kurikulum.id_kurikulum IS 'tb_matkul_kurikulum; tb_kurikulum;



sumber id kurikulum';


--
-- Name: COLUMN tb_join_matkul_kurikulum.angkatan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_join_matkul_kurikulum.angkatan IS 'angkatan berapa saja yang boleh ambil kurikulum ini';


--
-- Name: COLUMN tb_join_matkul_kurikulum.id_matkul; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_join_matkul_kurikulum.id_matkul IS 'tb_matakuliah';


--
-- Name: COLUMN tb_join_matkul_kurikulum.id_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_join_matkul_kurikulum.id_prodi IS 'tb_mst_prodi';


--
-- Name: tb_join_matkul_kurikulum_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_join_matkul_kurikulum_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_join_matkul_kurikulum_id_seq OWNER TO postgres;

--
-- Name: tb_join_matkul_kurikulum_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_join_matkul_kurikulum_id_seq OWNED BY public.tb_join_matkul_kurikulum.id;


--
-- Name: tb_jurnal_ajar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_jurnal_ajar (
    id integer NOT NULL,
    id_absensi integer,
    tanggal date,
    judul_bab character varying,
    rincian_materi text,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_jurnal_ajar OWNER TO postgres;

--
-- Name: COLUMN tb_jurnal_ajar.id_absensi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_jurnal_ajar.id_absensi IS 'tb_absensi';


--
-- Name: tb_jurnal_ajar_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_jurnal_ajar_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_jurnal_ajar_id_seq OWNER TO postgres;

--
-- Name: tb_jurnal_ajar_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_jurnal_ajar_id_seq OWNED BY public.tb_jurnal_ajar.id;


--
-- Name: tb_kampus_merdeka; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_kampus_merdeka (
    id integer NOT NULL,
    id_matkul integer,
    id_anggota integer,
    id_aktivitas integer,
    sks_mata_kuliah numeric,
    nilai_angka numeric,
    nilai_indeks numeric,
    nilai_huruf character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    id_konversi_aktivitas uuid
);


ALTER TABLE public.tb_kampus_merdeka OWNER TO postgres;

--
-- Name: COLUMN tb_kampus_merdeka.id_matkul; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kampus_merdeka.id_matkul IS 'tb_matakuliah';


--
-- Name: COLUMN tb_kampus_merdeka.id_anggota; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kampus_merdeka.id_anggota IS 'tb_anggota_aktivitas_mahasiswa';


--
-- Name: COLUMN tb_kampus_merdeka.id_aktivitas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kampus_merdeka.id_aktivitas IS 'tb_aktivitas_mahasiswa';


--
-- Name: tb_kampus_merdeka_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_kampus_merdeka_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_kampus_merdeka_id_seq OWNER TO postgres;

--
-- Name: tb_kampus_merdeka_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_kampus_merdeka_id_seq OWNED BY public.tb_kampus_merdeka.id;


--
-- Name: tb_kebutuhan_khusus_ayah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_kebutuhan_khusus_ayah (
    id integer NOT NULL,
    id_calon integer,
    id_riwayat_pendidikan integer,
    id_kebutuhan integer
);


ALTER TABLE public.tb_kebutuhan_khusus_ayah OWNER TO postgres;

--
-- Name: COLUMN tb_kebutuhan_khusus_ayah.id_calon; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kebutuhan_khusus_ayah.id_calon IS 'tb_calon_mahasiswa';


--
-- Name: COLUMN tb_kebutuhan_khusus_ayah.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kebutuhan_khusus_ayah.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_kebutuhan_khusus_ayah.id_kebutuhan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kebutuhan_khusus_ayah.id_kebutuhan IS 'tb_mst_kebutuhan_khusus';


--
-- Name: tb_kebutuhan_khusus_ayah_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_kebutuhan_khusus_ayah_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_kebutuhan_khusus_ayah_id_seq OWNER TO postgres;

--
-- Name: tb_kebutuhan_khusus_ayah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_kebutuhan_khusus_ayah_id_seq OWNED BY public.tb_kebutuhan_khusus_ayah.id;


--
-- Name: tb_kebutuhan_khusus_ibu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_kebutuhan_khusus_ibu (
    id integer NOT NULL,
    id_calon integer,
    id_riwayat_pendidikan integer,
    id_kebutuhan integer
);


ALTER TABLE public.tb_kebutuhan_khusus_ibu OWNER TO postgres;

--
-- Name: COLUMN tb_kebutuhan_khusus_ibu.id_calon; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kebutuhan_khusus_ibu.id_calon IS 'tb_calon_mahasiswa';


--
-- Name: COLUMN tb_kebutuhan_khusus_ibu.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kebutuhan_khusus_ibu.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_kebutuhan_khusus_ibu.id_kebutuhan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kebutuhan_khusus_ibu.id_kebutuhan IS 'tb_mst_kebutuhan_khusus';


--
-- Name: tb_kebutuhan_khusus_ibu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_kebutuhan_khusus_ibu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_kebutuhan_khusus_ibu_id_seq OWNER TO postgres;

--
-- Name: tb_kebutuhan_khusus_ibu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_kebutuhan_khusus_ibu_id_seq OWNED BY public.tb_kebutuhan_khusus_ibu.id;


--
-- Name: tb_kebutuhan_khusus_mhs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_kebutuhan_khusus_mhs (
    id integer NOT NULL,
    id_calon integer,
    id_riwayat_pendidikan integer,
    id_kebutuhan integer
);


ALTER TABLE public.tb_kebutuhan_khusus_mhs OWNER TO postgres;

--
-- Name: COLUMN tb_kebutuhan_khusus_mhs.id_calon; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kebutuhan_khusus_mhs.id_calon IS 'tb_calon_mahasiswa';


--
-- Name: COLUMN tb_kebutuhan_khusus_mhs.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kebutuhan_khusus_mhs.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_kebutuhan_khusus_mhs.id_kebutuhan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_kebutuhan_khusus_mhs.id_kebutuhan IS 'tb_mst_kebutuhan_khusus';


--
-- Name: tb_kebutuhan_khusus_mhs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_kebutuhan_khusus_mhs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_kebutuhan_khusus_mhs_id_seq OWNER TO postgres;

--
-- Name: tb_kebutuhan_khusus_mhs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_kebutuhan_khusus_mhs_id_seq OWNED BY public.tb_kebutuhan_khusus_mhs.id;


--
-- Name: tb_krs_backup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_krs_backup (
    id bigint NOT NULL,
    id_riwayat_pendidikan integer,
    tahun_ajaran numeric,
    id_semester numeric(1,0),
    sks_diberikan numeric DEFAULT 0,
    sks_diambil numeric DEFAULT 0,
    bobot numeric DEFAULT 0,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    semester_angka numeric,
    krs_disetujui boolean DEFAULT false,
    ip numeric DEFAULT 0,
    ipk numeric DEFAULT 0,
    is_active boolean DEFAULT false
);


ALTER TABLE public.tb_krs_backup OWNER TO postgres;

--
-- Name: TABLE tb_krs_backup; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.tb_krs_backup IS 'tabel ini mencatat kegiatan yang dilakukan pada aktivitas perkuliahan.

jika ada nilai pada tabel ini, maka krs nya di hapus sebelumnya';


--
-- Name: COLUMN tb_krs_backup.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_backup.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_krs_backup.bobot; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_backup.bobot IS 'sum nilai_bobot yang ada di krs detail';


--
-- Name: COLUMN tb_krs_backup.ipk; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_backup.ipk IS 'dihitung berdasarkan semua matakuliah yang telah diambil oleh mahasiswa';


--
-- Name: COLUMN tb_krs_backup.is_active; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_backup.is_active IS 'row krs yang digunakan untuk transkrip, field ini di update jadi true pada saat tutup periode tahun ajaran';


--
-- Name: tb_krs_backup_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_krs_backup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_krs_backup_id_seq OWNER TO postgres;

--
-- Name: tb_krs_backup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_krs_backup_id_seq OWNED BY public.tb_krs_backup.id;


--
-- Name: tb_krs_detail_backup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_krs_detail_backup (
    id bigint NOT NULL,
    id_krs integer,
    id_matakuliah integer,
    semester_matkul numeric,
    nilai_akhir numeric,
    nilai_huruf character varying(2) DEFAULT 'T'::character varying,
    nilai_bobot numeric,
    syncron_feeder boolean DEFAULT false,
    error_feeder text,
    uploaded timestamp without time zone,
    user_modified character varying,
    nilai_json json,
    modified timestamp without time zone,
    is_lulus boolean DEFAULT false,
    status_pengajuan boolean DEFAULT false,
    is_digunakan boolean DEFAULT true
);


ALTER TABLE public.tb_krs_detail_backup OWNER TO postgres;

--
-- Name: TABLE tb_krs_detail_backup; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.tb_krs_detail_backup IS 'tabel ini mencatat kegiatan yang dilakukan pada aktivitas perkuliahan.

jika ada nilai pada tabel ini, maka krs nya di hapus sebelumnya';


--
-- Name: COLUMN tb_krs_detail_backup.id_krs; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_detail_backup.id_krs IS 'tb_krs';


--
-- Name: COLUMN tb_krs_detail_backup.id_matakuliah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_detail_backup.id_matakuliah IS 'tb_matakuliah';


--
-- Name: COLUMN tb_krs_detail_backup.is_lulus; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_detail_backup.is_lulus IS 'apakah matakuliah ini lulus';


--
-- Name: COLUMN tb_krs_detail_backup.status_pengajuan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_detail_backup.status_pengajuan IS 'apakah status pengajuan matakuliah ini telah disetujui oleh prodi atau tidak';


--
-- Name: COLUMN tb_krs_detail_backup.is_digunakan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_krs_detail_backup.is_digunakan IS 'daftar matakuliah yang digunakan untuk transkrip';


--
-- Name: tb_krs_detail_backup_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_krs_detail_backup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_krs_detail_backup_id_seq OWNER TO postgres;

--
-- Name: tb_krs_detail_backup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_krs_detail_backup_id_seq OWNED BY public.tb_krs_detail_backup.id;


--
-- Name: tb_krs_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_krs_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_krs_detail_id_seq OWNER TO postgres;

--
-- Name: tb_krs_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_krs_detail_id_seq OWNED BY public.tb_krs_detail.id;


--
-- Name: tb_krs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_krs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_krs_id_seq OWNER TO postgres;

--
-- Name: tb_krs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_krs_id_seq OWNED BY public.tb_krs.id;


--
-- Name: tb_kurikulum_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_kurikulum_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_kurikulum_id_seq OWNER TO postgres;

--
-- Name: tb_kurikulum_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_kurikulum_id_seq OWNED BY public.tb_kurikulum.id;


--
-- Name: tb_lulus_dropout_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_lulus_dropout_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_lulus_dropout_id_seq OWNER TO postgres;

--
-- Name: tb_lulus_dropout_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_lulus_dropout_id_seq OWNED BY public.tb_lulus_dropout.id;


--
-- Name: tb_mahasiswa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mahasiswa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mahasiswa_id_seq OWNER TO postgres;

--
-- Name: tb_mahasiswa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mahasiswa_id_seq OWNED BY public.tb_mahasiswa.id;


--
-- Name: tb_matakuliah_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_matakuliah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_matakuliah_id_seq OWNER TO postgres;

--
-- Name: tb_matakuliah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_matakuliah_id_seq OWNED BY public.tb_matakuliah.id;


--
-- Name: tb_matkul_bersyarat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_matkul_bersyarat (
    id smallint NOT NULL,
    id_kurikulum integer,
    id_matkul_kurikulum integer,
    id_matkul integer,
    uploaded timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_matkul_bersyarat OWNER TO postgres;

--
-- Name: COLUMN tb_matkul_bersyarat.id_kurikulum; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_bersyarat.id_kurikulum IS 'tb_kurikulum';


--
-- Name: COLUMN tb_matkul_bersyarat.id_matkul_kurikulum; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_bersyarat.id_matkul_kurikulum IS 'tb_matkul_kurikulum;



ini merupakan master referensi matakuliah bersyarat;';


--
-- Name: COLUMN tb_matkul_bersyarat.id_matkul; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_bersyarat.id_matkul IS 'tb_matakuliah;



daftar matakuliah yang wajib lulus terlebih dahulu;';


--
-- Name: tb_matkul_bersyarat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_matkul_bersyarat_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_matkul_bersyarat_id_seq OWNER TO postgres;

--
-- Name: tb_matkul_bersyarat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_matkul_bersyarat_id_seq OWNED BY public.tb_matkul_bersyarat.id;


--
-- Name: tb_matkul_kurikulum_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_matkul_kurikulum_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_matkul_kurikulum_id_seq OWNER TO postgres;

--
-- Name: tb_matkul_kurikulum_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_matkul_kurikulum_id_seq OWNED BY public.tb_matkul_kurikulum.id;


--
-- Name: tb_matkul_prodi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_matkul_prodi (
    id integer NOT NULL,
    id_matkul integer,
    id_prodi integer,
    syncron_feeder boolean DEFAULT false,
    keterangan_error character varying
);


ALTER TABLE public.tb_matkul_prodi OWNER TO postgres;

--
-- Name: COLUMN tb_matkul_prodi.id_matkul; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_prodi.id_matkul IS 'tb_matakuliah';


--
-- Name: COLUMN tb_matkul_prodi.id_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_matkul_prodi.id_prodi IS 'tb_mst_prodi';


--
-- Name: tb_matkul_prodi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_matkul_prodi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_matkul_prodi_id_seq OWNER TO postgres;

--
-- Name: tb_matkul_prodi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_matkul_prodi_id_seq OWNED BY public.tb_matkul_prodi.id;


--
-- Name: tb_mst_alat_transportasi_id_alat_transportasi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_alat_transportasi_id_alat_transportasi_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_alat_transportasi_id_alat_transportasi_seq OWNER TO postgres;

--
-- Name: tb_mst_alat_transportasi_id_alat_transportasi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_alat_transportasi_id_alat_transportasi_seq OWNED BY public.tb_mst_alat_transportasi.id;


--
-- Name: tb_mst_bidang_minat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_bidang_minat (
    id integer NOT NULL,
    id_bidang_minat uuid,
    nama character varying,
    id_prodi integer,
    smt_dimulai character varying(5),
    sk_bidang_minat character varying,
    tamat_sk_bidang_minat date
);


ALTER TABLE public.tb_mst_bidang_minat OWNER TO postgres;

--
-- Name: COLUMN tb_mst_bidang_minat.id_bidang_minat; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_bidang_minat.id_bidang_minat IS 'ID feeder';


--
-- Name: COLUMN tb_mst_bidang_minat.smt_dimulai; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_bidang_minat.smt_dimulai IS 'Semester dimulai bidang minat';


--
-- Name: tb_mst_bidang_minat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_bidang_minat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_bidang_minat_id_seq OWNER TO postgres;

--
-- Name: tb_mst_bidang_minat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_bidang_minat_id_seq OWNED BY public.tb_mst_bidang_minat.id;


--
-- Name: tb_mst_config_krs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_config_krs (
    id integer NOT NULL,
    id_jenjang_pend integer,
    ip_mulai numeric(4,2),
    ip_sampai numeric(4,2),
    total_sks numeric
);


ALTER TABLE public.tb_mst_config_krs OWNER TO postgres;

--
-- Name: COLUMN tb_mst_config_krs.id_jenjang_pend; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_config_krs.id_jenjang_pend IS 'tb_mst_jenjang_pend';


--
-- Name: tb_mst_config_krs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_config_krs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_config_krs_id_seq OWNER TO postgres;

--
-- Name: tb_mst_config_krs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_config_krs_id_seq OWNED BY public.tb_mst_config_krs.id;


--
-- Name: tb_mst_fakultas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_fakultas (
    id integer NOT NULL,
    kode character varying(2),
    nama character varying(100),
    singkatan character varying(10),
    id_dekan integer,
    id_dekan1 integer,
    id_dekan2 integer,
    id_dekan3 integer,
    user_modified character varying,
    modified timestamp without time zone,
    uploaded timestamp without time zone
);


ALTER TABLE public.tb_mst_fakultas OWNER TO postgres;

--
-- Name: COLUMN tb_mst_fakultas.id_dekan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_fakultas.id_dekan IS 'tb_dosen';


--
-- Name: COLUMN tb_mst_fakultas.id_dekan1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_fakultas.id_dekan1 IS 'tb_cosen';


--
-- Name: COLUMN tb_mst_fakultas.id_dekan2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_fakultas.id_dekan2 IS 'tb_dosen';


--
-- Name: COLUMN tb_mst_fakultas.id_dekan3; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_fakultas.id_dekan3 IS 'tb_dosen';


--
-- Name: tb_mst_fakultas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_fakultas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_fakultas_id_seq OWNER TO postgres;

--
-- Name: tb_mst_fakultas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_fakultas_id_seq OWNED BY public.tb_mst_fakultas.id;


--
-- Name: tb_mst_feeder; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_feeder (
    username character varying,
    password character varying,
    link_feeder character varying
);


ALTER TABLE public.tb_mst_feeder OWNER TO postgres;

--
-- Name: tb_mst_golpang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_golpang (
    id integer NOT NULL,
    kode character varying,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_golpang OWNER TO postgres;

--
-- Name: tb_mst_golpang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_golpang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_golpang_id_seq OWNER TO postgres;

--
-- Name: tb_mst_golpang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_golpang_id_seq OWNED BY public.tb_mst_golpang.id;


--
-- Name: tb_mst_hari; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_hari (
    id character(1) NOT NULL,
    nama character varying
);


ALTER TABLE public.tb_mst_hari OWNER TO postgres;

--
-- Name: tb_mst_ikatan_kerja; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_ikatan_kerja (
    id character varying NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_ikatan_kerja OWNER TO postgres;

--
-- Name: tb_mst_jabatan_fungsional; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jabatan_fungsional (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_jabatan_fungsional OWNER TO postgres;

--
-- Name: tb_mst_jabatan_fungsional_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jabatan_fungsional_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jabatan_fungsional_id_seq OWNER TO postgres;

--
-- Name: tb_mst_jabatan_fungsional_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jabatan_fungsional_id_seq OWNED BY public.tb_mst_jabatan_fungsional.id;


--
-- Name: tb_mst_jalurmasuk_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jalurmasuk_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jalurmasuk_id_seq OWNER TO postgres;

--
-- Name: tb_mst_jalurmasuk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jalurmasuk_id_seq OWNED BY public.tb_mst_jalurmasuk.id;


--
-- Name: tb_mst_jalurmasuk_pmb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jalurmasuk_pmb (
    id integer NOT NULL,
    nama character varying,
    slug character varying,
    is_open boolean DEFAULT false,
    is_boleh_daftar boolean DEFAULT false,
    user_modified character varying,
    modified timestamp without time zone
);


ALTER TABLE public.tb_mst_jalurmasuk_pmb OWNER TO postgres;

--
-- Name: COLUMN tb_mst_jalurmasuk_pmb.is_open; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_jalurmasuk_pmb.is_open IS 'apakah di bukan untuk umum';


--
-- Name: COLUMN tb_mst_jalurmasuk_pmb.is_boleh_daftar; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_jalurmasuk_pmb.is_boleh_daftar IS 'jalur ini yang boleh melakukan pendaftaran secara mandiri';


--
-- Name: tb_mst_jalurmasuk_pmb_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jalurmasuk_pmb_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jalurmasuk_pmb_id_seq OWNER TO postgres;

--
-- Name: tb_mst_jalurmasuk_pmb_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jalurmasuk_pmb_id_seq OWNED BY public.tb_mst_jalurmasuk_pmb.id;


--
-- Name: tb_mst_jam_belajar_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jam_belajar_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jam_belajar_id_seq OWNER TO postgres;

--
-- Name: tb_mst_jam_belajar_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jam_belajar_id_seq OWNED BY public.tb_mst_jam_belajar.id;


--
-- Name: tb_mst_jenis_evaluasi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jenis_evaluasi (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_jenis_evaluasi OWNER TO postgres;

--
-- Name: tb_mst_jenis_evaluasi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jenis_evaluasi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jenis_evaluasi_id_seq OWNER TO postgres;

--
-- Name: tb_mst_jenis_evaluasi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jenis_evaluasi_id_seq OWNED BY public.tb_mst_jenis_evaluasi.id;


--
-- Name: tb_mst_jenis_jadwal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jenis_jadwal (
    id integer NOT NULL,
    nama character varying
);


ALTER TABLE public.tb_mst_jenis_jadwal OWNER TO postgres;

--
-- Name: tb_mst_jenis_jadwal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jenis_jadwal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jenis_jadwal_id_seq OWNER TO postgres;

--
-- Name: tb_mst_jenis_jadwal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jenis_jadwal_id_seq OWNED BY public.tb_mst_jenis_jadwal.id;


--
-- Name: tb_mst_jenis_keluar_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jenis_keluar_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jenis_keluar_id_seq OWNER TO postgres;

--
-- Name: tb_mst_jenis_keluar_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jenis_keluar_id_seq OWNED BY public.tb_mst_jenis_keluar.id;


--
-- Name: tb_mst_jenis_pegawai; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jenis_pegawai (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_jenis_pegawai OWNER TO postgres;

--
-- Name: tb_mst_jenis_pegawai_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jenis_pegawai_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jenis_pegawai_id_seq OWNER TO postgres;

--
-- Name: tb_mst_jenis_pegawai_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jenis_pegawai_id_seq OWNED BY public.tb_mst_jenis_pegawai.id;


--
-- Name: tb_mst_jenis_pendaftaran_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jenis_pendaftaran_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jenis_pendaftaran_id_seq OWNER TO postgres;

--
-- Name: tb_mst_jenis_pendaftaran_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jenis_pendaftaran_id_seq OWNED BY public.tb_mst_jenis_pendaftaran.id;


--
-- Name: tb_mst_jenis_prestasi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jenis_prestasi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jenis_prestasi_id_seq OWNER TO postgres;

--
-- Name: tb_mst_jenis_prestasi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jenis_prestasi_id_seq OWNED BY public.tb_mst_jenis_prestasi.id;


--
-- Name: tb_mst_jenis_substansi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jenis_substansi (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_jenis_substansi OWNER TO postgres;

--
-- Name: tb_mst_jenis_substansi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jenis_substansi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jenis_substansi_id_seq OWNER TO postgres;

--
-- Name: tb_mst_jenis_substansi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jenis_substansi_id_seq OWNED BY public.tb_mst_jenis_substansi.id;


--
-- Name: tb_mst_jenis_survey; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_jenis_survey (
    id integer NOT NULL,
    nama character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    keterangan character varying
);


ALTER TABLE public.tb_mst_jenis_survey OWNER TO postgres;

--
-- Name: tb_mst_jenis_survey_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jenis_survey_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jenis_survey_id_seq OWNER TO postgres;

--
-- Name: tb_mst_jenis_survey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jenis_survey_id_seq OWNED BY public.tb_mst_jenis_survey.id;


--
-- Name: tb_mst_jenis_tinggal_id_jenis_tinggal_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jenis_tinggal_id_jenis_tinggal_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jenis_tinggal_id_jenis_tinggal_seq OWNER TO postgres;

--
-- Name: tb_mst_jenis_tinggal_id_jenis_tinggal_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jenis_tinggal_id_jenis_tinggal_seq OWNED BY public.tb_mst_jenis_tinggal.id;


--
-- Name: tb_mst_jenjang_pend_id_jenjang_didik_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_jenjang_pend_id_jenjang_didik_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_jenjang_pend_id_jenjang_didik_seq OWNER TO postgres;

--
-- Name: tb_mst_jenjang_pend_id_jenjang_didik_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_jenjang_pend_id_jenjang_didik_seq OWNED BY public.tb_mst_jenjang_pend.id;


--
-- Name: tb_mst_kategori_kegiatan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_kategori_kegiatan (
    id character varying,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_kategori_kegiatan OWNER TO postgres;

--
-- Name: tb_mst_kebutuhan_khusus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_kebutuhan_khusus (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_kebutuhan_khusus OWNER TO postgres;

--
-- Name: tb_mst_kebutuhan_khusus_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_kebutuhan_khusus_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_kebutuhan_khusus_id_seq OWNER TO postgres;

--
-- Name: tb_mst_kebutuhan_khusus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_kebutuhan_khusus_id_seq OWNED BY public.tb_mst_kebutuhan_khusus.id;


--
-- Name: tb_mst_kelompok_matkul; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_kelompok_matkul (
    id character varying NOT NULL,
    nama character varying,
    keterangan character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_kelompok_matkul OWNER TO postgres;

--
-- Name: tb_mst_komponen_pembayaran; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_komponen_pembayaran (
    id integer NOT NULL,
    nama character varying,
    kode character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    biaya_default numeric,
    biaya_kelompok_ipa numeric,
    biaya_kelompok_ips numeric,
    biaya_kelompok_ipc numeric
);


ALTER TABLE public.tb_mst_komponen_pembayaran OWNER TO postgres;

--
-- Name: tb_mst_komponen_pembayaran_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_komponen_pembayaran_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_komponen_pembayaran_id_seq OWNER TO postgres;

--
-- Name: tb_mst_komponen_pembayaran_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_komponen_pembayaran_id_seq OWNED BY public.tb_mst_komponen_pembayaran.id;


--
-- Name: tb_mst_lembaga_pengangkat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_lembaga_pengangkat (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_lembaga_pengangkat OWNER TO postgres;

--
-- Name: tb_mst_lembaga_pengangkat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_lembaga_pengangkat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_lembaga_pengangkat_id_seq OWNER TO postgres;

--
-- Name: tb_mst_lembaga_pengangkat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_lembaga_pengangkat_id_seq OWNED BY public.tb_mst_lembaga_pengangkat.id;


--
-- Name: tb_mst_model_penilaian_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_model_penilaian_detail (
    id integer NOT NULL,
    id_model_penilaian integer,
    uraian_kegiatan character varying,
    kode_kegiatan character varying,
    bobot_persentase numeric,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_model_penilaian_detail OWNER TO postgres;

--
-- Name: COLUMN tb_mst_model_penilaian_detail.id_model_penilaian; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_model_penilaian_detail.id_model_penilaian IS 'tb_mst_model_penilaian';


--
-- Name: tb_mst_model_penilaian_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_model_penilaian_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_model_penilaian_detail_id_seq OWNER TO postgres;

--
-- Name: tb_mst_model_penilaian_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_model_penilaian_detail_id_seq OWNED BY public.tb_mst_model_penilaian_detail.id;


--
-- Name: tb_mst_model_penilaian_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_model_penilaian_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_model_penilaian_id_seq OWNER TO postgres;

--
-- Name: tb_mst_model_penilaian_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_model_penilaian_id_seq OWNED BY public.tb_mst_model_penilaian.id;


--
-- Name: tb_mst_nilai_huruf; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_nilai_huruf (
    nilai_huruf character varying,
    nilai_mulai numeric,
    nilai_sampai numeric,
    bobot numeric,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    is_minimal boolean DEFAULT false
);


ALTER TABLE public.tb_mst_nilai_huruf OWNER TO postgres;

--
-- Name: COLUMN tb_mst_nilai_huruf.is_minimal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_nilai_huruf.is_minimal IS 'apakah ini nilai minimal yang di anggap lulus matakuliah';


--
-- Name: tb_mst_pekerjaan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_pekerjaan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_pekerjaan_id_seq OWNER TO postgres;

--
-- Name: tb_mst_pekerjaan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_pekerjaan_id_seq OWNED BY public.tb_mst_pekerjaan.id;


--
-- Name: tb_mst_pembiayaan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_pembiayaan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_pembiayaan_id_seq OWNER TO postgres;

--
-- Name: tb_mst_pembiayaan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_pembiayaan_id_seq OWNED BY public.tb_mst_pembiayaan.id;


--
-- Name: tb_mst_penghasilan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_penghasilan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_penghasilan_id_seq OWNER TO postgres;

--
-- Name: tb_mst_penghasilan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_penghasilan_id_seq OWNED BY public.tb_mst_penghasilan.id;


--
-- Name: tb_mst_perguruan_tinggi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_perguruan_tinggi (
    id uuid NOT NULL,
    kode character varying,
    nama character varying,
    telepon character varying,
    faximile character varying,
    email character varying,
    website character varying,
    jalan character varying,
    dusun character varying,
    rt_rw character varying,
    kelurahan character varying,
    kode_pos character varying,
    id_wilayah character varying,
    lintang_bujur character varying,
    bank character varying,
    unit_cabang character varying,
    nomor_rekening character varying,
    mbs numeric(1,0),
    luas_tanah_milik numeric(7,0),
    luas_tanah_bukan_milik numeric(7,0),
    sk_pendirian character varying,
    tanggal_sk_pendirian date,
    id_status_milik numeric(1,0),
    status_perguruan_tinggi character varying(1),
    sk_izin_operasional character varying,
    tanggal_izin_operasional date
);


ALTER TABLE public.tb_mst_perguruan_tinggi OWNER TO postgres;

--
-- Name: COLUMN tb_mst_perguruan_tinggi.id_wilayah; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_perguruan_tinggi.id_wilayah IS 'ID Wilayah. Web Service: GetWilayah';


--
-- Name: tb_mst_periode_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_periode_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_periode_id_seq OWNER TO postgres;

--
-- Name: tb_mst_periode_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_periode_id_seq OWNED BY public.tb_mst_periode.id;


--
-- Name: tb_mst_periode_pmb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_periode_pmb (
    id integer NOT NULL,
    tahun_ajaran numeric(4,0),
    id_semester numeric(1,0),
    nama_semester character varying,
    periode_aktif boolean DEFAULT true,
    user_modified character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.tb_mst_periode_pmb OWNER TO postgres;

--
-- Name: tb_mst_periode_pmb_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_periode_pmb_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_periode_pmb_id_seq OWNER TO postgres;

--
-- Name: tb_mst_periode_pmb_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_periode_pmb_id_seq OWNED BY public.tb_mst_periode_pmb.id;


--
-- Name: tb_mst_prodi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_prodi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_prodi_id_seq OWNER TO postgres;

--
-- Name: tb_mst_prodi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_prodi_id_seq OWNED BY public.tb_mst_prodi.id;


--
-- Name: tb_mst_ruangan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_ruangan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_ruangan_id_seq OWNER TO postgres;

--
-- Name: tb_mst_ruangan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_ruangan_id_seq OWNED BY public.tb_mst_ruangan.id;


--
-- Name: tb_mst_ruangan_pmb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_ruangan_pmb (
    id integer NOT NULL,
    kode character varying,
    nama character varying,
    keterangan character varying,
    id_fakultas integer,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    kapasitas numeric
);


ALTER TABLE public.tb_mst_ruangan_pmb OWNER TO postgres;

--
-- Name: COLUMN tb_mst_ruangan_pmb.id_fakultas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_ruangan_pmb.id_fakultas IS 'tb_mst_fakultas';


--
-- Name: tb_mst_ruangan_pmb_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_ruangan_pmb_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_ruangan_pmb_id_seq OWNER TO postgres;

--
-- Name: tb_mst_ruangan_pmb_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_ruangan_pmb_id_seq OWNED BY public.tb_mst_ruangan_pmb.id;


--
-- Name: tb_mst_spp_prodi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_spp_prodi (
    id integer NOT NULL,
    id_prodi integer,
    ukt_1 numeric,
    ukt_2 numeric,
    ukt_3 numeric,
    ukt_4 numeric,
    ukt_5 numeric,
    ukt_6 numeric,
    ukt_7 numeric,
    ukt_8 numeric,
    ukt_9 numeric,
    ukt_10 numeric,
    angkatan numeric,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_spp_prodi OWNER TO postgres;

--
-- Name: COLUMN tb_mst_spp_prodi.id_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_mst_spp_prodi.id_prodi IS 'tb_mst_prodi';


--
-- Name: tb_mst_spp_prodi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_spp_prodi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_spp_prodi_id_seq OWNER TO postgres;

--
-- Name: tb_mst_spp_prodi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_spp_prodi_id_seq OWNED BY public.tb_mst_spp_prodi.id;


--
-- Name: tb_mst_status_keaktifan_pegawai_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_status_keaktifan_pegawai_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_status_keaktifan_pegawai_id_seq OWNER TO postgres;

--
-- Name: tb_mst_status_keaktifan_pegawai_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_status_keaktifan_pegawai_id_seq OWNED BY public.tb_mst_status_keaktifan_pegawai.id;


--
-- Name: tb_mst_status_kepegawaian; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_mst_status_kepegawaian (
    id integer NOT NULL,
    nama character varying,
    sumber character varying,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_mst_status_kepegawaian OWNER TO postgres;

--
-- Name: tb_mst_status_kepegawaian_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_status_kepegawaian_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_status_kepegawaian_id_seq OWNER TO postgres;

--
-- Name: tb_mst_status_kepegawaian_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_status_kepegawaian_id_seq OWNED BY public.tb_mst_status_kepegawaian.id;


--
-- Name: tb_mst_tingkat_prestasi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_mst_tingkat_prestasi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_mst_tingkat_prestasi_id_seq OWNER TO postgres;

--
-- Name: tb_mst_tingkat_prestasi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_mst_tingkat_prestasi_id_seq OWNED BY public.tb_mst_tingkat_prestasi.id;


--
-- Name: tb_nilai_transfer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_nilai_transfer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_nilai_transfer_id_seq OWNER TO postgres;

--
-- Name: tb_nilai_transfer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_nilai_transfer_id_seq OWNED BY public.tb_nilai_transfer.id;


--
-- Name: tb_pangkat_dosen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_pangkat_dosen (
    id integer NOT NULL,
    id_pangkat_golongan integer,
    sk_pangkat character varying,
    tanggal_sk_pangkat date,
    mulai_sk_pangkat date,
    masa_kerja_dalam_tahun numeric,
    masa_kerja_dalam_bulan numeric,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    id_dosen integer
);


ALTER TABLE public.tb_pangkat_dosen OWNER TO postgres;

--
-- Name: COLUMN tb_pangkat_dosen.id_pangkat_golongan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pangkat_dosen.id_pangkat_golongan IS 'tb_mst_golpang';


--
-- Name: COLUMN tb_pangkat_dosen.id_dosen; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pangkat_dosen.id_dosen IS 'tb_dosen';


--
-- Name: tb_pangkat_dosen_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_pangkat_dosen_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_pangkat_dosen_id_seq OWNER TO postgres;

--
-- Name: tb_pangkat_dosen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_pangkat_dosen_id_seq OWNED BY public.tb_pangkat_dosen.id;


--
-- Name: tb_pembayaran; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_pembayaran (
    id bigint NOT NULL,
    id_riwayat_pendidikan bigint,
    sudah_bayar boolean DEFAULT false,
    nilai_bayar numeric,
    tahun_ajaran numeric,
    id_semester numeric(1,0),
    id_komponen_pembayaran integer,
    id_jadwal_pembayaran integer,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    keterangan character varying,
    no_invoice character varying(24),
    jumlah_bayar numeric,
    tanggal_bayar timestamp without time zone,
    id_calon integer
);


ALTER TABLE public.tb_pembayaran OWNER TO postgres;

--
-- Name: COLUMN tb_pembayaran.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pembayaran.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_pembayaran.id_komponen_pembayaran; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pembayaran.id_komponen_pembayaran IS 'tb_mst_komponen_pembayaran';


--
-- Name: COLUMN tb_pembayaran.id_jadwal_pembayaran; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pembayaran.id_jadwal_pembayaran IS 'tb_jadwal_pembayaran;



nilai null maka ini khusus untuk pendaftaran calon mahasiswa baru';


--
-- Name: COLUMN tb_pembayaran.id_calon; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pembayaran.id_calon IS 'tb_calon_mahasiswa';


--
-- Name: tb_pembayaran_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_pembayaran_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_pembayaran_id_seq OWNER TO postgres;

--
-- Name: tb_pembayaran_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_pembayaran_id_seq OWNED BY public.tb_pembayaran.id;


--
-- Name: tb_pembimbing_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_pembimbing_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_pembimbing_id_seq OWNER TO postgres;

--
-- Name: tb_pembimbing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_pembimbing_id_seq OWNED BY public.tb_pembimbing.id;


--
-- Name: tb_pengajuan_surat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_pengajuan_surat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_pengajuan_surat_id_seq OWNER TO postgres;

--
-- Name: tb_pengajuan_surat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_pengajuan_surat_id_seq OWNED BY public.tb_pengajuan_surat.id;


--
-- Name: tb_penguji_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_penguji_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_penguji_id_seq OWNER TO postgres;

--
-- Name: tb_penguji_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_penguji_id_seq OWNED BY public.tb_penguji.id;


--
-- Name: tb_penugasan_dosen_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_penugasan_dosen_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_penugasan_dosen_id_seq OWNER TO postgres;

--
-- Name: tb_penugasan_dosen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_penugasan_dosen_id_seq OWNED BY public.tb_penugasan_dosen.id;


--
-- Name: tb_periode_feeder; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_periode_feeder (
    id integer NOT NULL,
    tahun_ajaran numeric,
    id_semester numeric,
    periode_aktif boolean DEFAULT true,
    uploaded timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_periode_feeder OWNER TO postgres;

--
-- Name: tb_periode_feeder_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_periode_feeder_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_periode_feeder_id_seq OWNER TO postgres;

--
-- Name: tb_periode_feeder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_periode_feeder_id_seq OWNED BY public.tb_periode_feeder.id;


--
-- Name: tb_pertanyaan_survey; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_pertanyaan_survey (
    id integer NOT NULL,
    id_jenis_survey integer,
    level character varying(1),
    id_fakultas integer,
    id_prodi integer,
    pertanyaan text,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_pertanyaan_survey OWNER TO postgres;

--
-- Name: COLUMN tb_pertanyaan_survey.id_jenis_survey; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pertanyaan_survey.id_jenis_survey IS 'tb_mst_jenis_survey';


--
-- Name: COLUMN tb_pertanyaan_survey.level; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pertanyaan_survey.level IS '1 => PT

2 => fakultas

3 => prodi';


--
-- Name: COLUMN tb_pertanyaan_survey.id_fakultas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pertanyaan_survey.id_fakultas IS 'tb_mst_fakultas';


--
-- Name: COLUMN tb_pertanyaan_survey.id_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_pertanyaan_survey.id_prodi IS 'tb_mst_prodi';


--
-- Name: tb_pertanyaan_survey_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_pertanyaan_survey_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_pertanyaan_survey_id_seq OWNER TO postgres;

--
-- Name: tb_pertanyaan_survey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_pertanyaan_survey_id_seq OWNED BY public.tb_pertanyaan_survey.id;


--
-- Name: tb_perwalian; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_perwalian (
    id integer NOT NULL,
    id_riwayat_pendidikan integer,
    id_dosen integer,
    uploaded timestamp without time zone,
    user_modified character varying,
    modified timestamp without time zone
);


ALTER TABLE public.tb_perwalian OWNER TO postgres;

--
-- Name: COLUMN tb_perwalian.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_perwalian.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_perwalian.id_dosen; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_perwalian.id_dosen IS 'tb_dosen';


--
-- Name: tb_perwalian_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_perwalian_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_perwalian_id_seq OWNER TO postgres;

--
-- Name: tb_perwalian_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_perwalian_id_seq OWNED BY public.tb_perwalian.id;


--
-- Name: tb_peserta_pmb_lokal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_peserta_pmb_lokal (
    id integer NOT NULL,
    id_calon integer,
    foto character varying,
    nomor_skhu character varying,
    nilai_skhu character varying,
    nomor_ijazah character varying,
    nilai_ijazah character varying,
    asal_sekolah numeric,
    jurusan_sekolah character varying,
    tahun_tamat numeric(4,0),
    kelompok_studi character varying,
    prodi_pilihan_1 integer,
    prodi_pilihan_2 integer,
    prodi_pilihan_3 integer,
    periode_masuk numeric,
    tanggal_daftar timestamp without time zone,
    user_modified character varying,
    sudah_pengumuman boolean DEFAULT false
);


ALTER TABLE public.tb_peserta_pmb_lokal OWNER TO postgres;

--
-- Name: COLUMN tb_peserta_pmb_lokal.id_calon; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_peserta_pmb_lokal.id_calon IS 'tb_calon_mahasiswa';


--
-- Name: COLUMN tb_peserta_pmb_lokal.kelompok_studi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_peserta_pmb_lokal.kelompok_studi IS 'IPA; IPS; IPC;';


--
-- Name: COLUMN tb_peserta_pmb_lokal.prodi_pilihan_1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_peserta_pmb_lokal.prodi_pilihan_1 IS 'tb_mst_prodi';


--
-- Name: COLUMN tb_peserta_pmb_lokal.prodi_pilihan_2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_peserta_pmb_lokal.prodi_pilihan_2 IS 'tb_mst_prodi';


--
-- Name: COLUMN tb_peserta_pmb_lokal.prodi_pilihan_3; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_peserta_pmb_lokal.prodi_pilihan_3 IS 'tb_mst_prodi';


--
-- Name: COLUMN tb_peserta_pmb_lokal.sudah_pengumuman; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_peserta_pmb_lokal.sudah_pengumuman IS 'apakah data status lulus sudah di upload';


--
-- Name: tb_peserta_pmb_lokal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_peserta_pmb_lokal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_peserta_pmb_lokal_id_seq OWNER TO postgres;

--
-- Name: tb_peserta_pmb_lokal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_peserta_pmb_lokal_id_seq OWNED BY public.tb_peserta_pmb_lokal.id;


--
-- Name: tb_prestasi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_prestasi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_prestasi_id_seq OWNER TO postgres;

--
-- Name: tb_prestasi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_prestasi_id_seq OWNED BY public.tb_prestasi.id;


--
-- Name: tb_profil_pt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_profil_pt (
    id_perguruan_tinggi uuid,
    kode_perguruan_tinggi numeric,
    nama_perguruan_tinggi character varying,
    telepon character varying,
    faximile character varying,
    email character varying,
    website character varying,
    jalan character varying,
    dusun character varying,
    rt_rw character varying,
    kelurahan character varying,
    kode_pos character varying,
    id_wilayah character varying,
    nama_wilayah character varying,
    lintang_bujur character varying,
    bank character varying,
    unit_cabang character varying,
    nomor_rekening character varying,
    mbs character varying,
    luas_tanah_milik character varying,
    luas_tanah_bukan_milik character varying,
    sk_pendirian character varying,
    tanggal_sk_pendirian date,
    id_status_milik character varying,
    nama_status_milik character varying,
    status_perguruan_tinggi character varying,
    sk_izin_operasional character varying,
    tanggal_izin_operasional date
);


ALTER TABLE public.tb_profil_pt OWNER TO postgres;

--
-- Name: tb_rencana_pembelajaran_matkul_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_rencana_pembelajaran_matkul_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_rencana_pembelajaran_matkul_id_seq OWNER TO postgres;

--
-- Name: tb_rencana_pembelajaran_matkul_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_rencana_pembelajaran_matkul_id_seq OWNED BY public.tb_rencana_pembelajaran_matkul.id;


--
-- Name: tb_riwayat_pend_dosen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_riwayat_pend_dosen (
    id integer NOT NULL,
    id_dosen integer,
    id_bidang_studi integer
);


ALTER TABLE public.tb_riwayat_pend_dosen OWNER TO postgres;

--
-- Name: COLUMN tb_riwayat_pend_dosen.id_dosen; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_riwayat_pend_dosen.id_dosen IS 'tb_dosen';


--
-- Name: tb_riwayat_pend_dosen_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_riwayat_pend_dosen_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_riwayat_pend_dosen_id_seq OWNER TO postgres;

--
-- Name: tb_riwayat_pend_dosen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_riwayat_pend_dosen_id_seq OWNED BY public.tb_riwayat_pend_dosen.id;


--
-- Name: tb_riwayat_pendidikan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_riwayat_pendidikan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_riwayat_pendidikan_id_seq OWNER TO postgres;

--
-- Name: tb_riwayat_pendidikan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_riwayat_pendidikan_id_seq OWNED BY public.tb_riwayat_pendidikan.id;


--
-- Name: tb_roster_asisten_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_roster_asisten_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_roster_asisten_id_seq OWNER TO postgres;

--
-- Name: tb_roster_asisten_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_roster_asisten_id_seq OWNED BY public.tb_roster_asisten.id;


--
-- Name: tb_roster_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_roster_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_roster_id_seq OWNER TO postgres;

--
-- Name: tb_roster_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_roster_id_seq OWNED BY public.tb_roster.id;


--
-- Name: tb_saran_survey; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_saran_survey (
    id bigint NOT NULL,
    id_riwayat_pendidikan integer,
    id_krs integer,
    saran text,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_saran_survey OWNER TO postgres;

--
-- Name: COLUMN tb_saran_survey.id_riwayat_pendidikan; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_saran_survey.id_riwayat_pendidikan IS 'tb_riwayat_pendidikan';


--
-- Name: COLUMN tb_saran_survey.id_krs; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_saran_survey.id_krs IS 'tb_krs';


--
-- Name: tb_saran_survey_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_saran_survey_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_saran_survey_id_seq OWNER TO postgres;

--
-- Name: tb_saran_survey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_saran_survey_id_seq OWNED BY public.tb_saran_survey.id;


--
-- Name: tb_substansi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_substansi (
    id integer NOT NULL,
    id_prodi integer,
    nama character varying,
    sks_mata_kuliah numeric DEFAULT 0,
    sks_tatap_muka numeric DEFAULT 0,
    sks_praktek numeric DEFAULT 0,
    sks_praktek_lapangan numeric DEFAULT 0,
    sks_simulasi numeric DEFAULT 0,
    id_jenis_substansi integer,
    id_substansi uuid,
    syncron_feeder boolean DEFAULT false,
    keterangan_error character varying,
    date_syncron timestamp without time zone,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying
);


ALTER TABLE public.tb_substansi OWNER TO postgres;

--
-- Name: COLUMN tb_substansi.id_prodi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_substansi.id_prodi IS 'tb_mst_prodi';


--
-- Name: COLUMN tb_substansi.id_jenis_substansi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_substansi.id_jenis_substansi IS 'tb_mst_jenis_substansi';


--
-- Name: COLUMN tb_substansi.id_substansi; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_substansi.id_substansi IS 'id feeder';


--
-- Name: tb_substansi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_substansi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_substansi_id_seq OWNER TO postgres;

--
-- Name: tb_substansi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_substansi_id_seq OWNED BY public.tb_substansi.id;


--
-- Name: tb_tentang_kampus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_tentang_kampus (
    content text,
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    thumbnail character varying DEFAULT 'about-img.jpg'::character varying
);


ALTER TABLE public.tb_tentang_kampus OWNER TO postgres;

--
-- Name: tb_unit_kelas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_unit_kelas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_unit_kelas_id_seq OWNER TO postgres;

--
-- Name: tb_unit_kelas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_unit_kelas_id_seq OWNED BY public.tb_unit_kelas.id;


--
-- Name: tb_unit_kelas_mahasiswa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_unit_kelas_mahasiswa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_unit_kelas_mahasiswa_id_seq OWNER TO postgres;

--
-- Name: tb_unit_kelas_mahasiswa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_unit_kelas_mahasiswa_id_seq OWNED BY public.tb_unit_kelas_mahasiswa.id;


--
-- Name: tb_unit_kelas_matakuliah_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_unit_kelas_matakuliah_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_unit_kelas_matakuliah_id_seq OWNER TO postgres;

--
-- Name: tb_unit_kelas_matakuliah_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_unit_kelas_matakuliah_id_seq OWNED BY public.tb_unit_kelas_matakuliah.id;


--
-- Name: tb_users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_users (
    id bigint NOT NULL,
    avatar character varying DEFAULT 'avatar-placeholder.png'::character varying,
    nama character varying,
    username character varying,
    email character varying,
    password character varying,
    role character varying(2),
    uploaded timestamp without time zone,
    modified timestamp without time zone,
    user_modified character varying,
    selected_id_prodi json,
    id_parent integer,
    last_login timestamp without time zone
);


ALTER TABLE public.tb_users OWNER TO postgres;

--
-- Name: COLUMN tb_users.role; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_users.role IS '1 = akademik;

2 = administrator;

3 = prodi;

4 = keuangan;

5 = mahasiswa;

6 = dosen;

7 = fakultas;

8 => user pendaftaran;

9 => calon mahasiswa;

10 => user verifikator pendaftaran;';


--
-- Name: COLUMN tb_users.id_parent; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.tb_users.id_parent IS 'if role 5 => tb_riwayat_pendidikan;';


--
-- Name: tb_users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_users_id_seq OWNER TO postgres;

--
-- Name: tb_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_users_id_seq OWNED BY public.tb_users.id;


--
-- Name: tbl_mst_prodi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tbl_mst_prodi (
    id_prodi character varying(300),
    kode character(5),
    nama character varying(80),
    status character(1),
    id_jenjang_pendidikan integer NOT NULL,
    id_fakultas integer NOT NULL,
    singkatan character varying(3),
    kelompok_ujian character varying(3),
    id_kepala_prodi integer
);


ALTER TABLE public.tbl_mst_prodi OWNER TO postgres;

--
-- Name: transkrip_mahasiswa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.transkrip_mahasiswa AS
 SELECT a.id_riwayat_pendidikan,
    a.kode_mk,
    a.nama_mk,
    a.semester,
    a.sks_mata_kuliah,
    a.nilai_akhir,
    a.nilai_huruf,
    a.indeks,
    a.nilai_bobot,
    a.is_lulus,
    a.sudah_isi_survey
   FROM ( SELECT tk.id_riwayat_pendidikan,
            tm.kode AS kode_mk,
            tm.nama AS nama_mk,
            (tmk.semester)::text AS semester,
            tm.sks_mata_kuliah,
            tkd.nilai_akhir,
            tkd.nilai_huruf,
            round((tkd.nilai_bobot / tm.sks_mata_kuliah), 2) AS indeks,
            tkd.nilai_bobot,
            tkd.is_lulus,
            tk.sudah_isi_survey
           FROM ((((public.tb_krs tk
             JOIN public.tb_krs_detail tkd ON ((tkd.id_krs = tk.id)))
             JOIN public.tb_riwayat_pendidikan trp ON ((trp.id = tk.id_riwayat_pendidikan)))
             LEFT JOIN public.tb_matkul_kurikulum tmk ON (((tmk.id_kurikulum = trp.id_kurikulum) AND (tmk.id_matkul = tkd.id_matakuliah))))
             JOIN public.tb_matakuliah tm ON ((tm.id = tkd.id_matakuliah)))
          WHERE ((tkd.is_digunakan = true) AND (tk.is_active = true))
          ORDER BY tmk.semester) a
UNION ALL
 SELECT b.id_riwayat_pendidikan,
    b.kode_mk,
    b.nama_mk,
    b.semester,
    b.sks_mata_kuliah,
    b.nilai_akhir,
    b.nilai_huruf,
    b.indeks,
    b.nilai_bobot,
    b.is_lulus,
    true AS sudah_isi_survey
   FROM ( SELECT tnt.id_riwayat_pendidikan,
            tm2.kode AS kode_mk,
            tm2.nama AS nama_mk,
            ''::text AS semester,
            tnt.sks_matkul AS sks_mata_kuliah,
            tnt.nilai_akhir,
            tnt.nilai_huruf,
            round((tnt.nilai_bobot / tnt.sks_matkul), 2) AS indeks,
            tnt.nilai_bobot,
            true AS is_lulus
           FROM (public.tb_nilai_transfer tnt
             JOIN public.tb_matakuliah tm2 ON ((tm2.id = tnt.id_matkul)))) b;


ALTER TABLE public.transkrip_mahasiswa OWNER TO postgres;

--
-- Name: updatenilaiperkuliahankelas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.updatenilaiperkuliahankelas AS
 SELECT tkd.id AS id_krs_detail,
    trp.id_prodi,
    trp.id_registrasi_mahasiswa,
    tuk.id_kelas_kuliah,
    COALESCE(tkd.nilai_akhir, (0)::numeric) AS nilai_angka,
    COALESCE(tmnh.bobot, (0)::numeric) AS nilai_indeks,
    tkd.nilai_huruf
   FROM (((((public.tb_krs_detail tkd
     LEFT JOIN public.tb_mst_nilai_huruf tmnh ON (((tmnh.nilai_huruf)::text = (tkd.nilai_huruf)::text)))
     LEFT JOIN public.tb_unit_kelas_mahasiswa tukm ON ((tukm.id_krs_detail = tkd.id)))
     LEFT JOIN public.tb_unit_kelas tuk ON ((tuk.id = tukm.id_unit_kelas)))
     JOIN public.tb_krs tk ON ((tk.id = tkd.id_krs)))
     JOIN public.tb_riwayat_pendidikan trp ON ((trp.id = tk.id_riwayat_pendidikan)))
  WHERE (tkd.syncron_feeder = false);


ALTER TABLE public.updatenilaiperkuliahankelas OWNER TO postgres;

--
-- Name: tb_absensi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_absensi ALTER COLUMN id SET DEFAULT nextval('public.tb_absensi_id_seq'::regclass);


--
-- Name: tb_absensi_mahasiswa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_absensi_mahasiswa ALTER COLUMN id SET DEFAULT nextval('public.tb_absensi_mahasiswa_id_seq'::regclass);


--
-- Name: tb_acara_mendatang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_acara_mendatang ALTER COLUMN id SET DEFAULT nextval('public.tb_acara_mendatang_id_seq'::regclass);


--
-- Name: tb_akm id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_akm ALTER COLUMN id SET DEFAULT nextval('public.tb_akm_id_seq'::regclass);


--
-- Name: tb_aktivitas_mahasiswa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_aktivitas_mahasiswa ALTER COLUMN id SET DEFAULT nextval('public.tb_aktivitas_mahasiswa_id_seq'::regclass);


--
-- Name: tb_anggota_aktivitas_mahasiswa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_anggota_aktivitas_mahasiswa ALTER COLUMN id SET DEFAULT nextval('public.tb_anggota_aktivitas_mahasiswa_id_seq'::regclass);


--
-- Name: tb_berita id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_berita ALTER COLUMN id SET DEFAULT nextval('public.tb_berita_id_seq'::regclass);


--
-- Name: tb_calon_mahasiswa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_calon_mahasiswa ALTER COLUMN id SET DEFAULT nextval('public.tb_calon_mahasiswa_id_seq'::regclass);


--
-- Name: tb_dosen id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_dosen ALTER COLUMN id SET DEFAULT nextval('public.tb_dosen_id_seq'::regclass);


--
-- Name: tb_jabfung_dosen id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jabfung_dosen ALTER COLUMN id SET DEFAULT nextval('public.tb_jabfung_dosen_id_seq'::regclass);


--
-- Name: tb_jadwal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jadwal ALTER COLUMN id SET DEFAULT nextval('public.tb_jadwal_id_seq'::regclass);


--
-- Name: tb_jadwal_pembayaran id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jadwal_pembayaran ALTER COLUMN id SET DEFAULT nextval('public.tb_jadwal_pembayaran_id_seq'::regclass);


--
-- Name: tb_jadwal_ujian_peserta_pmb id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jadwal_ujian_peserta_pmb ALTER COLUMN id SET DEFAULT nextval('public.tb_jadwal_ujian_peserta_pmb_id_seq'::regclass);


--
-- Name: tb_jadwal_ujian_pmb id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jadwal_ujian_pmb ALTER COLUMN id SET DEFAULT nextval('public.tb_jadwal_ujian_pmb_id_seq'::regclass);


--
-- Name: tb_jawaban_survey id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jawaban_survey ALTER COLUMN id SET DEFAULT nextval('public.tb_jawaban_survey_id_seq'::regclass);


--
-- Name: tb_join_matkul_kurikulum id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_join_matkul_kurikulum ALTER COLUMN id SET DEFAULT nextval('public.tb_join_matkul_kurikulum_id_seq'::regclass);


--
-- Name: tb_jurnal_ajar id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jurnal_ajar ALTER COLUMN id SET DEFAULT nextval('public.tb_jurnal_ajar_id_seq'::regclass);


--
-- Name: tb_kampus_merdeka id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_kampus_merdeka ALTER COLUMN id SET DEFAULT nextval('public.tb_kampus_merdeka_id_seq'::regclass);


--
-- Name: tb_kebutuhan_khusus_ayah id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_kebutuhan_khusus_ayah ALTER COLUMN id SET DEFAULT nextval('public.tb_kebutuhan_khusus_ayah_id_seq'::regclass);


--
-- Name: tb_kebutuhan_khusus_ibu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_kebutuhan_khusus_ibu ALTER COLUMN id SET DEFAULT nextval('public.tb_kebutuhan_khusus_ibu_id_seq'::regclass);


--
-- Name: tb_kebutuhan_khusus_mhs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_kebutuhan_khusus_mhs ALTER COLUMN id SET DEFAULT nextval('public.tb_kebutuhan_khusus_mhs_id_seq'::regclass);


--
-- Name: tb_krs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_krs ALTER COLUMN id SET DEFAULT nextval('public.tb_krs_id_seq'::regclass);


--
-- Name: tb_krs_backup id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_krs_backup ALTER COLUMN id SET DEFAULT nextval('public.tb_krs_backup_id_seq'::regclass);


--
-- Name: tb_krs_detail id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_krs_detail ALTER COLUMN id SET DEFAULT nextval('public.tb_krs_detail_id_seq'::regclass);


--
-- Name: tb_krs_detail_backup id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_krs_detail_backup ALTER COLUMN id SET DEFAULT nextval('public.tb_krs_detail_backup_id_seq'::regclass);


--
-- Name: tb_kurikulum id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_kurikulum ALTER COLUMN id SET DEFAULT nextval('public.tb_kurikulum_id_seq'::regclass);


--
-- Name: tb_lulus_dropout id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_lulus_dropout ALTER COLUMN id SET DEFAULT nextval('public.tb_lulus_dropout_id_seq'::regclass);


--
-- Name: tb_mahasiswa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mahasiswa ALTER COLUMN id SET DEFAULT nextval('public.tb_mahasiswa_id_seq'::regclass);


--
-- Name: tb_matakuliah id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_matakuliah ALTER COLUMN id SET DEFAULT nextval('public.tb_matakuliah_id_seq'::regclass);


--
-- Name: tb_matkul_bersyarat id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_matkul_bersyarat ALTER COLUMN id SET DEFAULT nextval('public.tb_matkul_bersyarat_id_seq'::regclass);


--
-- Name: tb_matkul_kurikulum id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_matkul_kurikulum ALTER COLUMN id SET DEFAULT nextval('public.tb_matkul_kurikulum_id_seq'::regclass);


--
-- Name: tb_matkul_prodi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_matkul_prodi ALTER COLUMN id SET DEFAULT nextval('public.tb_matkul_prodi_id_seq'::regclass);


--
-- Name: tb_mst_alat_transportasi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_alat_transportasi ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_alat_transportasi_id_alat_transportasi_seq'::regclass);


--
-- Name: tb_mst_bidang_minat id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_bidang_minat ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_bidang_minat_id_seq'::regclass);


--
-- Name: tb_mst_config_krs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_config_krs ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_config_krs_id_seq'::regclass);


--
-- Name: tb_mst_fakultas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_fakultas ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_fakultas_id_seq'::regclass);


--
-- Name: tb_mst_golpang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_golpang ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_golpang_id_seq'::regclass);


--
-- Name: tb_mst_jabatan_fungsional id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jabatan_fungsional ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jabatan_fungsional_id_seq'::regclass);


--
-- Name: tb_mst_jalurmasuk id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jalurmasuk ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jalurmasuk_id_seq'::regclass);


--
-- Name: tb_mst_jalurmasuk_pmb id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jalurmasuk_pmb ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jalurmasuk_pmb_id_seq'::regclass);


--
-- Name: tb_mst_jam_belajar id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jam_belajar ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jam_belajar_id_seq'::regclass);


--
-- Name: tb_mst_jenis_aktivitas_mahasiswa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_aktivitas_mahasiswa ALTER COLUMN id SET DEFAULT nextval('public.tb_jenis_aktivitas_mahasiswa_id_seq'::regclass);


--
-- Name: tb_mst_jenis_evaluasi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_evaluasi ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jenis_evaluasi_id_seq'::regclass);


--
-- Name: tb_mst_jenis_jadwal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_jadwal ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jenis_jadwal_id_seq'::regclass);


--
-- Name: tb_mst_jenis_keluar id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_keluar ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jenis_keluar_id_seq'::regclass);


--
-- Name: tb_mst_jenis_pegawai id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_pegawai ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jenis_pegawai_id_seq'::regclass);


--
-- Name: tb_mst_jenis_pendaftaran id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_pendaftaran ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jenis_pendaftaran_id_seq'::regclass);


--
-- Name: tb_mst_jenis_prestasi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_prestasi ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jenis_prestasi_id_seq'::regclass);


--
-- Name: tb_mst_jenis_substansi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_substansi ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jenis_substansi_id_seq'::regclass);


--
-- Name: tb_mst_jenis_survey id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_survey ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jenis_survey_id_seq'::regclass);


--
-- Name: tb_mst_jenis_tinggal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_tinggal ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jenis_tinggal_id_jenis_tinggal_seq'::regclass);


--
-- Name: tb_mst_jenjang_pend id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenjang_pend ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_jenjang_pend_id_jenjang_didik_seq'::regclass);


--
-- Name: tb_mst_kebutuhan_khusus id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_kebutuhan_khusus ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_kebutuhan_khusus_id_seq'::regclass);


--
-- Name: tb_mst_komponen_pembayaran id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_komponen_pembayaran ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_komponen_pembayaran_id_seq'::regclass);


--
-- Name: tb_mst_lembaga_pengangkat id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_lembaga_pengangkat ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_lembaga_pengangkat_id_seq'::regclass);


--
-- Name: tb_mst_model_penilaian id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_model_penilaian ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_model_penilaian_id_seq'::regclass);


--
-- Name: tb_mst_model_penilaian_detail id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_model_penilaian_detail ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_model_penilaian_detail_id_seq'::regclass);


--
-- Name: tb_mst_pekerjaan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_pekerjaan ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_pekerjaan_id_seq'::regclass);


--
-- Name: tb_mst_pembiayaan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_pembiayaan ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_pembiayaan_id_seq'::regclass);


--
-- Name: tb_mst_penghasilan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_penghasilan ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_penghasilan_id_seq'::regclass);


--
-- Name: tb_mst_periode id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_periode ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_periode_id_seq'::regclass);


--
-- Name: tb_mst_periode_pmb id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_periode_pmb ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_periode_pmb_id_seq'::regclass);


--
-- Name: tb_mst_prodi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_prodi ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_prodi_id_seq'::regclass);


--
-- Name: tb_mst_ruangan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_ruangan ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_ruangan_id_seq'::regclass);


--
-- Name: tb_mst_ruangan_pmb id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_ruangan_pmb ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_ruangan_pmb_id_seq'::regclass);


--
-- Name: tb_mst_spp_prodi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_spp_prodi ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_spp_prodi_id_seq'::regclass);


--
-- Name: tb_mst_status_keaktifan_pegawai id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_status_keaktifan_pegawai ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_status_keaktifan_pegawai_id_seq'::regclass);


--
-- Name: tb_mst_status_kepegawaian id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_status_kepegawaian ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_status_kepegawaian_id_seq'::regclass);


--
-- Name: tb_mst_tingkat_prestasi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_tingkat_prestasi ALTER COLUMN id SET DEFAULT nextval('public.tb_mst_tingkat_prestasi_id_seq'::regclass);


--
-- Name: tb_nilai_transfer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_nilai_transfer ALTER COLUMN id SET DEFAULT nextval('public.tb_nilai_transfer_id_seq'::regclass);


--
-- Name: tb_pangkat_dosen id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pangkat_dosen ALTER COLUMN id SET DEFAULT nextval('public.tb_pangkat_dosen_id_seq'::regclass);


--
-- Name: tb_pembayaran id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pembayaran ALTER COLUMN id SET DEFAULT nextval('public.tb_pembayaran_id_seq'::regclass);


--
-- Name: tb_pembimbing id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pembimbing ALTER COLUMN id SET DEFAULT nextval('public.tb_pembimbing_id_seq'::regclass);


--
-- Name: tb_pengajuan_surat id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pengajuan_surat ALTER COLUMN id SET DEFAULT nextval('public.tb_pengajuan_surat_id_seq'::regclass);


--
-- Name: tb_penguji id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_penguji ALTER COLUMN id SET DEFAULT nextval('public.tb_penguji_id_seq'::regclass);


--
-- Name: tb_penugasan_dosen id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_penugasan_dosen ALTER COLUMN id SET DEFAULT nextval('public.tb_penugasan_dosen_id_seq'::regclass);


--
-- Name: tb_periode_feeder id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_periode_feeder ALTER COLUMN id SET DEFAULT nextval('public.tb_periode_feeder_id_seq'::regclass);


--
-- Name: tb_pertanyaan_survey id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pertanyaan_survey ALTER COLUMN id SET DEFAULT nextval('public.tb_pertanyaan_survey_id_seq'::regclass);


--
-- Name: tb_perwalian id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_perwalian ALTER COLUMN id SET DEFAULT nextval('public.tb_perwalian_id_seq'::regclass);


--
-- Name: tb_peserta_pmb_lokal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_peserta_pmb_lokal ALTER COLUMN id SET DEFAULT nextval('public.tb_peserta_pmb_lokal_id_seq'::regclass);


--
-- Name: tb_prestasi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_prestasi ALTER COLUMN id SET DEFAULT nextval('public.tb_prestasi_id_seq'::regclass);


--
-- Name: tb_rencana_pembelajaran_matkul id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_rencana_pembelajaran_matkul ALTER COLUMN id SET DEFAULT nextval('public.tb_rencana_pembelajaran_matkul_id_seq'::regclass);


--
-- Name: tb_riwayat_pend_dosen id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_riwayat_pend_dosen ALTER COLUMN id SET DEFAULT nextval('public.tb_riwayat_pend_dosen_id_seq'::regclass);


--
-- Name: tb_riwayat_pendidikan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_riwayat_pendidikan ALTER COLUMN id SET DEFAULT nextval('public.tb_riwayat_pendidikan_id_seq'::regclass);


--
-- Name: tb_roster id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_roster ALTER COLUMN id SET DEFAULT nextval('public.tb_roster_id_seq'::regclass);


--
-- Name: tb_roster_asisten id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_roster_asisten ALTER COLUMN id SET DEFAULT nextval('public.tb_roster_asisten_id_seq'::regclass);


--
-- Name: tb_saran_survey id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_saran_survey ALTER COLUMN id SET DEFAULT nextval('public.tb_saran_survey_id_seq'::regclass);


--
-- Name: tb_substansi id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_substansi ALTER COLUMN id SET DEFAULT nextval('public.tb_substansi_id_seq'::regclass);


--
-- Name: tb_unit_kelas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_unit_kelas ALTER COLUMN id SET DEFAULT nextval('public.tb_unit_kelas_id_seq'::regclass);


--
-- Name: tb_unit_kelas_mahasiswa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_unit_kelas_mahasiswa ALTER COLUMN id SET DEFAULT nextval('public.tb_unit_kelas_mahasiswa_id_seq'::regclass);


--
-- Name: tb_unit_kelas_matakuliah id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_unit_kelas_matakuliah ALTER COLUMN id SET DEFAULT nextval('public.tb_unit_kelas_matakuliah_id_seq'::regclass);


--
-- Name: tb_users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_users ALTER COLUMN id SET DEFAULT nextval('public.tb_users_id_seq'::regclass);


--
-- Name: login_session login_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.login_session
    ADD CONSTRAINT login_session_pkey PRIMARY KEY (id, ip_address);


--
-- Name: tb_absensi_mahasiswa tb_absensi_mahasiswa_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_absensi_mahasiswa
    ADD CONSTRAINT tb_absensi_mahasiswa_pk PRIMARY KEY (id);


--
-- Name: tb_absensi_mahasiswa tb_absensi_mahasiswa_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_absensi_mahasiswa
    ADD CONSTRAINT tb_absensi_mahasiswa_un UNIQUE (id_roster, id_riwayat_pendidikan, tanggal_absen);


--
-- Name: tb_absensi tb_absensi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_absensi
    ADD CONSTRAINT tb_absensi_pk PRIMARY KEY (id);


--
-- Name: tb_acara_mendatang tb_acara_mendatang_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_acara_mendatang
    ADD CONSTRAINT tb_acara_mendatang_pk PRIMARY KEY (id);


--
-- Name: tb_akm tb_akm_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_akm
    ADD CONSTRAINT tb_akm_pk PRIMARY KEY (id);


--
-- Name: tb_akm tb_akm_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_akm
    ADD CONSTRAINT tb_akm_un UNIQUE (id_riwayat_pendidikan, tahun_ajaran, id_semester);


--
-- Name: tb_aktivitas_mahasiswa tb_aktivitas_mahasiswa_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_aktivitas_mahasiswa
    ADD CONSTRAINT tb_aktivitas_mahasiswa_pk PRIMARY KEY (id);


--
-- Name: tb_anggota_aktivitas_mahasiswa tb_anggota_aktivitas_mahasiswa_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_anggota_aktivitas_mahasiswa
    ADD CONSTRAINT tb_anggota_aktivitas_mahasiswa_pk PRIMARY KEY (id);


--
-- Name: tb_berita tb_berita_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_berita
    ADD CONSTRAINT tb_berita_pk PRIMARY KEY (id);


--
-- Name: tb_calon_mahasiswa tb_calon_mahasiswa_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_calon_mahasiswa
    ADD CONSTRAINT tb_calon_mahasiswa_pk PRIMARY KEY (id);


--
-- Name: tb_calon_mahasiswa tb_calon_mahasiswa_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_calon_mahasiswa
    ADD CONSTRAINT tb_calon_mahasiswa_un UNIQUE (nim);


--
-- Name: tb_dosen tb_dosen_email; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_dosen
    ADD CONSTRAINT tb_dosen_email UNIQUE (email);


--
-- Name: tb_dosen tb_dosen_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_dosen
    ADD CONSTRAINT tb_dosen_pk PRIMARY KEY (id);


--
-- Name: tb_informasi_dan_jadwal_pmb tb_informasi_dan_jadwal_pmb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_informasi_dan_jadwal_pmb
    ADD CONSTRAINT tb_informasi_dan_jadwal_pmb_pk PRIMARY KEY (slug);


--
-- Name: tb_jabfung_dosen tb_jabfung_dosen_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jabfung_dosen
    ADD CONSTRAINT tb_jabfung_dosen_pk PRIMARY KEY (id);


--
-- Name: tb_jadwal_pembayaran tb_jadwal_pembayaran_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jadwal_pembayaran
    ADD CONSTRAINT tb_jadwal_pembayaran_pk PRIMARY KEY (id);


--
-- Name: tb_jadwal tb_jadwal_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jadwal
    ADD CONSTRAINT tb_jadwal_pk PRIMARY KEY (id);


--
-- Name: tb_jadwal_ujian_peserta_pmb tb_jadwal_ujian_peserta_pmb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jadwal_ujian_peserta_pmb
    ADD CONSTRAINT tb_jadwal_ujian_peserta_pmb_pk PRIMARY KEY (id);


--
-- Name: tb_jadwal_ujian_pmb tb_jadwal_ujian_pmb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jadwal_ujian_pmb
    ADD CONSTRAINT tb_jadwal_ujian_pmb_pk PRIMARY KEY (id);


--
-- Name: tb_jawaban_survey tb_jawaban_survey_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jawaban_survey
    ADD CONSTRAINT tb_jawaban_survey_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jenis_aktivitas_mahasiswa tb_jenis_aktivitas_mahasiswa_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_aktivitas_mahasiswa
    ADD CONSTRAINT tb_jenis_aktivitas_mahasiswa_pk PRIMARY KEY (id);


--
-- Name: tb_join_matkul_kurikulum tb_join_matkul_kurikulum_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_join_matkul_kurikulum
    ADD CONSTRAINT tb_join_matkul_kurikulum_pk PRIMARY KEY (id);


--
-- Name: tb_jurnal_ajar tb_jurnal_ajar_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_jurnal_ajar
    ADD CONSTRAINT tb_jurnal_ajar_pk PRIMARY KEY (id);


--
-- Name: tb_kampus_merdeka tb_kampus_merdeka_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_kampus_merdeka
    ADD CONSTRAINT tb_kampus_merdeka_pk PRIMARY KEY (id);


--
-- Name: tb_kebutuhan_khusus_ayah tb_kebutuhan_khusus_ayah_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_kebutuhan_khusus_ayah
    ADD CONSTRAINT tb_kebutuhan_khusus_ayah_pk PRIMARY KEY (id);


--
-- Name: tb_kebutuhan_khusus_ibu tb_kebutuhan_khusus_ibu_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_kebutuhan_khusus_ibu
    ADD CONSTRAINT tb_kebutuhan_khusus_ibu_pk PRIMARY KEY (id);


--
-- Name: tb_krs_detail tb_krs_detail_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_krs_detail
    ADD CONSTRAINT tb_krs_detail_pk PRIMARY KEY (id);


--
-- Name: tb_krs tb_krs_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_krs
    ADD CONSTRAINT tb_krs_pk PRIMARY KEY (id);


--
-- Name: tb_krs tb_krs_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_krs
    ADD CONSTRAINT tb_krs_un UNIQUE (id_riwayat_pendidikan, tahun_ajaran, id_semester);


--
-- Name: tb_kurikulum tb_kurikulum_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_kurikulum
    ADD CONSTRAINT tb_kurikulum_pk PRIMARY KEY (id);


--
-- Name: tb_lulus_dropout tb_lulus_dropout_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_lulus_dropout
    ADD CONSTRAINT tb_lulus_dropout_pk PRIMARY KEY (id);


--
-- Name: tb_mahasiswa tb_mahasiswa_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mahasiswa
    ADD CONSTRAINT tb_mahasiswa_pk PRIMARY KEY (id);


--
-- Name: tb_matakuliah tb_matakuliah_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_matakuliah
    ADD CONSTRAINT tb_matakuliah_pk PRIMARY KEY (id);


--
-- Name: tb_matakuliah tb_matakuliah_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_matakuliah
    ADD CONSTRAINT tb_matakuliah_un UNIQUE (kode);


--
-- Name: tb_matkul_bersyarat tb_matkul_bersyarat_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_matkul_bersyarat
    ADD CONSTRAINT tb_matkul_bersyarat_pk PRIMARY KEY (id);


--
-- Name: tb_matkul_bersyarat tb_matkul_bersyarat_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_matkul_bersyarat
    ADD CONSTRAINT tb_matkul_bersyarat_un UNIQUE (id_kurikulum, id_matkul_kurikulum, id_matkul);


--
-- Name: tb_matkul_kurikulum tb_matkul_kurikulum_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_matkul_kurikulum
    ADD CONSTRAINT tb_matkul_kurikulum_pk PRIMARY KEY (id);


--
-- Name: tb_matkul_kurikulum tb_matkul_kurikulum_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_matkul_kurikulum
    ADD CONSTRAINT tb_matkul_kurikulum_un UNIQUE (id_kurikulum, id_matkul);


--
-- Name: tb_matkul_prodi tb_matkul_prodi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_matkul_prodi
    ADD CONSTRAINT tb_matkul_prodi_pk PRIMARY KEY (id);


--
-- Name: tb_matkul_prodi tb_matkul_prodi_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_matkul_prodi
    ADD CONSTRAINT tb_matkul_prodi_un UNIQUE (id_matkul, id_prodi);


--
-- Name: tb_mst_agama tb_mst_agama_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_agama
    ADD CONSTRAINT tb_mst_agama_pk PRIMARY KEY (id);


--
-- Name: tb_mst_alat_transportasi tb_mst_alat_transportasi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_alat_transportasi
    ADD CONSTRAINT tb_mst_alat_transportasi_pk PRIMARY KEY (id);


--
-- Name: tb_mst_all_prodi tb_mst_all_prodi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_all_prodi
    ADD CONSTRAINT tb_mst_all_prodi_pk PRIMARY KEY (id);


--
-- Name: tb_mst_all_pt tb_mst_all_pt_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_all_pt
    ADD CONSTRAINT tb_mst_all_pt_pk PRIMARY KEY (id);


--
-- Name: tb_mst_bidang_minat tb_mst_bidang_minat_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_bidang_minat
    ADD CONSTRAINT tb_mst_bidang_minat_pk PRIMARY KEY (id);


--
-- Name: tb_mst_config_krs tb_mst_config_krs_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_config_krs
    ADD CONSTRAINT tb_mst_config_krs_pk PRIMARY KEY (id);


--
-- Name: tb_mst_fakultas tb_mst_fakultas_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_fakultas
    ADD CONSTRAINT tb_mst_fakultas_pk PRIMARY KEY (id);


--
-- Name: tb_mst_golpang tb_mst_golpang_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_golpang
    ADD CONSTRAINT tb_mst_golpang_pk PRIMARY KEY (id);


--
-- Name: tb_mst_hari tb_mst_hari_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_hari
    ADD CONSTRAINT tb_mst_hari_pk PRIMARY KEY (id);


--
-- Name: tb_mst_ikatan_kerja tb_mst_ikatan_kerja_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_ikatan_kerja
    ADD CONSTRAINT tb_mst_ikatan_kerja_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jabatan_fungsional tb_mst_jabatan_fungsional_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jabatan_fungsional
    ADD CONSTRAINT tb_mst_jabatan_fungsional_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jalurmasuk tb_mst_jalurmasuk_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jalurmasuk
    ADD CONSTRAINT tb_mst_jalurmasuk_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jalurmasuk_pmb tb_mst_jalurmasuk_pmb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jalurmasuk_pmb
    ADD CONSTRAINT tb_mst_jalurmasuk_pmb_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jam_belajar tb_mst_jam_belajar_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jam_belajar
    ADD CONSTRAINT tb_mst_jam_belajar_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jenis_evaluasi tb_mst_jenis_evaluasi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_evaluasi
    ADD CONSTRAINT tb_mst_jenis_evaluasi_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jenis_jadwal tb_mst_jenis_jadwal_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_jadwal
    ADD CONSTRAINT tb_mst_jenis_jadwal_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jenis_matkul tb_mst_jenis_matkul_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_matkul
    ADD CONSTRAINT tb_mst_jenis_matkul_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jenis_pegawai tb_mst_jenis_pegawai_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_pegawai
    ADD CONSTRAINT tb_mst_jenis_pegawai_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jenis_pendaftaran tb_mst_jenis_pendaftaran_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_pendaftaran
    ADD CONSTRAINT tb_mst_jenis_pendaftaran_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jenis_prestasi tb_mst_jenis_prestasi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_prestasi
    ADD CONSTRAINT tb_mst_jenis_prestasi_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jenis_substansi tb_mst_jenis_substansi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_substansi
    ADD CONSTRAINT tb_mst_jenis_substansi_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jenis_survey tb_mst_jenis_survey_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_survey
    ADD CONSTRAINT tb_mst_jenis_survey_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jenis_tinggal tb_mst_jenis_tinggal_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenis_tinggal
    ADD CONSTRAINT tb_mst_jenis_tinggal_pk PRIMARY KEY (id);


--
-- Name: tb_mst_jenjang_pend tb_mst_jenjang_pend_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_jenjang_pend
    ADD CONSTRAINT tb_mst_jenjang_pend_pk PRIMARY KEY (id);


--
-- Name: tb_mst_kebutuhan_khusus tb_mst_kebutuhan_khusus_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_kebutuhan_khusus
    ADD CONSTRAINT tb_mst_kebutuhan_khusus_pk PRIMARY KEY (id);


--
-- Name: tb_mst_kelompok_matkul tb_mst_kelompok_matkul_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_kelompok_matkul
    ADD CONSTRAINT tb_mst_kelompok_matkul_pk PRIMARY KEY (id);


--
-- Name: tb_mst_komponen_pembayaran tb_mst_komponen_pembayaran_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_komponen_pembayaran
    ADD CONSTRAINT tb_mst_komponen_pembayaran_pk PRIMARY KEY (id);


--
-- Name: tb_mst_komponen_pembayaran tb_mst_komponen_pembayaran_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_komponen_pembayaran
    ADD CONSTRAINT tb_mst_komponen_pembayaran_un UNIQUE (kode);


--
-- Name: tb_mst_lembaga_pengangkat tb_mst_lembaga_pengangkat_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_lembaga_pengangkat
    ADD CONSTRAINT tb_mst_lembaga_pengangkat_pk PRIMARY KEY (id);


--
-- Name: tb_mst_model_penilaian_detail tb_mst_model_penilaian_detail_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_model_penilaian_detail
    ADD CONSTRAINT tb_mst_model_penilaian_detail_pk PRIMARY KEY (id);


--
-- Name: tb_mst_model_penilaian tb_mst_model_penilaian_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_model_penilaian
    ADD CONSTRAINT tb_mst_model_penilaian_pk PRIMARY KEY (id);


--
-- Name: tb_mst_negara tb_mst_negara_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_negara
    ADD CONSTRAINT tb_mst_negara_pk PRIMARY KEY (id);


--
-- Name: tb_mst_nilai_huruf tb_mst_nilai_huruf_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_nilai_huruf
    ADD CONSTRAINT tb_mst_nilai_huruf_un UNIQUE (nilai_huruf);


--
-- Name: tb_mst_pekerjaan tb_mst_pekerjaan_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_pekerjaan
    ADD CONSTRAINT tb_mst_pekerjaan_pk PRIMARY KEY (id);


--
-- Name: tb_mst_pembiayaan tb_mst_pembiayaan_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_pembiayaan
    ADD CONSTRAINT tb_mst_pembiayaan_pk PRIMARY KEY (id);


--
-- Name: tb_mst_penghasilan tb_mst_penghasilan_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_penghasilan
    ADD CONSTRAINT tb_mst_penghasilan_pk PRIMARY KEY (id);


--
-- Name: tb_mst_perguruan_tinggi tb_mst_perguruan_tinggi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_perguruan_tinggi
    ADD CONSTRAINT tb_mst_perguruan_tinggi_pk PRIMARY KEY (id);


--
-- Name: tb_mst_periode tb_mst_periode_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_periode
    ADD CONSTRAINT tb_mst_periode_pk PRIMARY KEY (id);


--
-- Name: tb_mst_periode_pmb tb_mst_periode_pmb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_periode_pmb
    ADD CONSTRAINT tb_mst_periode_pmb_pk PRIMARY KEY (id);


--
-- Name: tb_mst_prodi tb_mst_prodi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_prodi
    ADD CONSTRAINT tb_mst_prodi_pk PRIMARY KEY (id);


--
-- Name: tb_mst_prodi tb_mst_prodi_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_prodi
    ADD CONSTRAINT tb_mst_prodi_un UNIQUE (kode);


--
-- Name: tb_mst_ruangan tb_mst_ruangan_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_ruangan
    ADD CONSTRAINT tb_mst_ruangan_pk PRIMARY KEY (id);


--
-- Name: tb_mst_ruangan_pmb tb_mst_ruangan_pmb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_ruangan_pmb
    ADD CONSTRAINT tb_mst_ruangan_pmb_pk PRIMARY KEY (id);


--
-- Name: tb_mst_spp_prodi tb_mst_spp_prodi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_spp_prodi
    ADD CONSTRAINT tb_mst_spp_prodi_pk PRIMARY KEY (id);


--
-- Name: tb_mst_status_mahasiswa tb_mst_status_mahasiswa_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_status_mahasiswa
    ADD CONSTRAINT tb_mst_status_mahasiswa_pk PRIMARY KEY (id);


--
-- Name: tb_mst_wilayah tb_mst_wilayah_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_wilayah
    ADD CONSTRAINT tb_mst_wilayah_pk PRIMARY KEY (id);


--
-- Name: tb_nilai_transfer tb_nilai_transfer_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_nilai_transfer
    ADD CONSTRAINT tb_nilai_transfer_pk PRIMARY KEY (id);


--
-- Name: tb_pangkat_dosen tb_pangkat_dosen_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pangkat_dosen
    ADD CONSTRAINT tb_pangkat_dosen_pk PRIMARY KEY (id);


--
-- Name: tb_pembayaran tb_pembayaran_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pembayaran
    ADD CONSTRAINT tb_pembayaran_pk PRIMARY KEY (id);


--
-- Name: tb_pembayaran tb_pembayaran_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pembayaran
    ADD CONSTRAINT tb_pembayaran_un UNIQUE (tahun_ajaran, id_semester, no_invoice);


--
-- Name: tb_pembimbing tb_pembimbing_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pembimbing
    ADD CONSTRAINT tb_pembimbing_pk PRIMARY KEY (id);


--
-- Name: tb_pengajuan_surat tb_pengajuan_surat_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pengajuan_surat
    ADD CONSTRAINT tb_pengajuan_surat_pk PRIMARY KEY (id);


--
-- Name: tb_penguji tb_penguji_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_penguji
    ADD CONSTRAINT tb_penguji_pk PRIMARY KEY (id);


--
-- Name: tb_penugasan_dosen tb_penugasan_dosen_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_penugasan_dosen
    ADD CONSTRAINT tb_penugasan_dosen_pk PRIMARY KEY (id);


--
-- Name: tb_periode_feeder tb_periode_feeder_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_periode_feeder
    ADD CONSTRAINT tb_periode_feeder_pk PRIMARY KEY (id);


--
-- Name: tb_pertanyaan_survey tb_pertanyaan_survey_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_pertanyaan_survey
    ADD CONSTRAINT tb_pertanyaan_survey_pk PRIMARY KEY (id);


--
-- Name: tb_perwalian tb_perwalian_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_perwalian
    ADD CONSTRAINT tb_perwalian_pk PRIMARY KEY (id);


--
-- Name: tb_peserta_pmb_lokal tb_peserta_pmb_lokal_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_peserta_pmb_lokal
    ADD CONSTRAINT tb_peserta_pmb_lokal_pk PRIMARY KEY (id);


--
-- Name: tb_peserta_pmb_lokal tb_peserta_pmb_lokal_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_peserta_pmb_lokal
    ADD CONSTRAINT tb_peserta_pmb_lokal_un UNIQUE (id_calon, periode_masuk);


--
-- Name: tb_prestasi tb_prestasi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_prestasi
    ADD CONSTRAINT tb_prestasi_pk PRIMARY KEY (id);


--
-- Name: tb_rencana_pembelajaran_matkul tb_rencana_pembelajaran_matkul_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_rencana_pembelajaran_matkul
    ADD CONSTRAINT tb_rencana_pembelajaran_matkul_pk PRIMARY KEY (id);


--
-- Name: tb_riwayat_pend_dosen tb_riwayat_pend_dosen_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_riwayat_pend_dosen
    ADD CONSTRAINT tb_riwayat_pend_dosen_pk PRIMARY KEY (id);


--
-- Name: tb_riwayat_pendidikan tb_riwayat_pendidikan_id_registrasi_mahasiswa; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_riwayat_pendidikan
    ADD CONSTRAINT tb_riwayat_pendidikan_id_registrasi_mahasiswa UNIQUE (id_registrasi_mahasiswa);


--
-- Name: tb_riwayat_pendidikan tb_riwayat_pendidikan_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_riwayat_pendidikan
    ADD CONSTRAINT tb_riwayat_pendidikan_pk PRIMARY KEY (id);


--
-- Name: tb_riwayat_pendidikan tb_riwayat_pendidikan_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_riwayat_pendidikan
    ADD CONSTRAINT tb_riwayat_pendidikan_un UNIQUE (nim);


--
-- Name: tb_roster_asisten tb_roster_asisten_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_roster_asisten
    ADD CONSTRAINT tb_roster_asisten_pk PRIMARY KEY (id);


--
-- Name: tb_roster tb_roster_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_roster
    ADD CONSTRAINT tb_roster_pk PRIMARY KEY (id);


--
-- Name: tb_saran_survey tb_saran_survey_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_saran_survey
    ADD CONSTRAINT tb_saran_survey_pk PRIMARY KEY (id);


--
-- Name: tb_substansi tb_substansi_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_substansi
    ADD CONSTRAINT tb_substansi_pk PRIMARY KEY (id);


--
-- Name: tb_unit_kelas_mahasiswa tb_unit_kelas_mahasiswa_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_unit_kelas_mahasiswa
    ADD CONSTRAINT tb_unit_kelas_mahasiswa_pk PRIMARY KEY (id);


--
-- Name: tb_unit_kelas_matakuliah tb_unit_kelas_matakuliah_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_unit_kelas_matakuliah
    ADD CONSTRAINT tb_unit_kelas_matakuliah_pk PRIMARY KEY (id);


--
-- Name: tb_unit_kelas tb_unit_kelas_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_unit_kelas
    ADD CONSTRAINT tb_unit_kelas_pk PRIMARY KEY (id);


--
-- Name: tb_unit_kelas tb_unit_kelas_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_unit_kelas
    ADD CONSTRAINT tb_unit_kelas_un UNIQUE (kode_unit);


--
-- Name: tb_users tb_users_email; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_users
    ADD CONSTRAINT tb_users_email UNIQUE (email);


--
-- Name: tb_users tb_users_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_users
    ADD CONSTRAINT tb_users_pk PRIMARY KEY (id);


--
-- Name: tb_users tb_users_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_users
    ADD CONSTRAINT tb_users_un UNIQUE (username);


--
-- Name: ci_sessions_timestamp; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ci_sessions_timestamp ON public.login_session USING btree ("timestamp");


--
-- Name: tb_absensi_id_roster_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_absensi_id_roster_idx ON public.tb_absensi USING btree (id_roster);


--
-- Name: tb_absensi_mahasiswa_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_absensi_mahasiswa_id_riwayat_pendidikan_idx ON public.tb_absensi_mahasiswa USING btree (id_riwayat_pendidikan);


--
-- Name: tb_absensi_mahasiswa_id_roster_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_absensi_mahasiswa_id_roster_idx ON public.tb_absensi_mahasiswa USING btree (id_roster);


--
-- Name: tb_akm_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_akm_id_riwayat_pendidikan_idx ON public.tb_akm USING btree (id_riwayat_pendidikan);


--
-- Name: tb_akm_id_status_mahasiswa_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_akm_id_status_mahasiswa_idx ON public.tb_akm USING btree (id_status_mahasiswa);


--
-- Name: tb_anggota_aktivitas_mahasiswa_id_aktivitas_mahasiswa_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_anggota_aktivitas_mahasiswa_id_aktivitas_mahasiswa_idx ON public.tb_anggota_aktivitas_mahasiswa USING btree (id_aktivitas_mahasiswa);


--
-- Name: tb_anggota_aktivitas_mahasiswa_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_anggota_aktivitas_mahasiswa_id_riwayat_pendidikan_idx ON public.tb_anggota_aktivitas_mahasiswa USING btree (id_riwayat_pendidikan);


--
-- Name: tb_dosen_id_agama_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_dosen_id_agama_idx ON public.tb_dosen USING btree (id_agama);


--
-- Name: tb_dosen_id_golpang_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_dosen_id_golpang_idx ON public.tb_dosen USING btree (id_golpang);


--
-- Name: tb_dosen_id_jenis_pegawai_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_dosen_id_jenis_pegawai_idx ON public.tb_dosen USING btree (id_jenis_pegawai);


--
-- Name: tb_dosen_id_lembaga_pengangkatan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_dosen_id_lembaga_pengangkatan_idx ON public.tb_dosen USING btree (id_lembaga_pengangkatan);


--
-- Name: tb_dosen_id_status_aktif_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_dosen_id_status_aktif_idx ON public.tb_dosen USING btree (id_status_aktif);


--
-- Name: tb_dosen_id_status_pegawai_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_dosen_id_status_pegawai_idx ON public.tb_dosen USING btree (id_status_pegawai);


--
-- Name: tb_dosen_id_wilayah_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_dosen_id_wilayah_idx ON public.tb_dosen USING btree (id_wilayah);


--
-- Name: tb_jabfung_dosen_id_dosen_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_jabfung_dosen_id_dosen_idx ON public.tb_jabfung_dosen USING btree (id_dosen);


--
-- Name: tb_jabfung_dosen_id_jabfung_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_jabfung_dosen_id_jabfung_idx ON public.tb_jabfung_dosen USING btree (id_jabfung);


--
-- Name: tb_jadwal_pembayaran_id_komponen_pembayaran_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_jadwal_pembayaran_id_komponen_pembayaran_idx ON public.tb_jadwal_pembayaran USING btree (id_komponen_pembayaran);


--
-- Name: tb_jadwal_ujian_peserta_pmb_id_jadwal_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_jadwal_ujian_peserta_pmb_id_jadwal_idx ON public.tb_jadwal_ujian_peserta_pmb USING btree (id_jadwal);


--
-- Name: tb_jadwal_ujian_peserta_pmb_id_peserta_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_jadwal_ujian_peserta_pmb_id_peserta_idx ON public.tb_jadwal_ujian_peserta_pmb USING btree (id_peserta);


--
-- Name: tb_jadwal_ujian_pmb_id_ruangan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_jadwal_ujian_pmb_id_ruangan_idx ON public.tb_jadwal_ujian_pmb USING btree (id_ruangan);


--
-- Name: tb_jawaban_survey_id_krs_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_jawaban_survey_id_krs_idx ON public.tb_jawaban_survey USING btree (id_krs);


--
-- Name: tb_jawaban_survey_id_matakuliah_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_jawaban_survey_id_matakuliah_idx ON public.tb_jawaban_survey USING btree (id_matakuliah);


--
-- Name: tb_jawaban_survey_id_pertanyaan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_jawaban_survey_id_pertanyaan_idx ON public.tb_jawaban_survey USING btree (id_pertanyaan);


--
-- Name: tb_jawaban_survey_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_jawaban_survey_id_riwayat_pendidikan_idx ON public.tb_jawaban_survey USING btree (id_riwayat_pendidikan);


--
-- Name: tb_jawaban_survey_id_unit_kelas_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_jawaban_survey_id_unit_kelas_idx ON public.tb_jawaban_survey USING btree (id_unit_kelas);


--
-- Name: tb_join_matkul_kurikulum_angkatan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_join_matkul_kurikulum_angkatan_idx ON public.tb_join_matkul_kurikulum USING btree (angkatan);


--
-- Name: tb_join_matkul_kurikulum_id_kurikulum_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_join_matkul_kurikulum_id_kurikulum_idx ON public.tb_join_matkul_kurikulum USING btree (id_kurikulum);


--
-- Name: tb_jurnal_ajar_id_absensi_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_jurnal_ajar_id_absensi_idx ON public.tb_jurnal_ajar USING btree (id_absensi);


--
-- Name: tb_kampus_merdeka_id_aktivitas_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_kampus_merdeka_id_aktivitas_idx ON public.tb_kampus_merdeka USING btree (id_aktivitas);


--
-- Name: tb_kampus_merdeka_id_anggota_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_kampus_merdeka_id_anggota_idx ON public.tb_kampus_merdeka USING btree (id_anggota);


--
-- Name: tb_kampus_merdeka_id_matkul_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_kampus_merdeka_id_matkul_idx ON public.tb_kampus_merdeka USING btree (id_matkul);


--
-- Name: tb_kebutuhan_khusus_ayah_id_calon_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_kebutuhan_khusus_ayah_id_calon_idx ON public.tb_kebutuhan_khusus_ayah USING btree (id_calon);


--
-- Name: tb_kebutuhan_khusus_ayah_id_kebutuhan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_kebutuhan_khusus_ayah_id_kebutuhan_idx ON public.tb_kebutuhan_khusus_ayah USING btree (id_kebutuhan);


--
-- Name: tb_kebutuhan_khusus_ayah_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_kebutuhan_khusus_ayah_id_riwayat_pendidikan_idx ON public.tb_kebutuhan_khusus_ayah USING btree (id_riwayat_pendidikan);


--
-- Name: tb_kebutuhan_khusus_ibu_id_calon_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_kebutuhan_khusus_ibu_id_calon_idx ON public.tb_kebutuhan_khusus_ibu USING btree (id_calon);


--
-- Name: tb_kebutuhan_khusus_ibu_id_kebutuhan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_kebutuhan_khusus_ibu_id_kebutuhan_idx ON public.tb_kebutuhan_khusus_ibu USING btree (id_kebutuhan);


--
-- Name: tb_kebutuhan_khusus_ibu_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_kebutuhan_khusus_ibu_id_riwayat_pendidikan_idx ON public.tb_kebutuhan_khusus_ibu USING btree (id_riwayat_pendidikan);


--
-- Name: tb_kebutuhan_khusus_mhs_id_calon_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_kebutuhan_khusus_mhs_id_calon_idx ON public.tb_kebutuhan_khusus_mhs USING btree (id_calon);


--
-- Name: tb_kebutuhan_khusus_mhs_id_kebutuhan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_kebutuhan_khusus_mhs_id_kebutuhan_idx ON public.tb_kebutuhan_khusus_mhs USING btree (id_kebutuhan);


--
-- Name: tb_kebutuhan_khusus_mhs_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_kebutuhan_khusus_mhs_id_riwayat_pendidikan_idx ON public.tb_kebutuhan_khusus_mhs USING btree (id_riwayat_pendidikan);


--
-- Name: tb_krs_detail_id_krs_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_krs_detail_id_krs_idx ON public.tb_krs_detail USING btree (id_krs);


--
-- Name: tb_krs_detail_id_matakuliah_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_krs_detail_id_matakuliah_idx ON public.tb_krs_detail USING btree (id_matakuliah);


--
-- Name: tb_krs_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_krs_id_riwayat_pendidikan_idx ON public.tb_krs USING btree (id_riwayat_pendidikan);


--
-- Name: tb_lulus_dropout_id_jenis_keluar_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_lulus_dropout_id_jenis_keluar_idx ON public.tb_lulus_dropout USING btree (id_jenis_keluar);


--
-- Name: tb_lulus_dropout_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_lulus_dropout_id_riwayat_pendidikan_idx ON public.tb_lulus_dropout USING btree (id_riwayat_pendidikan);


--
-- Name: tb_mahasiswa_id_negara_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mahasiswa_id_negara_idx ON public.tb_mahasiswa USING btree (id_negara);


--
-- Name: tb_matakuliah_id_jenis_mata_kuliah_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_matakuliah_id_jenis_mata_kuliah_idx ON public.tb_matakuliah USING btree (id_jenis_mata_kuliah);


--
-- Name: tb_matakuliah_id_kelompok_mata_kuliah_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_matakuliah_id_kelompok_mata_kuliah_idx ON public.tb_matakuliah USING btree (id_kelompok_mata_kuliah);


--
-- Name: tb_matakuliah_id_model_nilai_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_matakuliah_id_model_nilai_idx ON public.tb_matakuliah USING btree (id_model_nilai);


--
-- Name: tb_matkul_bersyarat_id_kurikulum_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_matkul_bersyarat_id_kurikulum_idx ON public.tb_matkul_bersyarat USING btree (id_kurikulum);


--
-- Name: tb_matkul_bersyarat_id_matkul_kurikulum_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_matkul_bersyarat_id_matkul_kurikulum_idx ON public.tb_matkul_bersyarat USING btree (id_matkul_kurikulum);


--
-- Name: tb_matkul_prodi_id_matkul_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_matkul_prodi_id_matkul_idx ON public.tb_matkul_prodi USING btree (id_matkul);


--
-- Name: tb_matkul_prodi_id_prodi_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_matkul_prodi_id_prodi_idx ON public.tb_matkul_prodi USING btree (id_prodi);


--
-- Name: tb_mst_config_krs_id_jenjang_pend_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_config_krs_id_jenjang_pend_idx ON public.tb_mst_config_krs USING btree (id_jenjang_pend);


--
-- Name: tb_mst_fakultas_id_dekan1_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_fakultas_id_dekan1_idx ON public.tb_mst_fakultas USING btree (id_dekan1);


--
-- Name: tb_mst_fakultas_id_dekan2_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_fakultas_id_dekan2_idx ON public.tb_mst_fakultas USING btree (id_dekan2);


--
-- Name: tb_mst_fakultas_id_dekan3_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_fakultas_id_dekan3_idx ON public.tb_mst_fakultas USING btree (id_dekan3);


--
-- Name: tb_mst_fakultas_id_dekan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_fakultas_id_dekan_idx ON public.tb_mst_fakultas USING btree (id_dekan);


--
-- Name: tb_mst_jalurmasuk_pmb_slug_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_jalurmasuk_pmb_slug_idx ON public.tb_mst_jalurmasuk_pmb USING btree (slug);


--
-- Name: tb_mst_model_penilaian_detail_id_model_penilaian_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_model_penilaian_detail_id_model_penilaian_idx ON public.tb_mst_model_penilaian_detail USING btree (id_model_penilaian);


--
-- Name: tb_mst_negara_id_negara_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_negara_id_negara_idx ON public.tb_mst_negara USING btree (id);


--
-- Name: tb_mst_nilai_huruf_nilai_huruf_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_nilai_huruf_nilai_huruf_idx ON public.tb_mst_nilai_huruf USING btree (nilai_huruf);


--
-- Name: tb_mst_prodi_id_kelapa_prodi_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_prodi_id_kelapa_prodi_idx ON public.tb_mst_prodi USING btree (id_kepala_prodi);


--
-- Name: tb_mst_prodi_id_prodi_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_prodi_id_prodi_idx ON public.tb_mst_prodi USING btree (id_prodi);


--
-- Name: tb_mst_ruangan_pmb_id_fakultas_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_ruangan_pmb_id_fakultas_idx ON public.tb_mst_ruangan_pmb USING btree (id_fakultas);


--
-- Name: tb_mst_spp_prodi_id_prodi_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_spp_prodi_id_prodi_idx ON public.tb_mst_spp_prodi USING btree (id_prodi);


--
-- Name: tb_mst_wilayah_id_negara_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_mst_wilayah_id_negara_idx ON public.tb_mst_wilayah USING btree (id_negara);


--
-- Name: tb_nilai_transfer_id_matkul_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_nilai_transfer_id_matkul_idx ON public.tb_nilai_transfer USING btree (id_matkul);


--
-- Name: tb_nilai_transfer_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_nilai_transfer_id_riwayat_pendidikan_idx ON public.tb_nilai_transfer USING btree (id_riwayat_pendidikan);


--
-- Name: tb_pangkat_dosen_id_dosen_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_pangkat_dosen_id_dosen_idx ON public.tb_pangkat_dosen USING btree (id_dosen);


--
-- Name: tb_pangkat_dosen_id_pangkat_golongan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_pangkat_dosen_id_pangkat_golongan_idx ON public.tb_pangkat_dosen USING btree (id_pangkat_golongan);


--
-- Name: tb_pembayaran_id_calon_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_pembayaran_id_calon_idx ON public.tb_pembayaran USING btree (id_calon);


--
-- Name: tb_pembayaran_no_invoice_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_pembayaran_no_invoice_idx ON public.tb_pembayaran USING btree (no_invoice);


--
-- Name: tb_pembimbing_id_aktivitas_mahasiswa_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_pembimbing_id_aktivitas_mahasiswa_idx ON public.tb_pembimbing USING btree (id_aktivitas_mahasiswa);


--
-- Name: tb_pembimbing_id_dosen_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_pembimbing_id_dosen_idx ON public.tb_pembimbing USING btree (id_dosen);


--
-- Name: tb_pembimbing_id_kategori_kegiatan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_pembimbing_id_kategori_kegiatan_idx ON public.tb_pembimbing USING btree (id_kategori_kegiatan);


--
-- Name: tb_pengajuan_surat_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_pengajuan_surat_id_riwayat_pendidikan_idx ON public.tb_pengajuan_surat USING btree (id_riwayat_pendidikan);


--
-- Name: tb_penguji_id_aktivitas_mahasiswa_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_penguji_id_aktivitas_mahasiswa_idx ON public.tb_penguji USING btree (id_aktivitas_mahasiswa);


--
-- Name: tb_penguji_id_dosen_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_penguji_id_dosen_idx ON public.tb_penguji USING btree (id_dosen);


--
-- Name: tb_penguji_id_kategori_kegiatan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_penguji_id_kategori_kegiatan_idx ON public.tb_penguji USING btree (id_kategori_kegiatan);


--
-- Name: tb_penugasan_dosen_id_dosen_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_penugasan_dosen_id_dosen_idx ON public.tb_penugasan_dosen USING btree (id_dosen);


--
-- Name: tb_penugasan_dosen_id_prodi_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_penugasan_dosen_id_prodi_idx ON public.tb_penugasan_dosen USING btree (id_prodi);


--
-- Name: tb_pertanyaan_survey_id_jenis_survey_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_pertanyaan_survey_id_jenis_survey_idx ON public.tb_pertanyaan_survey USING btree (id_jenis_survey);


--
-- Name: tb_perwalian_id_dosen_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_perwalian_id_dosen_idx ON public.tb_perwalian USING btree (id_dosen);


--
-- Name: tb_perwalian_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_perwalian_id_riwayat_pendidikan_idx ON public.tb_perwalian USING btree (id_riwayat_pendidikan);


--
-- Name: tb_peserta_pmb_lokal_id_calon_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_peserta_pmb_lokal_id_calon_idx ON public.tb_peserta_pmb_lokal USING btree (id_calon);


--
-- Name: tb_peserta_pmb_lokal_prodi_pilihan_1_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_peserta_pmb_lokal_prodi_pilihan_1_idx ON public.tb_peserta_pmb_lokal USING btree (prodi_pilihan_1);


--
-- Name: tb_peserta_pmb_lokal_prodi_pilihan_2_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_peserta_pmb_lokal_prodi_pilihan_2_idx ON public.tb_peserta_pmb_lokal USING btree (prodi_pilihan_2);


--
-- Name: tb_peserta_pmb_lokal_prodi_pilihan_3_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_peserta_pmb_lokal_prodi_pilihan_3_idx ON public.tb_peserta_pmb_lokal USING btree (prodi_pilihan_3);


--
-- Name: tb_prestasi_id_mahasiswa_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_prestasi_id_mahasiswa_idx ON public.tb_prestasi USING btree (id_mahasiswa, id_aktivitas_mahasiswa, id_jenis_prestasi, id_tingkat_prestasi);


--
-- Name: tb_riwayat_pend_dosen_id_dosen_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_riwayat_pend_dosen_id_dosen_idx ON public.tb_riwayat_pend_dosen USING btree (id_dosen);


--
-- Name: tb_riwayat_pendidikan_id_jenis_keluar_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_riwayat_pendidikan_id_jenis_keluar_idx ON public.tb_riwayat_pendidikan USING btree (id_jenis_keluar);


--
-- Name: tb_riwayat_pendidikan_id_mahasiswa_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_riwayat_pendidikan_id_mahasiswa_idx ON public.tb_riwayat_pendidikan USING btree (id_mahasiswa);


--
-- Name: tb_riwayat_pendidikan_id_spp_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_riwayat_pendidikan_id_spp_idx ON public.tb_riwayat_pendidikan USING btree (id_spp);


--
-- Name: tb_roster_asisten_id_dosen_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_roster_asisten_id_dosen_idx ON public.tb_roster_asisten USING btree (id_dosen);


--
-- Name: tb_roster_asisten_id_roster_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_roster_asisten_id_roster_idx ON public.tb_roster_asisten USING btree (id_roster);


--
-- Name: tb_roster_asisten_id_unit_kelas_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_roster_asisten_id_unit_kelas_idx ON public.tb_roster_asisten USING btree (id_unit_kelas);


--
-- Name: tb_roster_id_hari_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_roster_id_hari_idx ON public.tb_roster USING btree (id_hari);


--
-- Name: tb_roster_id_unit_kelas_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_roster_id_unit_kelas_idx ON public.tb_roster USING btree (id_unit_kelas);


--
-- Name: tb_saran_survey_id_krs_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_saran_survey_id_krs_idx ON public.tb_saran_survey USING btree (id_krs);


--
-- Name: tb_saran_survey_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_saran_survey_id_riwayat_pendidikan_idx ON public.tb_saran_survey USING btree (id_riwayat_pendidikan);


--
-- Name: tb_unit_kelas_id_kurikulum_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_unit_kelas_id_kurikulum_idx ON public.tb_unit_kelas USING btree (id_kurikulum);


--
-- Name: tb_unit_kelas_id_prodi_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_unit_kelas_id_prodi_idx ON public.tb_unit_kelas USING btree (id_prodi);


--
-- Name: tb_unit_kelas_mahasiswa_id_krs_detail_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_unit_kelas_mahasiswa_id_krs_detail_idx ON public.tb_unit_kelas_mahasiswa USING btree (id_krs_detail);


--
-- Name: tb_unit_kelas_mahasiswa_id_riwayat_pendidikan_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_unit_kelas_mahasiswa_id_riwayat_pendidikan_idx ON public.tb_unit_kelas_mahasiswa USING btree (id_riwayat_pendidikan);


--
-- Name: tb_unit_kelas_mahasiswa_id_unit_kelas_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_unit_kelas_mahasiswa_id_unit_kelas_idx ON public.tb_unit_kelas_mahasiswa USING btree (id_unit_kelas);


--
-- Name: tb_unit_kelas_matakuliah_id_kurikulum_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_unit_kelas_matakuliah_id_kurikulum_idx ON public.tb_unit_kelas_matakuliah USING btree (id_kurikulum);


--
-- Name: tb_unit_kelas_matakuliah_id_matakuliah_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_unit_kelas_matakuliah_id_matakuliah_idx ON public.tb_unit_kelas_matakuliah USING btree (id_matakuliah);


--
-- Name: tb_unit_kelas_matakuliah_id_unit_kelas_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tb_unit_kelas_matakuliah_id_unit_kelas_idx ON public.tb_unit_kelas_matakuliah USING btree (id_unit_kelas);


--
-- Name: tb_absensi after_delete_absensi; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_delete_absensi AFTER DELETE ON public.tb_absensi FOR EACH ROW EXECUTE FUNCTION public.after_delete_absensi();


--
-- Name: tb_aktivitas_mahasiswa after_delete_aktivitas_mahasiswa; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_delete_aktivitas_mahasiswa AFTER DELETE ON public.tb_aktivitas_mahasiswa FOR EACH ROW EXECUTE FUNCTION public.after_delete_aktivitas_mahasiswa();


--
-- Name: tb_krs_detail after_delete_krs_detail; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_delete_krs_detail AFTER DELETE ON public.tb_krs_detail FOR EACH ROW EXECUTE FUNCTION public.after_delete_krs_detail();


--
-- Name: tb_lulus_dropout after_delete_lulus_dropout; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_delete_lulus_dropout AFTER DELETE ON public.tb_lulus_dropout FOR EACH ROW EXECUTE FUNCTION public.after_delete_lulus_dropout();


--
-- Name: tb_unit_kelas_mahasiswa after_delete_unit_kelas_mahasiswa; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_delete_unit_kelas_mahasiswa AFTER DELETE ON public.tb_unit_kelas_mahasiswa FOR EACH ROW EXECUTE FUNCTION public.after_delete_unit_kelas_mahasiswa();


--
-- Name: tb_akm after_insert_akm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_insert_akm AFTER INSERT ON public.tb_akm FOR EACH ROW EXECUTE FUNCTION public.after_insert_akm();


--
-- Name: tb_krs_detail after_insert_krs_detail; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_insert_krs_detail AFTER INSERT ON public.tb_krs_detail FOR EACH ROW EXECUTE FUNCTION public.after_insert_krs_detail();


--
-- Name: tb_pembayaran after_insert_pembayaran; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_insert_pembayaran AFTER INSERT ON public.tb_pembayaran FOR EACH ROW EXECUTE FUNCTION public.after_insert_pembayaran();


--
-- Name: tb_penugasan_dosen after_insert_penugasan_dosen; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_insert_penugasan_dosen AFTER INSERT ON public.tb_penugasan_dosen FOR EACH ROW EXECUTE FUNCTION public.after_insert_penugasan_dosen();


--
-- Name: tb_peserta_pmb_lokal after_insert_peserta_pmb_lokal; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_insert_peserta_pmb_lokal AFTER INSERT ON public.tb_peserta_pmb_lokal FOR EACH ROW EXECUTE FUNCTION public.after_insert_peserta_pmb_lokal();


--
-- Name: tb_unit_kelas_mahasiswa after_insert_unit_kelas_mahasiswa; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_insert_unit_kelas_mahasiswa AFTER INSERT ON public.tb_unit_kelas_mahasiswa FOR EACH ROW EXECUTE FUNCTION public.after_insert_unit_kelas_mahasiswa();


--
-- Name: tb_akm after_update_akm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_update_akm AFTER UPDATE ON public.tb_akm FOR EACH ROW EXECUTE FUNCTION public.after_update_akm();


--
-- Name: tb_calon_mahasiswa after_update_calon_mahasiswa; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_update_calon_mahasiswa AFTER UPDATE ON public.tb_calon_mahasiswa FOR EACH ROW EXECUTE FUNCTION public.after_update_calon_mahasiswa();


--
-- Name: tb_pembayaran after_update_pembayaran; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_update_pembayaran AFTER UPDATE ON public.tb_pembayaran FOR EACH ROW EXECUTE FUNCTION public.after_update_pembayaran();


--
-- Name: tb_matkul_kurikulum after_update_tb_matkul_kurikulum; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER after_update_tb_matkul_kurikulum AFTER UPDATE ON public.tb_matkul_kurikulum FOR EACH ROW EXECUTE FUNCTION public.after_update_tb_matkul_kurikulum();


--
-- Name: tb_acara_mendatang before_insert_acara_mendatang; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER before_insert_acara_mendatang BEFORE INSERT ON public.tb_acara_mendatang FOR EACH ROW EXECUTE FUNCTION public.before_insert_acara_mendatang();


--
-- Name: tb_akm before_insert_akm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER before_insert_akm BEFORE INSERT ON public.tb_akm FOR EACH ROW EXECUTE FUNCTION public.before_insert_akm();


--
-- Name: tb_berita before_insert_berita; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER before_insert_berita BEFORE INSERT ON public.tb_berita FOR EACH ROW EXECUTE FUNCTION public.before_insert_berita();


--
-- Name: tb_krs before_insert_krs; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER before_insert_krs BEFORE INSERT ON public.tb_krs FOR EACH ROW EXECUTE FUNCTION public.before_insert_krs();


--
-- Name: tb_krs_detail before_insert_krs_detail; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER before_insert_krs_detail BEFORE INSERT ON public.tb_krs_detail FOR EACH ROW EXECUTE FUNCTION public.before_insert_krs_detail();


--
-- Name: tb_nilai_transfer before_insert_nilai_transfer; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER before_insert_nilai_transfer BEFORE INSERT ON public.tb_nilai_transfer FOR EACH ROW EXECUTE FUNCTION public.before_insert_nilai_transfer();


--
-- Name: tb_penugasan_dosen before_insert_penugasan_dosen; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER before_insert_penugasan_dosen BEFORE INSERT ON public.tb_penugasan_dosen FOR EACH ROW EXECUTE FUNCTION public.before_insert_penugasan_dosen();


--
-- Name: tb_krs_detail before_update_krs_detail; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER before_update_krs_detail BEFORE UPDATE ON public.tb_krs_detail FOR EACH ROW EXECUTE FUNCTION public.before_update_krs_detail();


--
-- Name: tb_nilai_transfer before_update_nilai_transfer; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER before_update_nilai_transfer BEFORE UPDATE ON public.tb_nilai_transfer FOR EACH ROW EXECUTE FUNCTION public.before_update_nilai_transfer();


--
-- Name: tb_kurikulum delete_kurikulum; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_kurikulum BEFORE DELETE ON public.tb_kurikulum FOR EACH ROW EXECUTE FUNCTION public.delete_kurikulum();


--
-- Name: tb_mahasiswa delete_mahasiswa; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_mahasiswa AFTER DELETE ON public.tb_mahasiswa FOR EACH ROW EXECUTE FUNCTION public.delete_mahasiswa();


--
-- Name: tb_matkul_kurikulum delete_matkul_kurikulum; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_matkul_kurikulum BEFORE DELETE ON public.tb_matkul_kurikulum FOR EACH ROW EXECUTE FUNCTION public.delete_matkul_kurikulum();


--
-- Name: tb_riwayat_pendidikan delete_riwayat_pendidikan; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_riwayat_pendidikan BEFORE DELETE ON public.tb_riwayat_pendidikan FOR EACH ROW EXECUTE FUNCTION public.delete_riwayat_pendidikan();


--
-- Name: tb_roster delete_roster; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_roster BEFORE DELETE ON public.tb_roster FOR EACH ROW EXECUTE FUNCTION public.delete_roster();


--
-- Name: tb_unit_kelas delete_unit_kelas; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_unit_kelas AFTER DELETE ON public.tb_unit_kelas FOR EACH ROW EXECUTE FUNCTION public.delete_unit_kelas();


--
-- Name: tb_matkul_kurikulum insert_matkul_kurikulum; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER insert_matkul_kurikulum BEFORE INSERT ON public.tb_matkul_kurikulum FOR EACH ROW EXECUTE FUNCTION public.insert_matkul_kurikulum();


--
-- Name: tb_mst_all_prodi tb_mst_all_prodi_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_all_prodi
    ADD CONSTRAINT tb_mst_all_prodi_fk FOREIGN KEY (id_perguruan_tinggi) REFERENCES public.tb_mst_all_pt(id);


--
-- Name: tb_mst_bidang_minat tb_mst_bidang_minat_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_bidang_minat
    ADD CONSTRAINT tb_mst_bidang_minat_fk FOREIGN KEY (id_prodi) REFERENCES public.tb_mst_prodi(id);


--
-- Name: tb_mst_perguruan_tinggi tb_mst_perguruan_tinggi_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_mst_perguruan_tinggi
    ADD CONSTRAINT tb_mst_perguruan_tinggi_fk FOREIGN KEY (id_wilayah) REFERENCES public.tb_mst_wilayah(id);


--
-- PostgreSQL database dump complete
--

