FROM php:7.4.30-apache
# then add the following `RUN ...` lines in each separate line, like this:
RUN apt-get update


# Install Postgre PDO
RUN apt-get install -y libpq-dev libicu-dev\
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl 

# COPY ./docker/apache/httpd-vhosts.conf /etc/apache2/sites-available/000-default.conf

RUN docker-php-ext-install mysqli
RUN a2enmod rewrite
ADD . /var/www/html